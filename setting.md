## application.properties


```

#DataSource
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://localhost:5432/code_reminder
spring.datasource.username=ご自身の環境に合わせて下さい
spring.datasource.password=ご自身の環境に合わせて下さい

#MailSender
spring.mail.host=smtp.gmail.com
spring.mail.port=587
spring.mail.username=別途ご連絡下さい
spring.mail.password=別途ご連絡下さい
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true

#mybatis
mybatis.config-location=classpath:/mybatis/mybatis-config.xml

#server
server.port=8080

#flyway
spring.flyway.locations=classpath:/db/migration
spring.flyway.url=jdbc:postgresql://localhost:5432/code_reminder
spring.flyway.username=ご自身の環境に合わせて下さい
spring.flyway.password=ご自身の環境に合わせて下さい
spring.flyway.baselineOnMigrate=true
spring.flyway.baselineVersion=1.0
spring.flyway.baselineDescription=Initial

#sendMailProperty
spring.send.mail.protocol=http://
spring.send.mail.hostname=localhost:8080
spring.send.mail.domain=/
spring.send.mail.from=別途ご連絡下さい

```

## flyway.properies

```
flyway.url=jdbc:postgresql://localhost:5432/code_reminder
flyway.user=ご自身の環境に合わせて下さい
flyway.password=ご自身の環境に合わせて下さい
flyway.baselineOnMigrate=true
flyway.baselineVersion=1.0
flyway.baselineDescription=Initial
flyway.locations=classpath:/db/migration
```