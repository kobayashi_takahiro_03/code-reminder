package jp.co.code_reminder.app.welcome;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.code_reminder.app.auth.LoginController;
import lombok.val;

@SpringBootTest
@Transactional
public class LoginControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private LoginController target;

	@BeforeEach
	public void 初期化処理() throws Exception {
		val resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/");
		resolver.setSuffix(".html");

		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).setViewResolvers(resolver)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
	}

	@Test
	public void ログイン画面が表示出来るか() throws Exception {
		this.mockMvc.perform(get("/login"))
				.andExpect(status().isOk())
				.andExpect(view().name("auth/login"));
	}
}