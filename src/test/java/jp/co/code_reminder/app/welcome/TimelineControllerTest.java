	package jp.co.code_reminder.app.welcome;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.code_reminder.app.security.WithMockCustomUser;
import jp.co.code_reminder.web.validation.ValidAdvice;
import lombok.val;

@SpringBootTest
@Transactional(rollbackFor = Exception.class)
public class TimelineControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private TimelineController target;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private ValidAdvice validAdvice; //追加

	@BeforeEach
	public void 初期化処理() throws Exception {
		val resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/");
		resolver.setSuffix(".html");

		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).setControllerAdvice(this.validAdvice)
				.setViewResolvers(resolver).setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
				.build();
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	void Timelineが正しく表示できるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.mockMvc.perform(get("/timeline"))
				.andExpect(status().isOk())
				.andExpect(view().name("welcome/timeline"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	void 公開になっている問題のみ取得できているか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		var mvcResult = this.mockMvc.perform(get("/timeline"))
				.andExpect(status().isOk())
				.andExpect(view().name("welcome/timeline"))
				.andReturn();

		var mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qacFormList = ((List<QuestionAccountCategoryForm>) mav.getModel().get("qacFormList"));
		val actualListSize = qacFormList.size();
		val expectListSize = 5;
		assertThat(actualListSize, is(expectListSize));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	void 部分一致で問題絞り込みができるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		val searchWord = "パイソン";
		val searchType = "Question";

		var mvcResult = this.mockMvc.perform(get("/timeline?searchWord=" + searchWord + "&searchType=" + searchType))
				.andExpect(status().isOk())
				.andExpect(view().name("welcome/timeline"))
				.andReturn();

		var mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qacFormList = ((List<QuestionAccountCategoryForm>) mav.getModel().get("qacFormList"));
		val actualListSize = qacFormList.size();
		val expectListSize = 2;
		assertThat(actualListSize, is(expectListSize));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	void 検索で非公開の問題を取得しないか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		val searchWord = "数学";
		val searchType = "Question";

		var mvcResult = this.mockMvc.perform(get("/timeline?searchWord=" + searchWord + "&searchType=" + searchType))
				.andExpect(status().isOk())
				.andExpect(view().name("welcome/timeline"))
				.andReturn();

		var mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qacFormList = ((List<QuestionAccountCategoryForm>) mav.getModel().get("qacFormList"));
		val actualListSize = qacFormList.size();
		val expectListSize = 0;
		assertThat(actualListSize, is(expectListSize));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	void 部分一致でカテゴリの絞り込みができるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		val searchWord = "python";
		val searchType = "Category";

		var mvcResult = this.mockMvc.perform(get("/timeline?searchWord=" + searchWord + "&searchType=" + searchType))
				.andExpect(status().isOk())
				.andExpect(view().name("welcome/timeline"))
				.andReturn();

		var mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qacFormList = ((List<QuestionAccountCategoryForm>) mav.getModel().get("qacFormList"));
		val actualListSize = qacFormList.size();
		val expectListSize = 2;
		assertThat(actualListSize, is(expectListSize));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	void ページ遷移後も検索結果が保持されているか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		val searchWord = "ruby";
		val searchType = "Category";
		val pageNum = "1";

		var mvcResult = this.mockMvc
				.perform(get("/timeline?page=" + pageNum + "&searchWord=" + searchWord + "&searchType=" + searchType))
				.andExpect(status().isOk())
				.andExpect(view().name("welcome/timeline"))
				.andReturn();

		var mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qacFormList = ((List<QuestionAccountCategoryForm>) mav.getModel().get("qacFormList"));
		val actualListSize = qacFormList.size();
		val expectListSize = 1;
		assertThat(actualListSize, is(expectListSize));
	}

	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

}