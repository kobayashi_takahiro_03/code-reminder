package jp.co.code_reminder.app.question;

import static jp.co.code_reminder.app.forgot_pass.GenerateString.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.code_reminder.app.security.WithMockCustomUser;
import jp.co.code_reminder.domain.model.question.Learn;
import jp.co.code_reminder.domain.model.question.Share;
import jp.co.code_reminder.domain.service.account.AccountReferService;
import jp.co.code_reminder.domain.service.question.QuestionReferService;
import jp.co.code_reminder.web.validation.ValidAdvice;
import lombok.val;

@SpringBootTest
public class QuestionControllerTest {

	private MockMvc mockMvc;
	private QuestionReviewCategoryForm qrcForm;
	private QuestionForm questionForm;
	private ReviewForm reviewForm;
	private List<Integer> categoryNumList;
	@Autowired
	private QuestionsController target;

	@Autowired
	private AccountReferService accountReferService;

	@Autowired
	private QuestionReferService questionReferService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private ValidAdvice validAdvice; //追加

	@BeforeEach
	public void 初期化処理() throws Exception {
		this.qrcForm = new QuestionReviewCategoryForm();
		this.questionForm = new QuestionForm();
		this.reviewForm = new ReviewForm();
		this.categoryNumList = new ArrayList<Integer>();

		val resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/");
		resolver.setSuffix(".html");

		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).setViewResolvers(resolver)
				.setControllerAdvice(this.validAdvice)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 新規投稿が出来るか() throws Exception {
		MockitoAnnotations.initMocks(this);
		executeScript("/db/insertCategories.sql");

		this.questionForm.setQuestion("数学1");
		this.questionForm.setAnswer("数学1答え");
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.reviewForm.setCompleteDay(200);
		this.categoryNumList.add(7);
		this.categoryNumList.add(1);
		this.categoryNumList.add(6);
		this.questionForm.setIsShared(Share.Shared);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(this.categoryNumList);

		this.mockMvc.perform(post("/questions/new")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/questions"));

		val accountId = this.accountReferService.findOne("kobayasi", "login_id").getUuid();
		val testQuestion = this.questionReferService.findByAccountId(accountId);
		val expectedSize = 1;
		val expectedQuestion = "数学1";
		val expectedAnswer = "数学1答え";
		val expectedcompleteDay = 200;

		assertEquals(expectedSize, testQuestion.size());
		assertEquals(expectedQuestion, testQuestion.get(0).getQuestion().getQuestion());
		assertEquals(expectedAnswer, testQuestion.get(0).getQuestion().getAnswer());
		assertEquals(expectedcompleteDay, testQuestion.get(0).getReview().getCompleteDay());
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 存在しないカテゴリで新規投稿して例外をキャッチ出来るか() throws Exception {
		MockitoAnnotations.initMocks(this);
		executeScript("/db/insertCategories.sql");

		this.questionForm.setQuestion("数学1");
		this.questionForm.setAnswer("数学1答え");
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.questionForm.setAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2");
		this.reviewForm.setCompleteDay(200);
		this.categoryNumList.add(10);
		this.categoryNumList.add(20);
		this.categoryNumList.add(30);
		this.questionForm.setIsShared(Share.Shared);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(this.categoryNumList);

		this.mockMvc.perform(post("/questions/new")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/questions"))
				.andExpect(flash().attribute("error", "不正なパラメーターです"));
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 重複する問題IDで登録して例外をキャッチ出来るか() throws Exception {
		MockitoAnnotations.initMocks(this);
		executeScript("/db/insertQuestion.sql");

		this.questionForm.setQuestion("数学1");
		this.questionForm.setAnswer("数学1答え");
		this.questionForm.setAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2");
		this.questionForm.setId(1);
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.reviewForm.setCompleteDay(200);
		this.categoryNumList.add(6);
		this.categoryNumList.add(1);
		this.categoryNumList.add(7);
		this.questionForm.setIsShared(Share.Shared);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(this.categoryNumList);

		this.mockMvc.perform(post("/questions/new")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/questions"))
				.andExpect(flash().attribute("error", "登録に失敗しました。最初からやり直して下さい"));
	}

	@Disabled //全角スペースがこの方法では弾けません
	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 存在しない問題IDを削除しようとして例外をキャッチ出来るか() throws Exception {
		MockitoAnnotations.initMocks(this);
		executeScript("/db/insertQuestion.sql");

		this.mockMvc.perform(post("/questions/20/edit").queryParam("delete", "delete")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/questions"))
				.andExpect(flash().attribute("error", "削除に失敗しました"));
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 存在しないカテゴリで更新して例外をキャッチ出来るか() throws Exception {
		MockitoAnnotations.initMocks(this);
		executeScript("/db/insertQuestion.sql");

		this.questionForm.setQuestion("数学1");
		this.questionForm.setAnswer("数学1答え");
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.reviewForm.setCompleteDay(200);
		this.categoryNumList.add(10);
		this.categoryNumList.add(20);
		this.categoryNumList.add(30);
		this.questionForm.setIsShared(Share.Shared);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(this.categoryNumList);

		this.mockMvc.perform(post("/questions/1/edit").queryParam("update", "update")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/questions"))
				.andExpect(flash().attribute("error", "不正なパラメーターです"));
	}


	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ選択せずに新規投稿が出来るか() throws Exception {

		executeScript("/db/insertCategories.sql");

		this.questionForm.setQuestion("数学1");
		this.questionForm.setAnswer("数学1答え");
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.questionForm.setIsShared(Share.Shared);
		this.reviewForm.setCompleteDay(200);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(this.categoryNumList);

		this.mockMvc.perform(post("/questions/new")
				.flashAttr("qrcForm", qrcForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/questions"));

		val accountId = this.accountReferService.findOne("kobayasi", "login_id").getUuid();
		val testQuestion = this.questionReferService.findByAccountId(accountId);
		val expectedSize = 1;
		val expectedQuestion = "数学1";
		val expectedAnswer = "数学1答え";
		val expectedcompleteDay = 200;

		assertEquals(expectedSize, testQuestion.size());
		assertEquals(expectedQuestion, testQuestion.get(0).getQuestion().getQuestion());
		assertEquals(expectedAnswer, testQuestion.get(0).getQuestion().getAnswer());
		assertEquals(expectedcompleteDay, testQuestion.get(0).getReview().getCompleteDay());
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 数学カテゴリと紐づいた問題のみが絞り込みが出来るか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		var pageNum = "0";
		//selectedNum = 1 ->数学カテゴリID
		var mvcResult = this.mockMvc.perform(get("/questions?page=" + pageNum + "&selectedCategoryNums=1"))
				.andExpect(status().isOk())
				.andExpect(view().name("question/index"))
				.andReturn();

		var mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qrcFormList = ((List<QuestionReviewCategoryForm>) mav.getModel().get("qrcFormList"));
		val actualListSize = qrcFormList.size();
		val expectListSize = 3;
		assertThat(actualListSize, is(expectListSize));
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void パイソンカテゴリとルビーカテゴリに紐づいた問題のみが絞り込みが出来るか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		//selectedNum = 6->パイソン
		//selectedNum = 7->ルビー
		var pageNum = "0";
		var mvcResult = this.mockMvc.perform(get("/questions?page=" + pageNum + "&selectedCategoryNums=6&selectedCategoryNums=7"))
				.andExpect(status().isOk())
				.andExpect(view().name("question/index"))
				.andReturn();

		var mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qrcFormList = ((List<QuestionReviewCategoryForm>) mav.getModel().get("qrcFormList"));
		val actualListSize = qrcFormList.size();
		val expectListSize = 5;
		assertThat(actualListSize, is(expectListSize));

		pageNum = "1";
		mvcResult = this.mockMvc.perform(get("/questions?page=" + pageNum + "&selectedCategoryNums=6&selectedCategoryNums=7"))
				.andReturn();
		mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qrcFormList2 = ((List<QuestionReviewCategoryForm>) mav.getModel().get("qrcFormList"));
		val actualListSize2 = qrcFormList2.size();
		val expectListSize2 = 4;
		assertThat(actualListSize2, is(expectListSize2));
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 全て未入力で弾けるか() throws Exception {
		executeScript("/db/insertCategories.sql");

		this.questionForm.setQuestion("");
		this.questionForm.setAnswer("");
		this.questionForm.setIsLearned(null);
		this.reviewForm.setCompleteDay(null);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(this.categoryNumList);

		this.mockMvc.perform(post("/questions/new")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isOk())
				.andExpect(model().attribute("qrcForm", this.qrcForm))
				.andExpect(model().attributeHasFieldErrors("qrcForm", "question.question", "question.answer",
						"question.isLearned", "review.completeDay",
						"question.isShared"))
				.andExpect(model().errorCount(5))
				.andExpect(view().name("question/new"));
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 問題を100001文字以上で弾けるか() throws Exception {
		executeScript("/db/insertCategories.sql");

		this.questionForm.setQuestion(str100001);
		this.questionForm.setAnswer(str100000);
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.questionForm.setIsShared(Share.Shared);
		this.reviewForm.setCompleteDay(200);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(new ArrayList<Integer>(Arrays.asList(1)));

		this.mockMvc.perform(post("/questions/new")
				.flashAttr("qrcForm", qrcForm))
				.andExpect(status().isOk())
				.andExpect(model().attribute("qrcForm", this.qrcForm))
				.andExpect(model().attributeHasFieldErrors("qrcForm", "question.question"))
				.andExpect(model().errorCount(1))
				.andExpect(view().name("question/new"));
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 回答を100001文字以上で弾けるか() throws Exception {
		executeScript("/db/insertCategories.sql");

		this.questionForm.setQuestion(str100000);
		this.questionForm.setAnswer(str100001);
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.questionForm.setIsShared(Share.Shared);
		this.reviewForm.setCompleteDay(200);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(new ArrayList<Integer>(Arrays.asList(1)));

		this.mockMvc.perform(post("/questions/new")
				.flashAttr("qrcForm", qrcForm))
				.andExpect(status().isOk())
				.andExpect(model().attribute("qrcForm", this.qrcForm))
				.andExpect(model().attributeHasFieldErrors("qrcForm", "question.answer"))
				.andExpect(model().errorCount(1))
				.andExpect(view().name("question/new"));
	}

	@Disabled //全角スペースがこの方法では弾けません
	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 新規投稿で全角スペースのフォームを弾けるか() throws Exception {
		executeScript("/db/insertCategories.sql");

		this.questionForm.setQuestion("　");
		this.questionForm.setAnswer("　");
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.reviewForm.setCompleteDay(200);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(new ArrayList<Integer>(Arrays.asList(1)));

		System.out.println(this.qrcForm);
		this.mockMvc.perform(post("/questions/new")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(model().attribute("qrcForm", this.qrcForm))
				.andExpect(model().attributeHasFieldErrors("qrcForm", "question.question", "question.answer"))
				.andExpect(model().errorCount(2))
				.andExpect(view().name("question/new"));
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 編集画面が表示できるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.mockMvc.perform(get("/questions/1/edit"))
				.andExpect(status().isOk())
				.andExpect(view().name("question/edit"));
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 不正な編集画面のURLを弾いて問題一覧に戻せるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.mockMvc.perform(get("/questions/13/edit"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/questions"));
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 問題編集ができるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.questionForm.setId(1);
		this.questionForm.setQuestion("泉ピン子");
		this.questionForm.setAnswer("泉ピン子答え");
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.questionForm.setIsShared(Share.NotShared);
		this.questionForm.setAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2");

		this.reviewForm.setCompleteDay(365);

		this.qrcForm.setQuestion(questionForm);
		this.qrcForm.setReview(reviewForm);
		this.qrcForm.setCategoryNumList(new ArrayList<Integer>(Arrays.asList(1)));

		this.mockMvc.perform(post("/questions/2/edit").queryParam("update", "update")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/questions"));

		val testQuestion = this.questionReferService.findByQuestionId("1");
		val expectedQuestion = "泉ピン子";
		val expectedAnswer = "泉ピン子答え";
		val expectedcompleteDay = 365;
		val expectedIsShared = Share.NotShared;

		assertEquals(expectedQuestion, testQuestion.getQuestion().getQuestion());
		assertEquals(expectedAnswer, testQuestion.getQuestion().getAnswer());
		assertEquals(expectedcompleteDay, testQuestion.getReview().getCompleteDay());
		assertEquals(expectedIsShared, testQuestion.getQuestion().getIsShared());
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 問題が削除できるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.mockMvc.perform(post("/questions/1/edit").queryParam("delete", "delete"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/questions"));

		val testQuestions = this.questionReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2");
		val expectedSize = 11;

		assertEquals(expectedSize, testQuestions.size());
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 全て未入力で編集を弾けるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.questionForm.setId(1);
		this.questionForm.setQuestion("");
		this.questionForm.setAnswer("");
		this.questionForm.setIsLearned(null);
		this.questionForm.setIsShared(null);

		this.reviewForm.setCompleteDay(null);

		this.qrcForm.setQuestion(questionForm);
		this.qrcForm.setReview(reviewForm);
		this.qrcForm.setCategoryNumList(null);

		var mvcResult = this.mockMvc.perform(post("/questions/1/edit").queryParam("update", "update")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isOk())
				.andExpect(model().attribute("qrcForm", this.qrcForm))
				.andExpect(model().attributeHasFieldErrors("qrcForm", "question.question", "question.answer",
						"question.isLearned", "review.completeDay"))
				.andExpect(model().errorCount(5))
				.andExpect(view().name("question/edit"))
				.andReturn();

		var actualURI = mvcResult.getRequest().getRequestURI();
		val expectedURI = "/questions/1/edit";
		assertThat(actualURI, is(expectedURI));
	}

	@Disabled //全角スペースがこの方法では弾けません
	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 問題編集で全角スペースのフォームを弾けるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.questionForm.setId(1);
		this.questionForm.setQuestion("　");
		this.questionForm.setAnswer("　");
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.reviewForm.setCompleteDay(200);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(new ArrayList<Integer>(Arrays.asList(1)));

		System.out.println(this.qrcForm);
		var mvcResult = this.mockMvc.perform(post("/questions/1/edit").queryParam("update", "update")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(model().attribute("qrcForm", this.qrcForm))
				.andExpect(model().attributeHasFieldErrors("qrcForm", "question.question", "question.answer"))
				.andExpect(model().errorCount(2))
				.andExpect(view().name("question/edit"))
				.andReturn();

		var actualURI = mvcResult.getRequest().getRequestURI();
		val expectedURI = "/questions/1/edit";
		assertThat(actualURI, is(expectedURI));
	}

	@Disabled //このテストを有効化するにはこの行をコメントアウトしてください
	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 問題が100001文字以上で編集して弾けるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.questionForm.setId(1);
		this.questionForm.setQuestion(str100001);
		this.questionForm.setAnswer(str100001);
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.reviewForm.setCompleteDay(200);

		this.qrcForm.setQuestion(questionForm);
		this.qrcForm.setReview(reviewForm);
		this.categoryNumList.add(1);
		this.qrcForm.setCategoryNumList(this.categoryNumList);

		var mvcResult = this.mockMvc.perform(post("/questions/1/edit").queryParam("update", "update")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isOk())
				.andReturn();

		var actualURI = mvcResult.getRequest().getRequestURI();
		val expectURI = "/questions/1/edit";
		assertThat(actualURI, is(expectURI));
	}

	@Disabled //このテストを有効化するにはこの行をコメントアウトしてください
	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 答えが100001文字以上で編集して弾けるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.questionForm.setId(1);
		this.questionForm.setQuestion(str100000);
		this.questionForm.setAnswer(str100001);
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.reviewForm.setCompleteDay(200);

		this.qrcForm.setQuestion(questionForm);
		this.qrcForm.setReview(reviewForm);
		this.categoryNumList.add(1);
		this.qrcForm.setCategoryNumList(this.categoryNumList);

		var mvcResult = this.mockMvc.perform(post("/questions/1/edit").queryParam("update", "update")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isOk())
				.andReturn();

		var actualURI = mvcResult.getRequest().getRequestURI();
		val expectURI = "/questions/1/edit";
		assertThat(actualURI, is(expectURI));
	}

	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

}
