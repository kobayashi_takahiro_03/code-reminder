package jp.co.code_reminder.app.question;

import static jp.co.code_reminder.app.forgot_pass.GenerateString.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import jp.co.code_reminder.core.validator.order.GroupOrder;
import jp.co.code_reminder.domain.model.question.Learn;
import jp.co.code_reminder.domain.model.question.Share;
import lombok.val;

@SpringBootTest
public class QuestionReviewCategoryFormTest {

	private QuestionReviewCategoryForm qrcForm = new QuestionReviewCategoryForm();
	private QuestionForm questionForm = new QuestionForm();
	private ReviewForm reviewForm = new ReviewForm();
	private BindingResult bindingResult = new BindException(this.qrcForm, "accountForm");

	private Validator javaxValidator;

	@BeforeEach
	public void 値をセット() throws Exception {
		val validatorFactory = Validation.buildDefaultValidatorFactory();
		this.javaxValidator = validatorFactory.getValidator();

		this.questionForm.setId(1);
		this.questionForm.setQuestion("問題１");
		this.questionForm.setAnswer("答え１");
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.questionForm.setIsShared(Share.Shared);
		this.questionForm.setAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2");

		this.reviewForm.setCompleteDay(200);

		this.qrcForm.setQuestion(this.questionForm);
		this.qrcForm.setReview(this.reviewForm);
		this.qrcForm.setCategoryNumList(new ArrayList<Integer>(Arrays.asList(1, 6, 7)));

	}

	@Test
	public void エラー無し() throws Exception {
		val violations = this.javaxValidator.validate(this.qrcForm, GroupOrder.class);
		assertThat(this.bindingResult.getFieldError(), is(nullValue()));
		assertThat(violations.size(), is(0));
	}

	@Test
	public void review_completeDayがnull() throws Exception {
		this.reviewForm.setCompleteDay(null);
		this.qrcForm.setReview(this.reviewForm);

		val violations = this.javaxValidator.validate(this.qrcForm, GroupOrder.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "review.completeDay"), is(instanceOf(NotNull.class)));
	}

	@Test
	public void question_questionがnull() throws Exception {
		this.questionForm.setQuestion(null);
		this.qrcForm.setQuestion(this.questionForm);

		val violations = this.javaxValidator.validate(this.qrcForm, GroupOrder.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "question.question"), is(instanceOf(NotBlank.class)));
	}

	@Test
	public void question_questionが半角スペースのみ() throws Exception {
		this.questionForm.setQuestion(" ");
		this.qrcForm.setQuestion(this.questionForm);

		val violations = this.javaxValidator.validate(this.qrcForm, GroupOrder.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "question.question"), is(instanceOf(NotBlank.class)));
	}

	@Test
	public void question_questionが100001文字() throws Exception {
		this.questionForm.setQuestion(str100001);
		this.qrcForm.setQuestion(this.questionForm);

		val violations = this.javaxValidator.validate(this.qrcForm, GroupOrder.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "question.question"), is(instanceOf(Size.class)));
	}

	@Test
	public void question_answerがnull() throws Exception {
		this.questionForm.setAnswer(null);
		this.qrcForm.setQuestion(this.questionForm);

		val violations = this.javaxValidator.validate(this.qrcForm, GroupOrder.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "question.answer"), is(instanceOf(NotBlank.class)));
	}

	@Test
	public void question_answerが半角スペースのみ() throws Exception {
		this.questionForm.setAnswer(" ");
		this.qrcForm.setQuestion(this.questionForm);

		val violations = this.javaxValidator.validate(this.qrcForm, GroupOrder.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "question.answer"), is(instanceOf(NotBlank.class)));
	}

	@Test
	public void question_answerが100001文字() throws Exception {
		this.questionForm.setAnswer(str100001);
		this.qrcForm.setQuestion(this.questionForm);

		val violations = this.javaxValidator.validate(this.qrcForm, GroupOrder.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "question.answer"), is(instanceOf(Size.class)));
	}

	@Test
	public void question_isLearnedがnull() throws Exception {
		this.questionForm.setIsLearned(null);
		this.qrcForm.setQuestion(this.questionForm);

		val violations = this.javaxValidator.validate(this.qrcForm, GroupOrder.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "question.isLearned"), is(instanceOf(NotNull.class)));
	}

	private Annotation getAnnotation(Set<ConstraintViolation<QuestionReviewCategoryForm>> violations, String path) {
		return violations.stream()
				.filter(cv -> cv.getPropertyPath().toString().equals(path))
				.findFirst()
				.map(cv -> cv.getConstraintDescriptor().getAnnotation())
				.get();
	}

}
