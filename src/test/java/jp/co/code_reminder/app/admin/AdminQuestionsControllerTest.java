package jp.co.code_reminder.app.admin;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.code_reminder.app.question.QuestionForm;
import jp.co.code_reminder.app.question.QuestionReviewCategoryForm;
import jp.co.code_reminder.app.question.ReviewForm;
import jp.co.code_reminder.app.security.WithMockCustomUser;
import jp.co.code_reminder.domain.model.question.Learn;
import jp.co.code_reminder.domain.model.question.Share;
import jp.co.code_reminder.domain.service.question.QuestionReferService;
import jp.co.code_reminder.web.validation.ValidAdvice;
import lombok.val;

@SpringBootTest
@Transactional(rollbackFor = Exception.class)
public class AdminQuestionsControllerTest {

	private MockMvc mockMvc;
	private QuestionReviewCategoryForm qrcForm;
	private QuestionForm questionForm;
	private ReviewForm reviewForm;

	@Autowired
	private AdminQuestionsController target;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private QuestionReferService questionReferService;

	@Autowired
	private ValidAdvice validAdvice;

	@BeforeEach
	public void 初期化処理() throws Exception {
		this.qrcForm = new QuestionReviewCategoryForm();
		this.questionForm = new QuestionForm();
		this.reviewForm = new ReviewForm();

		val resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/");
		resolver.setSuffix(".html");

		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).setViewResolvers(resolver)
				.setControllerAdvice(this.validAdvice)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 全問題一覧画面が表示できるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		var mvcResult = this.mockMvc.perform(get("/admin/questions"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/questions/index"))
				.andReturn();

		var mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qrcFormList = ((List<QuestionReviewCategoryForm>) mav.getModel().get("qrcFormList"));

		val actualListSize = qrcFormList.size();
		val expectListSize = 5;
		assertThat(actualListSize, is(expectListSize));

		@SuppressWarnings("unchecked")
		val page = ((Page<QuestionReviewCategoryForm>) mav.getModel().get("page"));

		val actualPageTotalElement = page.getTotalElements();
		val expectPageTotalElement = 12L;
		assertThat(actualPageTotalElement, is(expectPageTotalElement));

		val actualPageSize = page.getSize();
		val expectPageSize = 5;
		assertThat(actualPageSize, is(expectPageSize));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 問題編集画面が表示できるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.mockMvc.perform(get("/admin/questions/1/edit"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/questions/edit"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 数学カテゴリと紐づいた問題のみが絞り込みが出来るか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		var pageNum = "0";
		//selectedNum = 1 ->数学カテゴリID
		var mvcResult = this.mockMvc.perform(get("/admin/questions?page=" + pageNum + "&selectedCategoryNums=1"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/questions/index"))
				.andReturn();

		var mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qrcFormList = ((List<QuestionReviewCategoryForm>) mav.getModel().get("qrcFormList"));

		val actualListSize = qrcFormList.size();
		val expectListSize = 3;
		assertThat(actualListSize, is(expectListSize));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void rubyカテゴリとpythonカテゴリに紐づいた問題のみが絞り込みが出来るか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		//selectedNum = 6->ruby
		//selectedNum = 7->python
		var pageNum = "0";
		var mvcResult = this.mockMvc
				.perform(get("/admin/questions?page=" + pageNum + "&selectedCategoryNums=6&selectedCategoryNums=7"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/questions/index"))
				.andReturn();

		var mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qrcFormList0 = ((List<QuestionReviewCategoryForm>) mav.getModel().get("qrcFormList"));
		val actualListSize0 = qrcFormList0.size();
		val expectListSize0 = 5;
		assertThat(actualListSize0, is(expectListSize0));

		pageNum = "1";
		mvcResult = this.mockMvc
				.perform(get("/admin/questions?page=" + pageNum + "&selectedCategoryNums=6&selectedCategoryNums=7"))
				.andReturn();

		mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val qrcFormList1 = ((List<QuestionReviewCategoryForm>) mav.getModel().get("qrcFormList"));
		val actualListSize1 = qrcFormList1.size();
		val expectListSize1 = 4;
		assertThat(actualListSize1, is(expectListSize1));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 不正な編集画面のURLを弾いて全問題一覧に戻せるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.mockMvc.perform(get("/admin/questions/13/edit"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/questions"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 問題編集ができるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.questionForm.setId(1);
		this.questionForm.setQuestion("ポビドンヨード");
		this.questionForm.setAnswer("エビデンスが不足しとるで～");
		this.questionForm.setIsLearned(Learn.NotLearned);
		this.questionForm.setIsShared(Share.NotShared);
		this.questionForm.setAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2");

		this.reviewForm.setCompleteDay(365);

		this.qrcForm.setQuestion(questionForm);
		this.qrcForm.setReview(reviewForm);
		this.qrcForm.setCategoryNumList(new ArrayList<Integer>(Arrays.asList(1)));

		this.mockMvc.perform(post("/admin/questions/1/edit").queryParam("update", "update")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/questions"));

		val testQuestion = this.questionReferService.findByQuestionId("1");
		val expectedQuestion = "ポビドンヨード";
		val expectedAnswer = "エビデンスが不足しとるで～";
		val expectedcompleteDay = 365;
		val expectedIsShared = Share.NotShared;

		assertEquals(expectedQuestion, testQuestion.getQuestion().getQuestion());
		assertEquals(expectedAnswer, testQuestion.getQuestion().getAnswer());
		assertEquals(expectedcompleteDay, testQuestion.getReview().getCompleteDay());
		assertEquals(expectedIsShared, testQuestion.getQuestion().getIsShared());
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 問題が削除できるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.mockMvc.perform(post("/admin/questions/1/edit").queryParam("delete", "delete"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/questions"));

		val testQuestions = this.questionReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2");
		val expectedSize = 11;

		assertEquals(expectedSize, testQuestions.size());
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 全て未入力で編集を弾けるか() throws Exception {
		executeScript("/db/insertQuestion.sql");

		this.questionForm.setId(1);
		this.questionForm.setQuestion("");
		this.questionForm.setAnswer("");
		this.questionForm.setIsLearned(null);
		this.questionForm.setIsShared(null);

		this.reviewForm.setCompleteDay(null);

		this.qrcForm.setQuestion(questionForm);
		this.qrcForm.setReview(reviewForm);
		this.qrcForm.setCategoryNumList(null);

		var mvcResult = this.mockMvc.perform(post("/admin/questions/1/edit").queryParam("update", "update")
				.flashAttr("qrcForm", this.qrcForm))
				.andExpect(status().isOk())
				.andExpect(model().attribute("qrcForm", this.qrcForm))
				.andExpect(model().attributeHasFieldErrors("qrcForm", "question.question", "question.answer",
						"question.isLearned", "review.completeDay"))
				.andExpect(model().errorCount(5))
				.andExpect(view().name("admin/questions/edit"))
				.andReturn();

		var actualURI = mvcResult.getRequest().getRequestURI();
		val expectedURI = "/admin/questions/1/edit";
		assertThat(actualURI, is(expectedURI));
	}

	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}
}
