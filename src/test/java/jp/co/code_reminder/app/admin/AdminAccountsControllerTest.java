package jp.co.code_reminder.app.admin;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.code_reminder.app.account.AccountForm;
import jp.co.code_reminder.app.security.WithMockCustomUser;
import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.model.account.RoleName;
import jp.co.code_reminder.domain.service.account.AccountReferService;
import lombok.val;
import ma.glasnost.orika.MapperFactory;

@SpringBootTest
@Transactional(rollbackFor = Exception.class)
public class AdminAccountsControllerTest {

	private MockMvc mockMvc;
	private AccountForm accountForm;

	@Autowired
	private AccountReferService accountReferService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private MapperFactory beanMapperFactory;

	@Autowired
	private AdminAccountsController target;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@BeforeEach
	@WithMockCustomUser(loginId = "kobayasi")
	public void 初期化処理() throws Exception {
		executeScript("/db/insertUsers.sql");
		val account = this.accountReferService.findOne("e034c492-d2c2-449b-a40f-9dea83ebe4c9", "uuid");
		this.accountForm = convertToAccountForm(account);

		val resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/");
		resolver.setSuffix(".html");

		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).setViewResolvers(resolver)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void アカウント一覧画面が表示できるか() throws Exception {
		executeScript("/db/insertUsers.sql");

		val mvcResult = this.mockMvc.perform(get("/admin/accounts"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/index"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val accountFormList = ((List<AccountForm>) mav.getModel().get("accountFormList"));

		val actualListSize = accountFormList.size();
		val expectListSize = 5;
		assertThat(actualListSize, is(expectListSize));

		@SuppressWarnings("unchecked")
		val page = ((Page<AccountForm>) mav.getModel().get("page"));

		val actualPageTotalElement = page.getTotalElements();
		val expectPageTotalElement = 6L;
		assertThat(actualPageTotalElement, is(expectPageTotalElement));

		val actualPageSize = page.getSize();
		val expectPageSize = 5;
		assertThat(actualPageSize, is(expectPageSize));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void アカウント編集画面が表示できるか() throws Exception {

		executeScript("/db/insertUsers.sql");

		this.mockMvc.perform(get("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andReturn();
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 内容変更無しで更新ボタン押下() throws Exception {

		executeScript("/db/insertUsers.sql");

		this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/accounts"))
				.andExpect(flash().attribute("success", "更新に成功しました"))
				.andReturn();
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを変更して更新ボタン押下() throws Exception {

		executeScript("/db/insertUsers.sql");
		this.accountForm.setLoginId("hirachan");

		this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/accounts"))
				.andExpect(flash().attribute("success", "更新に成功しました"))
				.andReturn();

		val expectedLoginId = "hirachan";
		val actualLoginId = this.accountReferService.findOne("hirachan", "login_id").getLoginId();

		assertEquals(expectedLoginId, actualLoginId);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードを変更して更新ボタン押下() throws Exception {
		this.accountForm.setPassword("testpassword");
		this.accountForm.setConfirmPassword("testpassword");

		this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/accounts"))
				.andExpect(flash().attribute("success", "更新に成功しました"));

		val expectedPassword = "testpassword";
		val actualPassword = this.accountReferService.findOne("hirata", "login_id").getPassword();

		assertTrue(this.passwordEncoder.matches(expectedPassword, actualPassword));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ハンドルネームを変更して更新ボタン押下() throws Exception {
		this.accountForm.setName("小林祐也");

		this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/accounts"))
				.andExpect(flash().attribute("success", "更新に成功しました"));

		val expectedName = "小林祐也";
		val actualName = this.accountReferService.findOne("hirata", "login_id").getName();

		assertEquals(expectedName, actualName);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void メールアドレスを変更して更新ボタン押下() throws Exception {
		this.accountForm.setMail("kobayashitakahiro@yahoo.co.jp");
		this.accountForm.setConfirmMail("kobayashitakahiro@yahoo.co.jp");

		this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/accounts"))
				.andExpect(flash().attribute("success", "更新に成功しました"));

		val expectedMail = "kobayashitakahiro@yahoo.co.jp";
		val actualMail = this.accountReferService.findOne("hirata", "login_id").getMail();

		assertEquals(expectedMail, actualMail);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 確認用メールアドレスを空にして更新ボタン押下() throws Exception {
		this.accountForm.setConfirmMail("");

		this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/accounts"))
				.andExpect(flash().attribute("success", "更新に成功しました"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードと確認用パスワードを空にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("");

		this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/accounts"))
				.andExpect(flash().attribute("success", "更新に成功しました"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインアカウント以外の権限を変更する() throws Exception {

		val mvcResult = this.mockMvc.perform(get("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val roleNameList = ((List<String>) mav.getModel().get("roleNameList"));

		val expectRoleNameListSize = 2;
		val actualRoleNameListSize = roleNameList.size();

		assertEquals(expectRoleNameListSize, actualRoleNameListSize);

		this.accountForm.setRoleName(RoleName.ROLE_ADMIN);

		this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/accounts"))
				.andExpect(flash().attribute("success", "更新に成功しました"));

		val expectRoleName = RoleName.ROLE_ADMIN;
		val actualRoleName = this.accountReferService.findOne("hirata", "login_id").getRoleName();

		assertEquals(expectRoleName, actualRoleName);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインアカウントの権限を変更する() throws Exception {

		val mvcResult = this.mockMvc.perform(get("/admin/accounts/39479f56-c0c4-43e3-9b84-e67406b66fc2/edit"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val roleNameList = ((List<String>) mav.getModel().get("roleNameList"));

		val expectRoleNameListSize = 1;
		val actualRoleNameListSize = roleNameList.size();

		assertEquals(expectRoleNameListSize, actualRoleNameListSize);

		val account = this.accountReferService.findOne("kobayasi", "login_id");
		this.accountForm = convertToAccountForm(account);
		this.accountForm.setRoleName(RoleName.ROLE_USER);

		this.mockMvc.perform(post("/admin/accounts/39479f56-c0c4-43e3-9b84-e67406b66fc2/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/admin/accounts"))
				.andExpect(flash().attribute("success", "更新に成功しました"));

		val expectRoleName = RoleName.ROLE_ADMIN;
		val actualRoleName = this.accountReferService.findOne("kobayasi", "login_id").getRoleName();

		assertEquals(expectRoleName, actualRoleName);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを空にして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードを空にして更新ボタン押下() throws Exception {
		this.accountForm.setConfirmPassword("password");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "password"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "パスワードを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 確認用パスワードを空にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("password");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "確認用パスワードを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ハンドルネーム空にして更新ボタン押下() throws Exception {
		this.accountForm.setName("");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "name"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ハンドルネームを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void メールアドレスを空にして更新ボタン押下() throws Exception {
		this.accountForm.setMail("");
		this.accountForm.setConfirmMail("kobayashitakahiro@yahoo.co.jp");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "メールアドレスを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを5文字以下にして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("kobas");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDは6文字以上、20文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを21文字以上にして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("ABCDEFGH1JKLMNOPQRS3U");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDは6文字以上、20文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードと確認用パスワードを5文字以下にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("ABCDE");
		this.accountForm.setConfirmPassword("ABCDE");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualPasswordErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedPasswordErrorMessage = "パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedPasswordErrorMessage, actualPasswordErrorMessage);

		val actualConfirmPasswordErrorMessage = errors.getAllErrors().get(1).getDefaultMessage();
		val expectedConfirmPasswordErrorMessage = "確認用パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedConfirmPasswordErrorMessage, actualConfirmPasswordErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードと確認用パスワードを21文字以上にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("ABCDEFGHIJKLMNOPQRSTU");
		this.accountForm.setConfirmPassword("ABCDEFGHIJKLMNOPQRSTU");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualPasswordErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedPasswordErrorMessage = "パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedPasswordErrorMessage, actualPasswordErrorMessage);

		val actualConfirmPasswordErrorMessage = errors.getAllErrors().get(1).getDefaultMessage();
		val expectedConfirmPasswordErrorMessage = "確認用パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedConfirmPasswordErrorMessage, actualConfirmPasswordErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ハンドルネームを1文字以下にして更新ボタン押下() throws Exception {
		this.accountForm.setName("A");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "name"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ハンドルネームは2文字以上、10文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ハンドルネームを11文字以上にして更新ボタン押下() throws Exception {
		this.accountForm.setName("ABCDEFGHIJK");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "name"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ハンドルネームは2文字以上、10文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void メールアドレスにEmail形式ではない文字列にして更新ボタン押下() throws Exception {
		this.accountForm.setMail("takahiroyahoo.co.jp");
		this.accountForm.setConfirmMail("takahiroyahoo.co.jp");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "メールアドレスはメールアドレス形式で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 確認用メールアドレスにEmail形式ではない文字列にして更新ボタン押下() throws Exception {
		this.accountForm.setConfirmMail("takahiroyahoo.co.jp");
		this.accountForm.setMail("takahiro@yahoo.co.jp");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmMail"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "確認用メールアドレスはメールアドレス形式で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを半角英数字以外で更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("小林たかひろざえもん");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDは半角英数字で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードと確認用パスワードを半角英数字以外で更新ボタン押下() throws Exception {
		this.accountForm.setPassword("小林たかひろざえもん");
		this.accountForm.setConfirmPassword("小林たかひろざえもん");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "password"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "パスワードは記号を含む半角文字で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void メールアドレスと確認用メールアドレスが異なった値で更新ボタン押下() throws Exception {
		this.accountForm.setMail("taka@yahoo.co.jp");
		this.accountForm.setConfirmMail("taka@yahoo.co.jpp");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたメールアドレスと確認用メールアドレスが異なっています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードと確認用パスワードが異なった値で更新ボタン押下() throws Exception {
		this.accountForm.setPassword("password");
		this.accountForm.setConfirmPassword("passwords");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "password"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたパスワードと確認用パスワードが異なっています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを登録されているログインIDにして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("maruyama");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたログインIDは既に登録されています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void メールアドレスを登録されているメールアドレスにして更新ボタン押下() throws Exception {
		this.accountForm.setMail("maru@yahoo.co.jp");
		this.accountForm.setConfirmMail("maru@yahoo.co.jp");

		val mvcResult = this.mockMvc.perform(post("/admin/accounts/e034c492-d2c2-449b-a40f-9dea83ebe4c9/edit")
				.queryParam("update", "update")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたメールアドレスは既に登録されています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDでユーザ検索ができるか() throws Exception {
		executeScript("/db/insertUsers.sql");

		val mvcResult = this.mockMvc.perform(get("/admin/accounts")
				.param("searchLoginId", "kobayasi"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/index"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val accountFormList = ((List<AccountForm>) mav.getModel().get("accountFormList"));
		val actualListSize = accountFormList.size();
		val expectListSize = 3;
		assertThat(actualListSize, is(expectListSize));

		val actualLoginId = (mav.getModel().get("searchLoginId"));
		val expectLoginId = "kobayasi";
		assertThat(actualLoginId, is(expectLoginId));
	}

	@Test
	public void 空白で検索するとすべてのユーザーを表示する() throws Exception {
		executeScript("/db/insertUsers.sql");

		val mvcResult = this.mockMvc.perform(get("/admin/accounts")
				.param("searchLoginId", ""))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/index"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val accountFormList = ((List<AccountForm>) mav.getModel().get("accountFormList"));
		val actualListSize = accountFormList.size();
		val expectListSize = 5;
		assertThat(actualListSize, is(expectListSize));
	}

	@Test
	public void 存在しないログインIDで検索するとユーザーを表示しない() throws Exception {
		executeScript("/db/insertUsers.sql");

		val mvcResult = this.mockMvc.perform(get("/admin/accounts")
				.param("searchLoginId", "test"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/accounts/index"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val accountFormList = ((List<AccountForm>) mav.getModel().get("accountFormList"));
		val actualListSize = accountFormList.size();
		val expectListSize = 0;
		assertThat(actualListSize, is(expectListSize));

		val actualLoginId = (mav.getModel().get("searchLoginId"));
		val expectLoginId = "test";
		assertThat(actualLoginId, is(expectLoginId));
	}

	//SQL文を実行
	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

	private AccountForm convertToAccountForm(final Account account) {
		val beanMapper = this.beanMapperFactory.getMapperFacade(Account.class, AccountForm.class);
		account.setPassword("");
		return beanMapper.map(account);
	}
}
