package jp.co.code_reminder.app.forgot_pass;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.lang.annotation.Annotation;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import jp.co.code_reminder.core.validator.account.ConfirmPassword;
import jp.co.code_reminder.core.validator.order.GroupOrder;
import jp.co.code_reminder.core.validator.order.ValidGroup1;
import jp.co.code_reminder.core.validator.order.ValidGroup3;
import jp.co.code_reminder.core.validator.order.ValidGroup4;
import lombok.val;

@SpringBootTest
public class ResetPassFormTest {

	private ResetPassForm rpForm = new ResetPassForm("39479f56-c0c4-43e3-9b84-e67406b66fc2");
	private BindingResult bindingResult = new BindException(this.rpForm, "resetPassForm");
	private Validator validator;

	@BeforeEach
	public void 値をセット() throws Exception {
		val validatorFactory = Validation.buildDefaultValidatorFactory();
		this.validator = validatorFactory.getValidator();
		this.rpForm.setPassword("password");
		this.rpForm.setConfirmPassword("password");
	}

	@Test
	public void エラー無し() throws Exception {
		val violations = this.validator.validate(this.rpForm, GroupOrder.class);
		assertThat(this.bindingResult.getFieldError(), is(nullValue()));
		assertThat(violations.size(), is(0));
	}

	@Test
	public void passwordがBlank() throws Exception {
		this.rpForm.setPassword("");
		val violations = this.validator.validate(this.rpForm, ValidGroup1.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "password"), is(instanceOf(NotBlank.class)));
	}

	@Test
	public void passwordが全角スペース() throws Exception {
		this.rpForm.setPassword("　　　　　　");
		val violations = this.validator.validate(this.rpForm, ValidGroup3.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "password"), is(instanceOf(Pattern.class)));
	}

	@Test
	public void confirmPasswordがBlank() throws Exception {
		this.rpForm.setConfirmPassword("");
		val violations = this.validator.validate(this.rpForm, ValidGroup1.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "confirmPassword"), is(instanceOf(NotBlank.class)));
	}

	@Test
	public void confirmPasswordが全角スペース() throws Exception {
		this.rpForm.setConfirmPassword("　　　　　　");
		val violations = this.validator.validate(this.rpForm, ValidGroup3.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "confirmPassword"), is(instanceOf(Pattern.class)));
	}

	@Test
	public void 誤ったパスワードを入力() throws Exception {
		this.rpForm.setPassword("aaaaaa");
		this.rpForm.setConfirmPassword("bbbbbb");
		val violations = this.validator.validate(this.rpForm, ValidGroup4.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "password"), is(instanceOf(ConfirmPassword.class)));
	}

	private Annotation getAnnotation(Set<ConstraintViolation<ResetPassForm>> violations, String path) {
		return violations.stream()
				.filter(cv -> cv.getPropertyPath().toString().equals(path))
				.findFirst()
				.map(cv -> cv.getConstraintDescriptor().getAnnotation())
				.get();
	}
}
