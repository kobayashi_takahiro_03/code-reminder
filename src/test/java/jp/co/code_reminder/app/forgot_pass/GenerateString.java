package jp.co.code_reminder.app.forgot_pass;

import org.apache.commons.lang3.RandomStringUtils;

public class GenerateString {

	public final static String str100000 = RandomStringUtils.randomAlphabetic(100000);
	public final static String str100001 = RandomStringUtils.randomAlphabetic(100001);

}
