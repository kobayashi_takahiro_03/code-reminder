package jp.co.code_reminder.app.forgot_pass;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.code_reminder.domain.service.account.AccountReferService;
import lombok.val;

@SpringBootTest
@Transactional(rollbackFor = Exception.class)
public class ForgotPassControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private AccountReferService accountReferService;

	@Autowired
	private PasswordEncoder pwEncoder;

	@Autowired
	private ForgotPassController target;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@BeforeEach
	public void 初期化処理() throws Exception {
		val resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/");
		resolver.setSuffix(".html");
		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).setViewResolvers(resolver).build();
	}

	@Test
	public void メールアドレス入力画面が表示出来るか() throws Exception {
		this.mockMvc.perform(get("/forgotpass"))
				.andExpect(status().isOk())
				.andExpect(view().name("forgotpass/index"));
	}

	@Test
	public void 新規パスワード登録画面が表示出来るか() throws Exception {
		executeScript("/db/insertForgotPass.sql");
		this.mockMvc.perform(get("/forgotpass/validate/39479f56-c0c4-43e3-9b84-e67406b66fc2"))
				.andExpect(status().isOk())
				.andExpect(view().name("forgotpass/edit"));
	}

	@Test
	public void 誤ったパスワードを入れても画面遷移しないか() throws Exception {
		executeScript("/db/insertUsers.sql");
		executeScript("/db/insertForgotPass.sql");

		val password = "aaaaaa";
		val wrongPassword = "bbbbbb";

		this.mockMvc.perform(get("/forgotpass/validate/39479f56-c0c4-43e3-9b84-e67406b66fc2"))
				.andExpect(status().isOk());

		this.mockMvc.perform(post("/forgotpass/validate/39479f56-c0c4-43e3-9b84-e67406b66fc2")
				.param("password", password)
				.param("confirmPassword", wrongPassword))
				.andExpect(status().isOk())
				.andExpect(view().name("forgotpass/edit"));
	}

	@Test
	public void パスワードが変更できているか() throws Exception {

		executeScript("/db/insertUsers.sql");
		executeScript("/db/insertForgotPass.sql");

		val rawPassword = "aaaaaa";
		val mail = "takachanman@yahoo.co.jp";

		this.mockMvc.perform(post("/forgotpass/validate/39479f56-c0c4-43e3-9b84-e67406b66fc2")
				.param("password", rawPassword)
				.param("confirmPassword", rawPassword))
				.andExpect(status().isFound())
				.andExpect(redirectedUrl("/login"));

		val testAccount = this.accountReferService.findOne(mail, "mail");
		val hashedPassword = testAccount.getPassword();
		assertTrue(this.pwEncoder.matches(rawPassword, hashedPassword));
	}

	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

}
