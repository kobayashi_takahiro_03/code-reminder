package jp.co.code_reminder.app.forgot_pass;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.lang.annotation.Annotation;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import jp.co.code_reminder.core.validator.order.GroupOrder;
import jp.co.code_reminder.core.validator.order.ValidGroup1;
import jp.co.code_reminder.core.validator.order.ValidGroup2;
import lombok.val;

@SpringBootTest
public class ForgotPassFormTest {

	private ForgotPassForm fpForm = new ForgotPassForm();
	private BindingResult bindingResult = new BindException(this.fpForm, "forgotPassForm");
	private Validator validator;

	@BeforeEach
	public void 値をセット() throws Exception {
		val validatorFactory = Validation.buildDefaultValidatorFactory();
		this.validator = validatorFactory.getValidator();
		this.fpForm.setMail("wantailang0223@gmail.com");
	}

	@Test
	public void エラー無し() throws Exception {
		val violations = this.validator.validate(this.fpForm, GroupOrder.class);
		assertThat(this.bindingResult.getFieldError(), is(nullValue()));
		assertThat(violations.size(), is(0));
	}

	@Test
	public void mailがBlank() throws Exception {
		this.fpForm.setMail("");
		val violations = this.validator.validate(this.fpForm, ValidGroup1.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "mail"), is(instanceOf(NotBlank.class)));
	}

	@Test
	public void メールの書式ではない() throws Exception {
		this.fpForm.setMail("takahiroyahoo.co.jp");
		val violations = this.validator.validate(this.fpForm, ValidGroup2.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "mail"), is(instanceOf(Email.class)));
	}

	@Test
	public void メールアドレスローカル部が65文字以上() throws Exception {
		this.fpForm.setMail("ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLM@yahoo.co.jp");
		val violations = this.validator.validate(this.fpForm, ValidGroup2.class);
		assertThat(violations.size(), is(1));
		assertThat(getAnnotation(violations, "mail"), is(instanceOf(Email.class)));
	}

	private Annotation getAnnotation(Set<ConstraintViolation<ForgotPassForm>> violations, String path) {
		return violations.stream()
				.filter(cv -> cv.getPropertyPath().toString().equals(path))
				.findFirst()
				.map(cv -> cv.getConstraintDescriptor().getAnnotation())
				.get();
	}

}
