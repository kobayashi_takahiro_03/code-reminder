package jp.co.code_reminder.app.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import jp.co.code_reminder.domain.service.security.AccountDetailService;
import lombok.val;


public class WithMockCustomUserSecurityContextFactory implements WithSecurityContextFactory<WithMockCustomUser> {

	@Autowired
	private AccountDetailService accountDetailService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

    @Override
	@WithMockCustomUser(loginId = "kobayasi")
    public SecurityContext createSecurityContext(WithMockCustomUser account) {
    	executeScript("/db/insertUsers.sql");

        val context = SecurityContextHolder.createEmptyContext();
        val principal = this.accountDetailService.loadUserByUsername(account.loginId());
        val auth = new UsernamePasswordAuthenticationToken(principal, principal.getPassword(), Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN")));
        context.setAuthentication(auth);
        return context;
    }

    private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}
}