package jp.co.code_reminder.app.security;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import lombok.val;

@SpringBootTest
public class SecurityTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@BeforeEach
	public void 初期化処理() throws Exception {
		this.mockMvc = MockMvcBuilders
				.webAppContextSetup(this.context)
				.apply(springSecurity())
				.build();
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	public void DB上に存在するユーザでログインできる() throws Exception {
		executeScript("/db/insertUsers.sql");

		this.mockMvc.perform(formLogin("/login")
				.user("loginId", "kobayasi")
				.password("password", "password"))
				.andExpect(status().is2xxSuccessful())
				.andExpect(forwardedUrl("/"));
	}

	@Test
	@WithMockUser
	@Transactional(rollbackFor = Exception.class)
	public void DB上に存在しないユーザでログインできない() throws Exception {
		executeScript("/db/insertUsers.sql");

		this.mockMvc.perform(formLogin("/login")
				.user("loginId", "notexistuser")
				.password("password", "hirahira"))
				.andExpect(status().is2xxSuccessful())
				.andExpect(forwardedUrl("/login-error"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	@Transactional(rollbackFor = Exception.class)
	public void ログアウト処理が正常に行われるか() throws Exception {
		executeScript("/db/insertUsers.sql");

		this.mockMvc.perform(formLogin("/login")
				.user("loginId", "kobayasi")
				.password("password", "password"))
				.andExpect(status().is2xxSuccessful())
				.andExpect(forwardedUrl("/"));
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	public void ログイン失敗三回目でアカウントロックが掛かってログイン不可になるか() throws Exception {
		executeScript("/db/insertUsers.sql");

		this.mockMvc.perform(formLogin("/login")
				.user("loginId", "kobayasi")
				.password("password", "password"))
				.andExpect(status().is2xxSuccessful())
				.andExpect(forwardedUrl("/"));

		this.mockMvc.perform(formLogin("/login")
				.user("loginId", "kobayasi")
				.password("password", "wrongPassword"))
				.andExpect(status().is2xxSuccessful())
				.andExpect(forwardedUrl("/login-error"));

		this.mockMvc.perform(formLogin("/login")
				.user("loginId", "kobayasi")
				.password("password", "wrongPassword"))
				.andExpect(status().is2xxSuccessful())
				.andExpect(forwardedUrl("/login-error"));

		this.mockMvc.perform(formLogin("/login")
				.user("loginId", "kobayasi")
				.password("password", "wrongPassword"))
				.andExpect(status().is2xxSuccessful())
				.andExpect(forwardedUrl("/login-error"));

		this.mockMvc.perform(formLogin("/login")
				.user("loginId", "kobayasi")
				.password("password", "password"))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(forwardedUrl("/login-error"));
	}

	@Test
	@WithMockUser
	@Transactional(rollbackFor = Exception.class)
	public void 認可されていないユーザーはユーザー一覧を開けない() throws Exception {
		executeScript("/db/insertUsers.sql");
		this.mockMvc.perform(get("/admin")).andExpect(status().isForbidden());
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	@Transactional(rollbackFor = Exception.class)
	public void 認可されているユーザーはユーザー一覧を開ける() throws Exception {
		executeScript("/db/insertUsers.sql");
		this.mockMvc.perform(get("/admin/accounts")).andExpect(status().isOk());
	}

	@Test
	@WithMockUser
	@Transactional(rollbackFor = Exception.class)
	public void 認可されていないユーザーはユーザ詳細を開けない() throws Exception {
		executeScript("/db/insertUsers.sql");
		this.mockMvc.perform(get("/admin/accounts/695d36e1-4bbe-4f4a-b598-02be3ac312f7/edit"))
				.andExpect(status().isForbidden());
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	@Transactional(rollbackFor = Exception.class)
	public void 認可されているユーザーはユーザー詳細を開ける() throws Exception {
		executeScript("/db/insertUsers.sql");
		this.mockMvc.perform(get("/admin/accounts/39479f56-c0c4-43e3-9b84-e67406b66fc2/edit"))
				.andExpect(status().isOk());
	}

	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}
}