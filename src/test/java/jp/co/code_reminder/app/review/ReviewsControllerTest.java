package jp.co.code_reminder.app.review;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.code_reminder.app.question.QuestionForm;
import jp.co.code_reminder.app.question.QuestionReviewCategoryForm;
import jp.co.code_reminder.app.security.WithMockCustomUser;
import jp.co.code_reminder.domain.model.question.QuestionReviewCategory;
import jp.co.code_reminder.domain.service.question.QuestionReferService;
import lombok.val;
import ma.glasnost.orika.MapperFactory;

@SpringBootTest
public class ReviewsControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private ReviewsController target;

	@Autowired
	private QuestionReferService questionReferService;

	@Autowired
	private MapperFactory beanMapperFactory;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@BeforeEach
	public void 初期化処理() throws Exception {
		val resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/");
		resolver.setSuffix(".html");

		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).setViewResolvers(resolver)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 学習画面のトップページが表示出来るか() throws Exception {
		executeScript("/db/insertReview.sql");

		this.mockMvc.perform(get("/reviews"))
				.andExpect(status().isOk())
				.andExpect(view().name("review/index"));

	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 新たに学習6＿復習1で表示されているか() throws Exception {
		executeScript("/db/insertReview.sql");

		val mvcResult = this.mockMvc.perform(get("/reviews"))
				.andExpect(status().isOk())
				.andExpect(view().name("review/index"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val newQuestionCount = mav.getModel().get("newQuestionCount");
		val reviewQuestionCount = mav.getModel().get("reviewQuestionCount");

		assertEquals(6, newQuestionCount);
		assertEquals(1, reviewQuestionCount);
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 学習項目が0件になっていること() throws Exception {
		executeScript("/db/deleteReview.sql");

		val mvcResult = this.mockMvc.perform(get("/reviews"))
				.andExpect(status().isOk())
				.andExpect(view().name("review/index"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val newQuestionCount = mav.getModel().get("newQuestionCount");
		val reviewQuestionCount = mav.getModel().get("reviewQuestionCount");

		assertEquals(0, newQuestionCount);
		assertEquals(0, reviewQuestionCount);
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 問題が表示できるか() throws Exception {
		executeScript("/db/insertReview.sql");
		val qrcForm = getQrcForm("1").getQuestion();

		this.mockMvc.perform(get("/reviews/question"))
				.andExpect(status().isOk())
				.andExpect(view().name("review/edit"))
				.andExpect(model().attribute("question", qrcForm))
				.andReturn();
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 回答結果が表示できるか_余裕() throws Exception {
		executeScript("/db/insertReview.sql");

		val mvcResult = this.mockMvc.perform(post("/reviews/question")
				.param("score", "難しい"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/reviews/answer"))
				.andReturn();

		val expectedQuestionFormId = getQrcForm("1").getQuestion().getId();
		val acutualQuestionFromId = ((QuestionForm) mvcResult.getFlashMap().get("question")).getId();

		assertEquals(expectedQuestionFormId, acutualQuestionFromId);

		mvcResult.getRequest().getSession().invalidate();
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 回答結果が表示できるか_普通() throws Exception {

		executeScript("/db/insertReview.sql");

		val mvcResult = this.mockMvc.perform(post("/reviews/question")
				.param("score", "難しい"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/reviews/answer"))
				.andReturn();

		val expectedQuestionFormId = getQrcForm("1").getQuestion().getId();
		val acutualQuestionFromId = ((QuestionForm) mvcResult.getFlashMap().get("question")).getId();

		assertEquals(expectedQuestionFormId, acutualQuestionFromId);

		mvcResult.getRequest().getSession().invalidate();
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 回答結果が表示できるか_難しい() throws Exception {

		executeScript("/db/insertReview.sql");

		val mvcResult = this.mockMvc.perform(post("/reviews/question")
				.param("score", "難しい"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/reviews/answer"))
				.andReturn();

		val expectedQuestionFormId = getQrcForm("1").getQuestion().getId();
		val acutualQuestionFromId = ((QuestionForm) mvcResult.getFlashMap().get("question")).getId();

		assertEquals(expectedQuestionFormId, acutualQuestionFromId);

		mvcResult.getRequest().getSession().invalidate();
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 回答結果が表示できるか_分からなかった() throws Exception {

		executeScript("/db/insertReview.sql");

		val mvcResult = this.mockMvc.perform(post("/reviews/question")
				.param("score", "難しい"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/reviews/answer"))
				.andReturn();

		val expectedQuestionFormId = getQrcForm("1").getQuestion().getId();
		val acutualQuestionFromId = ((QuestionForm) mvcResult.getFlashMap().get("question")).getId();

		assertEquals(expectedQuestionFormId, acutualQuestionFromId);

		mvcResult.getRequest().getSession().invalidate();
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void 回答結果が表示できるか_規定値以外() throws Exception {

		executeScript("/db/insertReview.sql");

		this.mockMvc.perform(get("/"));

		val mvcResult = this.mockMvc.perform(post("/reviews/question")
				.param("score", "難しい"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/reviews/answer"))
				.andReturn();

		val expectedQuestionFormId = getQrcForm("1").getQuestion().getId();
		val acutualQuestionFromId = ((QuestionForm) mvcResult.getFlashMap().get("question")).getId();

		assertEquals(expectedQuestionFormId, acutualQuestionFromId);

		mvcResult.getRequest().getSession().invalidate();
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	@WithMockCustomUser(loginId = "kobayasi")
	public void urlにanswerを入力するとreviewに飛ばされる() throws Exception {
		this.mockMvc.perform(get("/reviews/answer"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/reviews"))
				.andReturn();
	}

	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

	private QuestionReviewCategoryForm getQrcForm(final String id) {
		val qrc = this.questionReferService.findByQuestionId(id);
		return this.beanMapperFactory.getMapperFacade(QuestionReviewCategory.class, QuestionReviewCategoryForm.class)
				.map(qrc);
	}

}