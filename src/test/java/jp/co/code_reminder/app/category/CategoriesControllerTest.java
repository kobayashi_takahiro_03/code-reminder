package jp.co.code_reminder.app.category;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.code_reminder.app.security.WithMockCustomUser;
import jp.co.code_reminder.domain.model.category.Category;
import jp.co.code_reminder.domain.service.category.CategoryReferService;
import lombok.val;
import ma.glasnost.orika.MapperFactory;

@SpringBootTest
@Transactional(rollbackFor = Exception.class)
public class CategoriesControllerTest {

	private MockMvc mockMvc;

	private CategoryForm categoryForm;

	@Autowired
	private MapperFactory beanMapperFactory;

	@Autowired
	private CategoryReferService categoryReferService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private CategoriesController target;

	@BeforeEach
	public void 初期化処理() throws Exception {
		this.categoryForm = new CategoryForm();
		this.categoryForm.setAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2");

		val resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/");
		resolver.setSuffix(".html");

		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).setViewResolvers(resolver)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ一覧画面が表示出来るか() throws Exception {
		executeScript("/db/insertCategories.sql");

		val mvcResult = this.mockMvc.perform(get("/categories"))
				.andExpect(status().isOk())
				.andExpect(view().name("category/index"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val compareNum = new int[] { 7, 6 };
		@SuppressWarnings("unchecked")
		val categoryList = ((List<CategoryForm>) mav.getModel().get("categoryFormList"));

		for (int i = 0; i < compareNum.length; i++) {
			assertEquals(compareNum[i], categoryList.get(i).getId());
		}
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ編集画面が表示出来るか() throws Exception {
		executeScript("/db/insertCategories.sql");

		val mvcResult = this.mockMvc.perform(get("/categories/edit"))
				.andExpect(status().isOk())
				.andExpect(view().name("category/edit"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val compareNum = new int[] { 7, 6 };
		val categoryForms = ((CategoryForms) mav.getModel().get("categoryForms"));

		for (int i = 0; i < compareNum.length; i++) {
			assertEquals(compareNum[i], categoryForms.getCategoryForm().get(i).getId());
		}
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ新規登録が出来るか() throws Exception {
		executeScript("/db/insertCategories.sql");

		this.categoryForm.setName("テストカテゴリ");

		this.mockMvc.perform(post("/categories/edit").queryParam("create", "create")
				.flashAttr("categoryForm", this.categoryForm))
				.andExpect(status().isFound())
				.andExpect(flash().attribute("success", "登録に成功しました"))
				.andExpect(view().name("redirect:/categories/edit"));

		val category = this.categoryReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2");
		val actualCategory = category.get(0).getName();
		val expectCategory = "テストカテゴリ";
		assertEquals(expectCategory, actualCategory);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ編集が出来るか() throws Exception {
		executeScript("/db/insertCategories.sql");

		val categoryFormList = convertCategoryFormList(
				this.categoryReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2"));
		val categoryForms = new CategoryForms(categoryFormList);

		this.mockMvc.perform(post("/categories/edit").queryParam("update", "update")
				.flashAttr("categoryForms", categoryForms)
				.param("categoryForm[0].name", "テストカテゴリ"))
				.andExpect(status().isFound())
				.andExpect(flash().attribute("success", "更新に成功しました"))
				.andExpect(view().name("redirect:/categories/edit"));

		val actualCategory = categoryForms.getCategoryForm().get(0).getName();
		val expectCategory = "テストカテゴリ";
		assertEquals(expectCategory, actualCategory);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ削除が出来るか() throws Exception {
		executeScript("/db/insertCategories.sql");

		var categoryFormList = convertCategoryFormList(
				this.categoryReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2"));
		val categoryForms = new CategoryForms(categoryFormList);

		this.mockMvc.perform(post("/categories/edit").queryParam("delete", "delete")
				.flashAttr("categoryForms", categoryForms)
				.param("deleteNum", "7"))
				.andExpect(status().isFound())
				.andExpect(flash().attribute("success", "削除に成功しました"))
				.andExpect(view().name("redirect:/categories/edit"));

		categoryFormList = convertCategoryFormList(
				this.categoryReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2"));
		categoryForms.setCategoryForm(categoryFormList);
		val actualCategory = categoryForms.getCategoryForm().get(0).getId();
		val expectCategory = 6;
		assertEquals(expectCategory, actualCategory);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ新規登録時_カテゴリ名を空にする() throws Exception {
		executeScript("/db/insertCategories.sql");

		val mvcResult = this.mockMvc.perform(post("/categories/edit").queryParam("create", "create")
				.param("name", ""))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(view().name("category/edit"))
				.andExpect(model().attributeExists("categoryForm"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "categoryForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectErrorMessage = "カテゴリを入力してください";
		assertThat(actualErrorMessage, is(expectErrorMessage));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ新規登録時_カテゴリ名を51文字にする() throws Exception {
		executeScript("/db/insertCategories.sql");

		val mvcResult = this.mockMvc.perform(post("/categories/edit").queryParam("create", "create")
				.param("name", "ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでとどなにぬねのはばぱひび"))
				.andExpect(status().isOk())
				.andExpect(view().name("category/edit"))
				.andExpect(model().attributeExists("categoryForm"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "categoryForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectErrorMessage = "カテゴリは50文字以下で入力してください";
		assertThat(actualErrorMessage, is(expectErrorMessage));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ編集時_カテゴリ名を空文字にする() throws Exception {
		executeScript("/db/insertCategories.sql");

		val categoryFormList = convertCategoryFormList(
				this.categoryReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2"));
		val categoryForms = new CategoryForms(categoryFormList);

		val mvcResult = this.mockMvc.perform(post("/categories/edit").queryParam("update", "update")
				.flashAttr("categoryForms", categoryForms)
				.param("categoryForm[0].name", ""))
				.andExpect(status().isOk())
				.andExpect(view().name("category/edit"))
				.andExpect(model().attributeExists("categoryForms"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "categoryForms");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectErrorMessage = "カテゴリを入力してください";
		assertThat(actualErrorMessage, is(expectErrorMessage));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ編集時_カテゴリ名を51文字にする() throws Exception {
		executeScript("/db/insertCategories.sql");

		val categoryFormList = convertCategoryFormList(
				this.categoryReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2"));
		val categoryForms = new CategoryForms(categoryFormList);

		val mvcResult = this.mockMvc.perform(post("/categories/edit").queryParam("update", "update")
				.flashAttr("categoryForms", categoryForms)
				.param("categoryForm[0].name", ""))
				.andExpect(status().isOk())
				.andExpect(view().name("category/edit"))
				.andExpect(model().attributeExists("categoryForms"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "categoryForms");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectErrorMessage = "カテゴリを入力してください";
		assertThat(actualErrorMessage, is(expectErrorMessage));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ編集時_カテゴリIDを他ユーザー固有のIDに変更する() throws Exception {
		executeScript("/db/insertCategories.sql");

		val categoryFormList = convertCategoryFormList(
				this.categoryReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2"));
		val categoryForms = new CategoryForms(categoryFormList);

		this.mockMvc.perform(post("/categories/edit").queryParam("update", "update")
				.flashAttr("categoryForms", categoryForms)
				.param("categoryForm[0].name", "テストカテゴリ")
				.param("categoryForm[0].id", "3"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/categories/edit"))
				.andExpect(flash().attribute("error", "不正なパラメーターです"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ削除時_チェックボックスを空にして削除ボタンを押す() throws Exception {
		executeScript("/db/insertCategories.sql");

		val categoryFormList = convertCategoryFormList(
				this.categoryReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2"));
		val categoryForms = new CategoryForms(categoryFormList);

		this.mockMvc.perform(post("/categories/edit").queryParam("delete", "delete")
				.flashAttr("categoryForms", categoryForms)
				.param("deleteNum", ""))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/categories/edit"))
				.andExpect(flash().attribute("error", "削除するカテゴリーを選択してください"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリ削除時_カテゴリIDを他ユーザー固有のIDに変更する() throws Exception {
		executeScript("/db/insertCategories.sql");

		val categoryFormList = convertCategoryFormList(
				this.categoryReferService.findByAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2"));
		val categoryForms = new CategoryForms(categoryFormList);

		this.mockMvc.perform(post("/categories/edit").queryParam("delete", "delete")
				.flashAttr("categoryForms", categoryForms)
				.param("deleteNum", "3"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/categories/edit"))
				.andExpect(flash().attribute("error", "不正なパラメーターです"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void カテゴリー名で検索ができるか() throws Exception {
		executeScript("/db/insertCategories.sql");

		val mvcResult = this.mockMvc.perform(get("/categories")
				.param("searchCategoryName", "数学"))
				.andExpect(status().isOk())
				.andExpect(view().name("category/index"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val categoryFormList = ((List<CategoryForm>) mav.getModel().get("categoryFormList"));
		val actualListSize = categoryFormList.size();
		val expectListSize = 1;
		assertThat(actualListSize, is(expectListSize));

		val actualCategoryName = (mav.getModel().get("searchCategoryName"));
		val expectCategoryName = "数学";
		assertThat(actualCategoryName, is(expectCategoryName));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 空白で検索ボタンを押下すると全カテゴリーを表示する() throws Exception {
		executeScript("/db/insertCategories.sql");

		val mvcResult = this.mockMvc.perform(get("/categories"))
				.andExpect(status().isOk())
				.andExpect(view().name("category/index"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val categoryFormList = ((List<CategoryForm>) mav.getModel().get("categoryFormList"));
		val actualListSize = categoryFormList.size();
		val expectListSize = 3;
		assertThat(actualListSize, is(expectListSize));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 存在しないカテゴリー名で検索するとカテゴリーを表示しない() throws Exception {
		executeScript("/db/insertCategories.sql");

		val mvcResult = this.mockMvc.perform(get("/categories")
				.param("searchCategoryName", "存在しないカテゴリー"))
				.andExpect(status().isOk())
				.andExpect(view().name("category/index"))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		@SuppressWarnings("unchecked")
		val categoryFormList = ((List<CategoryForm>) mav.getModel().get("categoryFormList"));
		val actualListSize = categoryFormList.size();
		val expectListSize = 0;
		assertThat(actualListSize, is(expectListSize));

		val actualCategoryName = (mav.getModel().get("searchCategoryName"));
		val expectCategoryName = "存在しないカテゴリー";
		assertThat(actualCategoryName, is(expectCategoryName));
	}

	//SQL文を実行
	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

	private List<CategoryForm> convertCategoryFormList(final List<Category> categoryList) {
		val beanMapper = this.beanMapperFactory.getMapperFacade(Category.class, CategoryForm.class);
		return List.copyOf(categoryList.stream().map(x -> beanMapper.map(x)).collect(Collectors.toList()));
	}
}