package jp.co.code_reminder.app.category;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Timestamp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

@SpringBootTest
public class CategoryFormTest {

	@Autowired
	@Qualifier("categoryFormValidator")
	private org.springframework.validation.Validator categoryValidator;
	private final CategoryForm categoryForm = new CategoryForm();
	private final BindingResult bindingResult = new BindException(categoryForm, "categoryForm");

	@BeforeEach
	public void 値をセット() throws Exception {
		this.categoryForm.setId(1);
		this.categoryForm.setName("数学");
		this.categoryForm.setAccountId("0b80bf97-6da7-4ec0-a5d2-4ffcc382259f");
		this.categoryForm.setCreatedDate(new Timestamp(System.currentTimeMillis()));
		this.categoryForm.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
	}

	@Test
	public void エラー無し() throws Exception {
		this.categoryValidator.validate(this.categoryForm, this.bindingResult);
		assertThat(this.bindingResult.getFieldError(), is(nullValue()));
	}

	@Test
	public void カテゴリ名がBlank() throws Exception {
		this.categoryForm.setName("");
		this.categoryValidator.validate(this.categoryForm, this.bindingResult);
		assertEquals(this.bindingResult.getFieldError().getDefaultMessage(), "カテゴリを入力してください");
	}

	@Test
	public void 文字数が51文字() throws Exception {
		this.categoryForm.setName("ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでとどなにぬねのはばぱひび");
		this.categoryValidator.validate(this.categoryForm, this.bindingResult);
		assertEquals(this.bindingResult.getFieldError().getDefaultMessage(), "カテゴリは50文字以下で入力してください");
	}

}