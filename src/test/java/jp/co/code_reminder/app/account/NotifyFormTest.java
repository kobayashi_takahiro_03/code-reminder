package jp.co.code_reminder.app.account;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Timestamp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import jp.co.code_reminder.domain.model.line_notification.NotificationFlag;

@SpringBootTest
public class NotifyFormTest {

	@Autowired
	@Qualifier("notificationValidator")
	private org.springframework.validation.Validator notificationValidator;
	private final NotifyForm notifyForm = new NotifyForm();
	private final BindingResult bindingResult = new BindException(notifyForm, "notifyForm");

	@BeforeEach
	public void 値をセット() throws Exception {
		this.notifyForm.setAccountId("0b80bf97-6da7-4ec0-a5d2-4ffcc382259f");
		this.notifyForm.setToken("cwHEt4kZjvDF2jo3DSwxJsRt0rwspBecjgFbvqjKBcE");
		this.notifyForm.setNotificationFlag(NotificationFlag.Notification);
		this.notifyForm.setNotificationTime("9");
		this.notifyForm.setCreatedDate(new Timestamp(System.currentTimeMillis()));
		this.notifyForm.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
	}

	@Test
	public void エラー無し() throws Exception {
		this.notificationValidator.validate(this.notifyForm, this.bindingResult);
		assertThat(this.bindingResult.getFieldError(), is(nullValue()));
	}

	@Test
	public void 通知設定がnull() throws Exception {
		this.notifyForm.setNotificationFlag(null);
		this.notificationValidator.validate(this.notifyForm, this.bindingResult);
		assertEquals(this.bindingResult.getFieldError().getDefaultMessage(), "通知設定の選択は必須です");
	}

	@Test
	public void ハンドルネームがBlank() throws Exception {
		this.notifyForm.setToken("");
		this.notificationValidator.validate(this.notifyForm, this.bindingResult);
		assertEquals(this.bindingResult.getFieldError().getDefaultMessage(), "トークンを入力してください");
	}

	@Test
	public void 文字数が39文字() throws Exception {
		this.notifyForm.setToken("ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLM");
		this.notificationValidator.validate(this.notifyForm, this.bindingResult);
		assertEquals(this.bindingResult.getFieldError().getDefaultMessage(), "トークンは40文字以上50文字以下で入力してください");
	}

	@Test
	public void 文字数が51文字() throws Exception {
		this.notifyForm.setToken("ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXY");
		this.notificationValidator.validate(this.notifyForm, this.bindingResult);
		assertEquals(this.bindingResult.getFieldError().getDefaultMessage(), "トークンは40文字以上50文字以下で入力してください");
	}

	@Test
	public void 文字が記号を含む半角英数字以外() throws Exception {
		this.notifyForm.setToken("ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでと");
		this.notificationValidator.validate(this.notifyForm, this.bindingResult);
		assertEquals(this.bindingResult.getFieldError().getDefaultMessage(), "トークンは記号を含む半角英数字で入力してください");
	}
}
