package jp.co.code_reminder.app.account;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.code_reminder.app.security.WithMockCustomUser;
import jp.co.code_reminder.domain.model.account.RoleName;
import jp.co.code_reminder.domain.model.line_notification.NotificationFlag;
import jp.co.code_reminder.domain.service.account.AccountReferService;
import lombok.val;

@SpringBootTest
@Transactional(rollbackFor = Exception.class)
public class AccountsControllerTest {

	private MockMvc mockMvc;

	private AccountForm accountForm;

	private NotifyForm notifyForm;

	@Autowired
	private AccountReferService accountReferService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AccountsController target;

	@BeforeEach
	public void 初期化処理() throws Exception {
		this.accountForm = new AccountForm();
		this.accountForm.setUuid("39479f56-c0c4-43e3-9b84-e67406b66fc2");
		this.accountForm.setLoginId("kobayasi");
		this.accountForm.setName("小林隆弘");
		this.accountForm.setMail("takachanman@yahoo.co.jp");
		this.accountForm.setConfirmMail("takachanman@yahoo.co.jp");
		this.accountForm.setPassword("password");
		this.accountForm.setConfirmPassword("password");
		this.accountForm.setRoleName(RoleName.ROLE_USER);

		this.notifyForm = new NotifyForm();
		this.notifyForm.setAccountId("39479f56-c0c4-43e3-9b84-e67406b66fc2");
		this.notifyForm.setToken("cwHEt4kZjvDF2jo3DSwxJsRt0rwspBecjgFbvqjKBcE");
		this.notifyForm.setNotificationTime("9");
		this.notifyForm.setNotificationFlag(NotificationFlag.Notification);

		val resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/");
		resolver.setSuffix(".html");
		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).setViewResolvers(resolver)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void マイページが表示出来るか() throws Exception {
		this.mockMvc.perform(get("/accounts/mypage")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/show"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 変更無しで更新ボタン押下() throws Exception {
		this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/accounts/mypage"))
				.andExpect(flash().attribute("success", "更新に成功しました"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを変更して更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("takachanman");
		this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/accounts/mypage"))
				.andExpect(flash().attribute("success", "更新に成功しました"));

		val expectedLoginId = "takachanman";
		val actualLoginId = this.accountReferService.findOne("takachanman", "login_id").getLoginId();

		assertEquals(expectedLoginId, actualLoginId);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードを変更して更新ボタン押下() throws Exception {
		this.accountForm.setPassword("testpassword");
		this.accountForm.setConfirmPassword("testpassword");
		this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/accounts/mypage"))
				.andExpect(flash().attribute("success", "更新に成功しました"));

		val expectedPassword = "testpassword";
		val actualPassword = this.accountReferService.findOne("kobayasi", "login_id").getPassword();

		assertTrue(this.passwordEncoder.matches(expectedPassword, actualPassword));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ハンドルネームを変更して更新ボタン押下() throws Exception {
		this.accountForm.setName("小林祐也");
		this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/accounts/mypage"))
				.andExpect(flash().attribute("success", "更新に成功しました"));

		val expectedName = "小林祐也";
		val actualName = this.accountReferService.findOne("kobayasi", "login_id").getName();

		assertEquals(expectedName, actualName);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void メールアドレスを変更して更新ボタン押下() throws Exception {
		this.accountForm.setMail("kobayashitakahiro@yahoo.co.jp");
		this.accountForm.setConfirmMail("kobayashitakahiro@yahoo.co.jp");
		this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/accounts/mypage"))
				.andExpect(flash().attribute("success", "更新に成功しました"));

		val expectedMail = "kobayashitakahiro@yahoo.co.jp";
		val actualMail = this.accountReferService.findOne("kobayasi", "login_id").getMail();

		assertEquals(expectedMail, actualMail);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 確認用メールアドレスとメールアドレスを空にして更新ボタン押下() throws Exception {
		this.accountForm.setConfirmMail("");
		this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/accounts/mypage"))
				.andExpect(flash().attribute("success", "更新に成功しました"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードと確認用パスワードを空にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("");
		this.accountForm.setConfirmPassword("");
		this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andDo(print())
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/accounts/mypage"))
				.andExpect(flash().attribute("success", "更新に成功しました"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void トークンに記号を含む半角英数字を記入し更新ボタン押下() throws Exception {
		this.notifyForm.setToken("A1#CDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNRT");
		this.mockMvc.perform(post("/accounts/mypage/updateNotify")
				.flashAttr("notifyForm", this.notifyForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/accounts/mypage"))
				.andExpect(flash().attribute("success", "更新に成功しました"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 通知設定をオフにして更新ボタン押下() throws Exception {
		this.notifyForm.setNotificationFlag(NotificationFlag.NotNotification);
		this.mockMvc.perform(post("/accounts/mypage/updateNotify")
				.flashAttr("notifyForm", this.notifyForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/accounts/mypage"))
				.andExpect(flash().attribute("success", "更新に成功しました"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 通知設定をオフの状態でトークンを空にして更新ボタン押下() throws Exception {
		this.notifyForm.setToken("");
		this.notifyForm.setNotificationFlag(NotificationFlag.NotNotification);
		this.mockMvc.perform(post("/accounts/mypage/updateNotify")
				.flashAttr("notifyForm", this.notifyForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/accounts/mypage"))
				.andExpect(flash().attribute("success", "更新に成功しました"));
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを空にして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードを空にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "password"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "パスワードを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 確認用パスワードを空にして更新ボタン押下() throws Exception {
		this.accountForm.setConfirmPassword("");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "確認用パスワードを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ハンドルネーム空にして更新ボタン押下() throws Exception {
		this.accountForm.setName("");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "name"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ハンドルネームを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);

		val actualAccountForm = (AccountForm) mav.getModel().get("accountForm");
		val expectedAccountForm = this.accountForm;

		assertEquals(expectedAccountForm, actualAccountForm);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void メールアドレスを空にして更新ボタン押下() throws Exception {
		this.accountForm.setMail("");
		this.accountForm.setConfirmMail("kobayashitakahiro@yahoo.co.jp");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "メールアドレスを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを5文字以下にして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("kobas");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDは6文字以上、20文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを21文字以上にして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("ABCDEFGH1JKLMNOPQRS3U");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDは6文字以上、20文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードと確認用パスワードを5文字以下にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("ABCDE");
		this.accountForm.setConfirmPassword("ABCDE");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualPasswordErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedPasswordErrorMessage = "パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedPasswordErrorMessage, actualPasswordErrorMessage);

		val actualConfirmPasswordErrorMessage = errors.getAllErrors().get(1).getDefaultMessage();
		val expectedConfirmPasswordErrorMessage = "確認用パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedConfirmPasswordErrorMessage, actualConfirmPasswordErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードと確認用パスワードを21文字以上にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("ABCDEFGHIJKLMNOPQRSTU");
		this.accountForm.setConfirmPassword("ABCDEFGHIJKLMNOPQRSTU");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualPasswordErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedPasswordErrorMessage = "パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedPasswordErrorMessage, actualPasswordErrorMessage);

		val actualConfirmPasswordErrorMessage = errors.getAllErrors().get(1).getDefaultMessage();
		val expectedConfirmPasswordErrorMessage = "確認用パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedConfirmPasswordErrorMessage, actualConfirmPasswordErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ハンドルネームを1文字以下にして更新ボタン押下() throws Exception {
		this.accountForm.setName("A");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "name"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ハンドルネームは2文字以上、10文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ハンドルネームを11文字以上にして更新ボタン押下() throws Exception {
		this.accountForm.setName("ABCDEFGHIJK");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "name"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ハンドルネームは2文字以上、10文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void メールアドレスにEmail形式ではない文字列にして更新ボタン押下() throws Exception {
		this.accountForm.setMail("takahiroyahoo.co.jp");
		this.accountForm.setConfirmMail("takahiro@yahoo.co.jp");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "メールアドレスはメールアドレス形式で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 確認用メールアドレスにEmail形式ではない文字列にして更新ボタン押下() throws Exception {
		this.accountForm.setConfirmMail("takahiroyahoo.co.jp");
		this.accountForm.setMail("takahiro@yahoo.co.jp");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmMail"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "確認用メールアドレスはメールアドレス形式で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを半角英数字以外で更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("小林たかひろざえもん");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDは半角英数字で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードと確認用パスワードを半角英数字以外で更新ボタン押下() throws Exception {
		this.accountForm.setPassword("小林たかひろざえもん");
		this.accountForm.setConfirmPassword("小林たかひろざえもん");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "パスワードは記号を含む半角文字で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void メールアドレスと確認用メールアドレスが異なった値で更新ボタン押下() throws Exception {
		this.accountForm.setMail("takachanman@yahoo.co.jp");
		this.accountForm.setConfirmMail("takachanman@yahoo.co.jpp");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたメールアドレスと確認用メールアドレスが異なっています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void パスワードと確認用パスワードが異なった値で更新ボタン押下() throws Exception {
		this.accountForm.setPassword("password");
		this.accountForm.setConfirmPassword("passwords");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "password"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたパスワードと確認用パスワードが異なっています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void ログインIDを登録されているログインIDにして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("maruyama");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたログインIDは既に登録されています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void メールアドレスを登録されているメールアドレスにして更新ボタン押下() throws Exception {
		this.accountForm.setMail("maru@yahoo.co.jp");
		this.accountForm.setConfirmMail("maru@yahoo.co.jp");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/edit")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたメールアドレスは既に登録されています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void トークン入力欄を空にして更新ボタン押下() throws Exception {
		this.notifyForm.setToken("");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/updateNotify")
				.flashAttr("notifyForm", this.notifyForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("notifyForm", this.notifyForm))
				.andExpect(model().attributeHasFieldErrors("notifyForm", "token"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "notifyForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "トークンを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void トークン入力欄に39文字入力して更新ボタン押下() throws Exception {
		this.notifyForm.setToken("ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLM");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/updateNotify")
				.flashAttr("notifyForm", this.notifyForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("notifyForm", this.notifyForm))
				.andExpect(model().attributeHasFieldErrors("notifyForm", "token"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "notifyForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "トークンは40文字以上50文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void トークン入力欄に51文字入力して更新ボタン押下() throws Exception {
		this.notifyForm.setToken("ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXY");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/updateNotify")
				.flashAttr("notifyForm", this.notifyForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("notifyForm", this.notifyForm))
				.andExpect(model().attributeHasFieldErrors("notifyForm", "token"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "notifyForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "トークンは40文字以上50文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void トークン入力欄に記号を含む半角英数字以外を入力して更新ボタン押下() throws Exception {
		this.notifyForm.setToken("ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでと");
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/updateNotify")
				.flashAttr("notifyForm", this.notifyForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("notifyForm", this.notifyForm))
				.andExpect(model().attributeHasFieldErrors("notifyForm", "token"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "notifyForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "トークンは記号を含む半角英数字で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	@WithMockCustomUser(loginId = "kobayasi")
	public void 通知設定欄で送られるパラメーターをnullにして更新ボタン押下() throws Exception {
		this.notifyForm.setNotificationFlag(null);
		val mvcResult = this.mockMvc.perform(post("/accounts/mypage/updateNotify")
				.flashAttr("notifyForm", this.notifyForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/edit"))
				.andExpect(model().attribute("notifyForm", this.notifyForm))
				.andExpect(model().attributeHasFieldErrors("notifyForm", "notificationFlag"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "notifyForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "通知設定の選択は必須です";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}
}