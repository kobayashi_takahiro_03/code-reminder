package jp.co.code_reminder.app.account;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import jp.co.code_reminder.core.validator.account.AccountFormValidator;
import jp.co.code_reminder.domain.model.account.RoleName;
import lombok.val;

@SpringBootTest
public class AccountFormTest {

	private AccountForm accountForm = new AccountForm();
	private BindingResult bindingResult = new BindException(this.accountForm, "accountForm");

	@Autowired
	@Qualifier("accountFormValidator")
	private AccountFormValidator accountFormValidator;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@BeforeEach
	public void 値をセット() throws Exception {
		this.accountForm.setUuid("0b80bf07-6da7-4ec0-a5d2-4ffcc382259f");
		this.accountForm.setLoginId("takahiro");
		this.accountForm.setName("コバヤシ");
		this.accountForm.setMail("taka@yahoo.co.jp");
		this.accountForm.setConfirmMail("taka@yahoo.co.jp");
		this.accountForm.setPassword("password");
		this.accountForm.setConfirmPassword("password");
		this.accountForm.setRoleName(RoleName.ROLE_USER);
	}

	@Test
	public void エラー無し() throws Exception {
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertTrue(!this.bindingResult.hasErrors());
	}

	@Test
	public void ログインIDがBlank() throws Exception {
		this.accountForm.setLoginId("");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("ログインIDを入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void パスワードがBlank() throws Exception {
		this.accountForm.setPassword("");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("パスワードを入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void 確認用パスワードがBlank() throws Exception {
		this.accountForm.setConfirmPassword("");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("確認用パスワードを入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void ハンドルネームがBlank() throws Exception {
		this.accountForm.setName("");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("ハンドルネームを入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void メールアドレスがBlank() throws Exception {
		this.accountForm.setMail("");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("メールアドレスを入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void 確認用メールアドレスがBlank() throws Exception {
		this.accountForm.setConfirmMail("");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("確認用メールアドレスを入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void ログインIDが5文字以下() throws Exception {
		this.accountForm.setLoginId("aiueo");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("ログインIDは6文字以上、20文字以下で入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void ログインIDが21文字以上() throws Exception {
		this.accountForm.setLoginId("ABCDEFGHIJKLMNOPQRSTU");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("ログインIDは6文字以上、20文字以下で入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void パスワードと確認用パスワードが5文字以下() throws Exception {
		this.accountForm.setPassword("aiueo");
		this.accountForm.setConfirmPassword("aiueo");

		this.accountFormValidator.validate(this.accountForm, this.bindingResult);

		val errors = this.bindingResult.getFieldErrors();
		val passwordErrorMessage = "パスワードは6文字以上、20文字以下で入力してください";
		val confirmPasswordErrorMessage = "確認用パスワードは6文字以上、20文字以下で入力してください";

		boolean result = false;
		val expectedErrorCount = 2;
		val acutualErrorCount = errors.size();
		for (val error : errors) {
			if (error.getDefaultMessage().equals(passwordErrorMessage)
					|| error.getDefaultMessage().equals(confirmPasswordErrorMessage)) {
				result = true;
			}
		}

		assertEquals(expectedErrorCount, acutualErrorCount);
		assertTrue(result);
	}

	@Test
	public void パスワードと確認用パスワードが21文字以上() throws Exception {
		this.accountForm.setPassword("ABCDEFGHIJKLMNOPQRSTU");
		this.accountForm.setConfirmPassword("ABCDEFGHIJKLMNOPQRSTU");

		this.accountFormValidator.validate(this.accountForm, this.bindingResult);

		val errors = this.bindingResult.getFieldErrors();
		val passwordErrorMessage = "パスワードは6文字以上、20文字以下で入力してください";
		val confirmPasswordErrorMessage = "確認用パスワードは6文字以上、20文字以下で入力してください";

		boolean result = false;
		val expectedErrorCount = 2;
		val acutualErrorCount = errors.size();
		for (val error : errors) {
			if (error.getDefaultMessage().equals(passwordErrorMessage)
					|| error.getDefaultMessage().equals(confirmPasswordErrorMessage)) {
				result = true;
			}
		}

		assertEquals(expectedErrorCount, acutualErrorCount);
		assertTrue(result);
	}

	@Test
	public void ハンドルネームが1文字以下() throws Exception {
		this.accountForm.setName("a");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("ハンドルネームは2文字以上、10文字以下で入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void ハンドルネームが11文字以上() throws Exception {
		this.accountForm.setName("ABCDEFGHIKI");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("ハンドルネームは2文字以上、10文字以下で入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void メールアドレスの書式が不正() throws Exception {
		this.accountForm.setMail("takahiroyahoo.co.jp");
		this.accountForm.setConfirmMail("takahiroyahoo.co.jp");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("メールアドレスはメールアドレス形式で入力してください", this.bindingResult.getFieldError("mail").getDefaultMessage());
	}

	@Test
	public void 確認用メールアドレスの書式が不正() throws Exception {
		this.accountForm.setMail("takahiroyahoo.co.jp");
		this.accountForm.setConfirmMail("takahiroyahoo.co.jp");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("確認用メールアドレスはメールアドレス形式で入力してください", this.bindingResult.getFieldError("confirmMail").getDefaultMessage());
	}

	@Test
	public void ログインIDが半角英数字以外() throws Exception {
		this.accountForm.setLoginId("テストユーザー");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("ログインIDは半角英数字で入力してください", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void パスワードと確認用パスワードが記号を含む半角英数字以外() throws Exception {
		this.accountForm.setConfirmPassword("テストパスワード");
		this.accountForm.setPassword("テストパスワード");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);

		val errors = this.bindingResult.getFieldErrors();
		val passwordErrorMessage = "パスワードは記号を含む半角文字で入力してください";
		val confirmPasswordErrorMessage = "確認用パスワードは記号を含む半角文字で入力してください";

		boolean result = false;
		val expectedErrorCount = 2;
		val acutualErrorCount = errors.size();
		for (val error : errors) {
			if (error.getDefaultMessage().equals(passwordErrorMessage)
					|| error.getDefaultMessage().equals(confirmPasswordErrorMessage)) {
				result = true;
			}
		}

		assertEquals(expectedErrorCount, acutualErrorCount);
		assertTrue(result);
	}

	@Test
	public void メールアドレスと確認用メールアドレスが異なる() throws Exception {
		this.accountForm.setMail("taka@yahoo.co.jp");
		this.accountForm.setConfirmMail("takaa@yahoo.co.jp");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("入力されたメールアドレスと確認用メールアドレスが異なっています", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	public void パスワードと確認用パスワードが異なる() throws Exception {
		this.accountForm.setPassword("password");
		this.accountForm.setConfirmPassword("paassword");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("入力されたパスワードと確認用パスワードが異なっています", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	public void 既に登録されているログインIDを入力() throws Exception {
		executeScript("/db/insertUsers.sql");
		this.accountForm.setLoginId("kobayasi");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("入力されたログインIDは既に登録されています", this.bindingResult.getFieldError().getDefaultMessage());
	}

	@Test
	@Transactional(rollbackFor = Exception.class)
	public void 既に登録されているメールアドレスを入力() throws Exception {
		executeScript("/db/insertUsers.sql");
		this.accountForm.setMail("takachanman@yahoo.co.jp");
		this.accountForm.setConfirmMail("takachanman@yahoo.co.jp");
		this.accountFormValidator.validate(this.accountForm, this.bindingResult);
		assertEquals("入力されたメールアドレスは既に登録されています", this.bindingResult.getFieldError().getDefaultMessage());
	}

	//SQL文を実行
	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

}