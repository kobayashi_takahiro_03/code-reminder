package jp.co.code_reminder.app.account;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.code_reminder.domain.model.account.RoleName;
import jp.co.code_reminder.domain.service.account.AccountReferService;
import lombok.val;

@SpringBootTest
@Transactional(rollbackFor = Exception.class)
public class SignupControllerTest {

	private MockMvc mockMvc;

	private AccountForm accountForm;

	@Autowired
	private AccountReferService accountReferService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private SignupController target;

	@BeforeEach
	public void 初期化処理() throws Exception {
		this.accountForm = new AccountForm();
		this.accountForm.setLoginId("kobayasi");
		this.accountForm.setPassword("password");
		this.accountForm.setConfirmPassword("password");
		this.accountForm.setMail("takachanman0317@yahoo.co.jp");
		this.accountForm.setConfirmMail("takachanman0317@yahoo.co.jp");
		this.accountForm.setName("小林隆弘");

		val resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/");
		resolver.setSuffix(".html");
		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).setViewResolvers(resolver)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
	}

	@Test
	public void アカウント新規登録画面が表示出来るか() throws Exception {
		this.mockMvc.perform(get("/signup"))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"));
	}

	@Disabled
	@Test
	public void メールが送信出来るか() throws Exception {

		this.accountForm.setUuid("39479f56-c0c4-43e3-9b84-e67406b70fc2");

		this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/login"))
				.andExpect(flash().attribute("success", "メールが送信されました"));

		this.mockMvc.perform(get("/signup/validate/39479f56-c0c4-43e3-9b84-e67406b70fc2"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/login"))
				.andExpect(flash().attribute("success", "認証に成功しました"));

		val actualRoleName = this.accountReferService.findOne("39479f56-c0c4-43e3-9b84-e67406b70fc2", "uuid")
				.getRoleName();
		val expectedRoleName = RoleName.ROLE_USER;

		assertEquals(expectedRoleName, actualRoleName);
		assertTrue(this.accountReferService.existAccount("39479f56-c0c4-43e3-9b84-e67406b70fc2", "uuid"));
	}

	@Test
	public void ログインIDを空にして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	public void パスワードと確認用パスワードを空にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("");
		this.accountForm.setConfirmPassword("");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualPasswordErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedPasswordErrorMessage = "パスワードを入力してください";

		assertEquals(expectedPasswordErrorMessage, actualPasswordErrorMessage);

		val actualConfirmPasswordErrorMessage = errors.getAllErrors().get(1).getDefaultMessage();
		val expectedConfirmPasswordErrorMessage = "確認用パスワードを入力してください";

		assertEquals(expectedConfirmPasswordErrorMessage, actualConfirmPasswordErrorMessage);
	}

	@Test
	public void メールアドレスと確認用メールアドレスを空にして更新ボタン押下() throws Exception {
		this.accountForm.setMail("");
		this.accountForm.setConfirmMail("");

		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmMail"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualMailErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedMailErrorMessage = "メールアドレスを入力してください";

		assertEquals(expectedMailErrorMessage, actualMailErrorMessage);

		val actualConfirmErrorMessage = errors.getAllErrors().get(1).getDefaultMessage();
		val expectedConfirmErrorMessage = "確認用メールアドレスを入力してください";

		assertEquals(expectedConfirmErrorMessage, actualConfirmErrorMessage);
	}

	@Test
	public void ハンドルネーム空にして更新ボタン押下() throws Exception {
		this.accountForm.setName("");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "name"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ハンドルネームを入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);

		val actualAccountForm = (AccountForm) mav.getModel().get("accountForm");
		val expectedAccountForm = this.accountForm;

		assertEquals(expectedAccountForm, actualAccountForm);
	}

	@Test
	public void ログインIDを5文字以下にして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("kobas");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDは6文字以上、20文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	public void ログインIDを21文字以上にして更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("ABCDEFGH1JKLMNOPQRS3U");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDは6文字以上、20文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	public void パスワードと確認用パスワードを5文字以下にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("ABCDE");
		this.accountForm.setConfirmPassword("ABCDE");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "password"))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualPasswordErrorMessage = errors.getFieldError("password").getDefaultMessage();
		val expectedPasswordErrorMessage = "パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedPasswordErrorMessage, actualPasswordErrorMessage);

		val actualConfirmPasswordErrorMessage = errors.getFieldError("confirmPassword").getDefaultMessage();
		val expectedConfirmPasswordErrorMessage = "確認用パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedConfirmPasswordErrorMessage, actualConfirmPasswordErrorMessage);
	}

	@Test
	public void パスワードと確認用パスワードを21文字以上にして更新ボタン押下() throws Exception {
		this.accountForm.setPassword("ABCDEFGHIJKLMNOPQRSTU");
		this.accountForm.setConfirmPassword("ABCDEFGHIJKLMNOPQRSTU");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "password"))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualPasswordErrorMessage = errors.getFieldError("password").getDefaultMessage();
		val expectedPasswordErrorMessage = "パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedPasswordErrorMessage, actualPasswordErrorMessage);

		val actualConfirmPasswordErrorMessage = errors.getFieldError("confirmPassword").getDefaultMessage();
		val expectedConfirmPasswordErrorMessage = "確認用パスワードは6文字以上、20文字以下で入力してください";

		assertEquals(expectedConfirmPasswordErrorMessage, actualConfirmPasswordErrorMessage);
	}

	@Test
	public void メールアドレスと確認用メールアドレスにEmail形式ではない文字列にして更新ボタン押下() throws Exception {
		this.accountForm.setConfirmMail("takahiroyahoo.co.jp");
		this.accountForm.setMail("takahiroyahoo.co.jp");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmMail"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualPasswordErrorMessage = errors.getFieldError("mail").getDefaultMessage();
		val expectedPasswordErrorMessage = "メールアドレスはメールアドレス形式で入力してください";

		assertEquals(expectedPasswordErrorMessage, actualPasswordErrorMessage);

		val actualConfirmPasswordErrorMessage = errors.getFieldError("confirmMail").getDefaultMessage();
		val expectedConfirmPasswordErrorMessage = "確認用メールアドレスはメールアドレス形式で入力してください";

		assertEquals(expectedConfirmPasswordErrorMessage, actualConfirmPasswordErrorMessage);
	}

	@Test
	public void ハンドルネームを1文字以下にして更新ボタン押下() throws Exception {
		this.accountForm.setName("A");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "name"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ハンドルネームは2文字以上、10文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	public void ハンドルネームを11文字以上にして更新ボタン押下() throws Exception {
		this.accountForm.setName("ABCDEFGHIJK");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "name"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ハンドルネームは2文字以上、10文字以下で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	public void ログインIDを半角英数字以外で更新ボタン押下() throws Exception {
		this.accountForm.setLoginId("小林たかひろざえもん");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "ログインIDは半角英数字で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	public void パスワードと確認用パスワードを半角英数字以外で更新ボタン押下() throws Exception {
		this.accountForm.setPassword("小林たかひろざえもん");
		this.accountForm.setConfirmPassword("小林たかひろざえもん");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "password"))
				.andExpect(model().attributeHasFieldErrors("accountForm", "confirmPassword"))
				.andExpect(model().errorCount(2))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "パスワードは記号を含む半角文字で入力してください";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	public void メールアドレスと確認用メールアドレスが異なった値で更新ボタン押下() throws Exception {
		this.accountForm.setMail("takacha@yahoo.co.jp");
		this.accountForm.setConfirmMail("takacha@yahoo.co.jpp");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたメールアドレスと確認用メールアドレスが異なっています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	public void パスワードと確認用パスワードが異なった値で更新ボタン押下() throws Exception {
		this.accountForm.setPassword("password");
		this.accountForm.setConfirmPassword("passwords");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "password"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたパスワードと確認用パスワードが異なっています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	public void ログインIDを登録されているログインIDにして更新ボタン押下() throws Exception {
		executeScript("/db/insertUsers.sql");

		this.accountForm.setLoginId("maruyama");
		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "loginId"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたログインIDは既に登録されています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	@Test
	public void メールアドレスを登録されているメールアドレスにして更新ボタン押下() throws Exception {
		executeScript("/db/insertUsers.sql");

		this.accountForm.setLoginId("takahirou");
		this.accountForm.setMail("maru@yahoo.co.jp");
		this.accountForm.setConfirmMail("maru@yahoo.co.jp");

		val mvcResult = this.mockMvc.perform(post("/signup")
				.flashAttr("accountForm", this.accountForm))
				.andExpect(status().isOk())
				.andExpect(view().name("account/new"))
				.andExpect(model().attribute("accountForm", this.accountForm))
				.andExpect(model().attributeHasFieldErrors("accountForm", "mail"))
				.andExpect(model().errorCount(1))
				.andReturn();

		val mav = mvcResult.getModelAndView();
		val errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "accountForm");
		val actualErrorMessage = errors.getAllErrors().get(0).getDefaultMessage();
		val expectedErrorMessage = "入力されたメールアドレスは既に登録されています";

		assertEquals(expectedErrorMessage, actualErrorMessage);
	}

	//SQL文を実行
	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

}
