package jp.co.code_reminder.domain.service.category;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.category.Category;
import jp.co.code_reminder.domain.repository.category.CategoryRepository;
import lombok.val;

@SpringBootTest
@Transactional(rollbackFor = Exception.class)
public class CategoryReferServiceTest {

	@Mock
	private CategoryRegistService categoryRegistService;

	@Mock
	private CategoryRepository categoryRepository;

	@Mock
	private Category category;

	@InjectMocks
	private CategoryReferServiceImpl categoryReferService;

	@BeforeEach
	void setUp() {
		this.categoryReferService = new CategoryReferServiceImpl(this.categoryRepository);
	}

	@Test
	public void findByAccountIdが正常に呼び出されるか() throws Exception {
		val categoryList = new ArrayList<Category>();
		lenient().when(this.categoryRepository.findByAccountId(anyString())).thenReturn(categoryList);

		val expect = categoryList;
		val actual = this.categoryReferService.findByAccountId("accountId");
		assertEquals(expect, actual);
		verify(this.categoryRepository, times(1)).findByAccountId(anyString());
	}

	@Test
	public void findByAccountIdに渡されるaccountIdがnullの場合にエラーをthrowするか() throws Exception {
		assertThrows(NullPointerException.class, () -> this.categoryReferService.findByAccountId(null));
	}

	@Test
	public void findByQuestionIdが正常に呼び出されるか() throws Exception {
		val pathQuestionId = "12345";
		val categoryList = new ArrayList<Category>();
		lenient().when(this.categoryRepository.findByQuestionId(anyInt())).thenReturn(categoryList);

		val expect = categoryList;
		val actual = this.categoryReferService.findByQuestionId(pathQuestionId);
		assertEquals(expect, actual);
	}

	@Test
	public void findByQuestionIdに数字以外のQuestionIdが渡された場合エラーをthorwするか() throws Exception {
		val pathQuestionId = "test";
		val categoryList = new ArrayList<Category>();
		lenient().when(this.categoryRepository.findByQuestionId(anyInt())).thenReturn(categoryList);

		assertThrows(IllegalArgumentException.class, () -> this.categoryReferService.findByQuestionId(pathQuestionId));
	}

	@Test
	public void findByPageAndAccountIdAndNameが正常に呼び出されるか() throws Exception {
		val accountId = "accountId";
		val searchCategoryName = "searchCategoryName";
		val pageable = Pageable.unpaged();
		val categoryList = new ArrayList<Category>();
		lenient().when(this.categoryRepository.findByAccountIdAndPage(eq(pageable), anyString(), anyString()))
				.thenReturn(categoryList);

		val expect = categoryList;
		val actual = this.categoryReferService.findByPageAndAccountIdAndName(pageable, accountId, searchCategoryName);
		assertEquals(expect, actual);
		verify(this.categoryRepository, times(1)).findByAccountIdAndPage(eq(pageable), anyString(), anyString());
	}

	@Test
	public void findByPageAndAccountIdAndNameに値がnullで渡された場合エラーをthrowするか() throws Exception {
		val accountId = "accountId";
		val searchCategoryName = "searchCategoryName";
		val pageable = Pageable.unpaged();

		assertThrows(NullPointerException.class,
				() -> this.categoryReferService.findByPageAndAccountIdAndName(null, accountId, searchCategoryName));
		assertThrows(NullPointerException.class,
				() -> this.categoryReferService.findByPageAndAccountIdAndName(pageable, null, searchCategoryName));
	}

	@Test
	public void findByAccountIdAndIdが正常に呼び出されるか() throws Exception {
		val accountId = "accountId";
		val id = 1;
		Optional<Category> optionalCategory = Optional.ofNullable(category);
		lenient().when(this.categoryRepository.findByIdAndId(anyString(), anyInt())).thenReturn(optionalCategory);

		val expect = category;
		val actual = this.categoryReferService.findByAccountIdAndId(accountId, id);
		assertEquals(expect, actual);
	}

	@Test
	public void findByAccountIdAndIdに渡される戻り値がnullだった場合エラーをthrowするか() throws Exception {
		val accountId = "accountId";
		val id = 1;
		Optional<Category> optionalCategory = Optional.empty();
		lenient().when(this.categoryRepository.findByIdAndId(anyString(), anyInt())).thenReturn(optionalCategory);

		assertThrows(RecordNotFoundException.class,
				() -> this.categoryReferService.findByAccountIdAndId(accountId, id));
	}

	@Test
	public void findByAccountIdAndIdに渡される値がnullだった場合エラーをthrowするか() throws Exception {
		val accountId = "accountId";
		val id = 1;

		assertThrows(NullPointerException.class, () -> this.categoryReferService.findByAccountIdAndId(null, id));
		assertThrows(NullPointerException.class, () -> this.categoryReferService.findByAccountIdAndId(accountId, null));
	}

	@Test
	public void findAllが正常に呼び出されるか() throws Exception {
		val categoryList = new ArrayList<Category>();
		lenient().when(this.categoryRepository.findAll()).thenReturn(categoryList);

		val expect = categoryList;
		val actual = this.categoryReferService.findAll();
		assertEquals(expect, actual);
		verify(this.categoryRepository, times(1)).findAll();
	}

	@Test
	public void existIdAndAccountIdが正常に呼び出されるか() throws Exception {
		val accountId = "accountId";
		val category = new Category();
		val categoryList = new ArrayList<Category>();
		val categoryNumList = new ArrayList<Integer>();
		category.setId(1);
		categoryList.add(category);
		categoryNumList.add(1);

		lenient().when(this.categoryRepository.findByAccountId(anyString())).thenReturn(categoryList);

		boolean expect = true;
		boolean actual = this.categoryReferService.existIdAndAccountId(categoryNumList, accountId);
		assertEquals(expect, actual);

		category.setId(2);
		expect = false;
		actual = this.categoryReferService.existIdAndAccountId(categoryNumList, accountId);
		assertEquals(expect, actual);
	}

	@Test
	public void existIdAndAccountIdでregisteredCategoryNumListが空の場合戻り値がfalseになるか() throws Exception {
		val accountId = "accountId";
		val categoryList = new ArrayList<Category>();
		val categoryNumList = new ArrayList<Integer>();
		categoryNumList.add(1);

		lenient().when(this.categoryRepository.findByAccountId(anyString())).thenReturn(categoryList);

		val expect = false;
		val actual = this.categoryReferService.existIdAndAccountId(categoryNumList, accountId);
		assertEquals(expect, actual);
	}

	@Test
	public void existIdAndAccountIdでaccountIdがnullの場合エラーをthrowするか() throws Exception {
		val category = new Category();
		val categoryList = new ArrayList<Category>();
		val categoryNumList = new ArrayList<Integer>();
		category.setId(1);
		categoryList.add(category);
		categoryNumList.add(1);

		lenient().when(this.categoryRepository.findByAccountId(anyString())).thenReturn(categoryList);

		assertThrows(NullPointerException.class,
				() -> this.categoryReferService.existIdAndAccountId(categoryNumList, null));
	}

	@Test
	public void countが正常に呼び出されるか() throws Exception {
		val accountId = "accountId";
		val searchCategoryName = "searchCategoryName";
		val countResult = 1;

		lenient().when(this.categoryRepository.count(anyString(), anyString())).thenReturn(countResult);

		val expect = countResult;
		val actual = this.categoryReferService.count(accountId, searchCategoryName);
		assertEquals(expect, actual);
		verify(this.categoryRepository, times(1)).count(anyString(), anyString());
	}

	@Test
	public void countに渡されるaccountIdがnullの場合エラーをthrowするか() throws Exception {
		val searchCategoryName = "searchCategoryName";
		val countResult = 1;

		lenient().when(this.categoryRepository.count(anyString(), anyString())).thenReturn(countResult);

		assertThrows(NullPointerException.class, () -> this.categoryReferService.count(null, searchCategoryName));
	}

	@Test
	public void parseQuestionIdParamが正常に動作するか() throws Exception {
		val method = CategoryReferServiceImpl.class.getDeclaredMethod("parseQuestionIdParam", String.class);
		method.setAccessible(true);

		val questionId = "1";
		val actual = method.invoke(this.categoryReferService, questionId);

		assertThat(actual, is(1));
	}

	@Test
	public void parseQuestionIdParamに数字以外のquestionIdが渡された場合エラーをthrowするか() throws Exception {
		val method = CategoryReferServiceImpl.class.getDeclaredMethod("parseQuestionIdParam", String.class);
		method.setAccessible(true);

		val questionId = "test";
		try {
			method.invoke(this.categoryReferService, questionId);
		} catch (InvocationTargetException e) {
			assertThat(e.getCause()).isInstanceOf(IllegalArgumentException.class);
		}
	}
}