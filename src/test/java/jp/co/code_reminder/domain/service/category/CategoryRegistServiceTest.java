package jp.co.code_reminder.domain.service.category;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.category.Category;
import jp.co.code_reminder.domain.repository.category.CategoryRepository;
import lombok.val;

@SpringBootTest
@Transactional(rollbackFor = Exception.class)
public class CategoryRegistServiceTest {


	@Mock
	private CategoryReferService categoryReferService;

	@Mock
	private CategoryRepository categoryRepository;

	@Mock
	private Category category;

	@InjectMocks
	private CategoryRegistServiceImpl categoryRegistService;

	@BeforeEach
	void setUp() {
		this.categoryRegistService = new CategoryRegistServiceImpl(this.categoryRepository, this.categoryReferService);
	}

	@Test
	public void createが正常に呼び出されるか() throws Exception {
		lenient().doNothing().when(this.categoryRepository).insert(this.category);

		this.categoryRegistService.create(this.category);

		verify(this.categoryRepository, times(1)).insert(this.category);
	}

	@Test
	public void updateが正常に呼び出されるか() throws Exception {
		lenient().when(this.categoryReferService.existIdAndAccountId(anyList(), anyString())).thenReturn(true);

		val categoryList = new ArrayList<Category>();
		categoryList.add(this.category);
		this.categoryRegistService.update("accoutId",categoryList);

		verify(this.categoryRepository, times(1)).update(categoryList);
	}

	@Test
	public void deleteMultipleが正常に呼び出されるか() throws Exception {
		lenient().when(this.categoryReferService.existIdAndAccountId(anyList(), anyString())).thenReturn(true);

		val deleteNumList = new ArrayList<Integer>();
		deleteNumList.add(1);

		this.categoryRegistService.deleteMultiple("accoutId",deleteNumList);

		verify(this.categoryRepository, times(1)).delete(deleteNumList);
	}

	@Test
	public void カテゴリ作成時にCategoryがnullの場合にエラーをthrowするか() throws Exception {
		assertThrows(NullPointerException.class,() -> this.categoryRegistService.create(null));
	}

	@Test
	public void カテゴリ更新時にCategoryListがnullの場合updateが呼び出されていないか() throws Exception {
		this.categoryRegistService.update("accountId",null);

		verify(this.categoryRepository, never()).update(any());
	}


	@Test
	public void カテゴリ更新時に不正なパラメーターが入力された場合にエラーをthrowするか() throws Exception {
		 lenient().when(this.categoryReferService.existIdAndAccountId(anyList(), anyString())).thenReturn(false);

		val categoryList = new ArrayList<Category>();
		categoryList.add(this.category);

		assertThrows(IllegalArgumentException.class,() -> this.categoryRegistService.update("accountId",categoryList));
	}

	@Test
	public void カテゴリ更新時にアカウントIDがnullの場合にエラーをthrowするか() throws Exception {
		val categoryList = new ArrayList<Category>();
		categoryList.add(this.category);

		assertThrows(NullPointerException.class,() -> this.categoryRegistService.update(null,categoryList));
	}

	@Test
	public void カテゴリ削除時にdeleteNumListがnullの場合にエラーをthrowするか() throws Exception {
		assertThrows(RecordNotFoundException.class,() -> this.categoryRegistService.deleteMultiple("39479f56-c0c4-43e3-9b84-e67406b66fc2",null));
	}

	@Test
	public void カテゴリ削除時に不正なパラメーターが入力された場合にエラーをthrowするか() throws Exception {
		 lenient().when(this.categoryReferService.existIdAndAccountId(anyList(), anyString())).thenReturn(false);

		val deleteNumList = new ArrayList<Integer>();
		deleteNumList.add(1); //空だと先にRecordNodFoundExceptionが反応するため、あえて値を入れています。

		assertThrows(IllegalArgumentException.class,() -> this.categoryRegistService.deleteMultiple("accountId",deleteNumList));
	}

	@Test
	public void カテゴリ削除時にアカウントIDがnullの場合にエラーをthrowするか() throws Exception {
		val deleteNumList = new ArrayList<Integer>();
		deleteNumList.add(1);

		assertThrows(NullPointerException.class,() -> this.categoryRegistService.deleteMultiple(null,deleteNumList));
	}
}