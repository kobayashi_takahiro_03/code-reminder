package jp.co.code_reminder.core.line_notify;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.mockito.Mockito.*;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.transaction.annotation.Transactional;

import jp.co.code_reminder.domain.model.line_notification.NotificationFlag;
import jp.co.code_reminder.domain.model.line_notification.NotifyAccount;
import lombok.val;

@SpringBootTest
@Transactional(rollbackFor = Exception.class)
public class LineNotifyTest {

	@Autowired
	@InjectMocks
	private LineNotify lineNotify;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Mock
	private HttpURLConnection connection;

	private NotifyAccount notifyAccount = new NotifyAccount();

	@BeforeEach
	public void 初期化処理() throws Exception {
		this.notifyAccount.setAccountId("testId");
		this.notifyAccount.setMail("testMail");
		this.notifyAccount.setToken("testToken");
		this.notifyAccount.setName("testName");
		this.notifyAccount.setNotificationFlag(NotificationFlag.Notification);
	}

	/**
	 * インターネットに接続している必要有り
	 * @throws Exception
	 *
	 */
	@Test
	public void トークンが正しい時にnotifyAccountListのサイズがnullになっているか() throws Exception {

		val notifyAccountList = new ArrayList<NotifyAccount>();
		when(this.connection.getResponseCode()).thenReturn(200);

		val method = LineNotify.class.getDeclaredMethod("createNotify", NotifyAccount.class, String.class, List.class);
		method.setAccessible(true);
		method.invoke(this.lineNotify, this.notifyAccount, "testMessage", notifyAccountList);

		val field = this.lineNotify.getClass().getDeclaredField("notifyAccountList");
		field.setAccessible(true);

		@SuppressWarnings("unchecked")
		val actualListSize = (List<NotifyAccount>) field.get(this.lineNotify);

		assertThat(actualListSize, is(nullValue()));

		field.set(this.lineNotify, null);
	}

	/**
	 * インターネットに接続している必要有り
	 * @throws Exception
	 *
	 */
	@Test
	public void トークンが誤っていた場合にnotifyAccountListのサイズがnullになっていないか() throws Exception {

		val notifyAccountList = new ArrayList<NotifyAccount>();

		val method = LineNotify.class.getDeclaredMethod("createNotify", NotifyAccount.class, String.class, List.class);
		method.setAccessible(true);
		method.invoke(this.lineNotify, this.notifyAccount, "testMessage", notifyAccountList);

		val field = this.lineNotify.getClass().getDeclaredField("notifyAccountList");
		field.setAccessible(true);

		@SuppressWarnings("unchecked")
		val actualList = (List<NotifyAccount>) field.get(this.lineNotify);

		assertThat(actualList.size(), is(1));

		field.set(this.lineNotify, null);
	}

	@Test
	@Disabled
	public void 通知オンの場合Mapのサイズが1になるか() throws Exception {
		//SQLで1つのUSERのみに正しいトークンを付与
		executeScript("/db/insertLineNotifications.sql");

		val method = LineNotify.class.getDeclaredMethod("getTokenAndNotifyMessage");
		method.setAccessible(true);

		@SuppressWarnings("unchecked")
		val actualMap = (Map<NotifyAccount, String>) method.invoke(this.lineNotify);

		assertThat(actualMap.size(), is(1));
	}

	@Test
	public void 通知オフの場合Mapのサイズが0になるか() throws Exception {
		//SQLで全てのユーザーの通知をオフ
		executeScript("/db/insertLineNotifications2.sql");

		val method = LineNotify.class.getDeclaredMethod("getTokenAndNotifyMessage");
		method.setAccessible(true);

		@SuppressWarnings("unchecked")
		val actualMap = (Map<NotifyAccount, String>) method.invoke(this.lineNotify);

		assertThat(actualMap.size(), is(0));
	}

	@Test
	public void 問題がない場合Mapのサイズが0になるか() throws Exception {
		//SQLで全てのユーザーの通知をオフ
		executeScript("/db/insertLineNotifications.sql");
		executeScript("/db/deleteReview.sql");

		val method = LineNotify.class.getDeclaredMethod("getTokenAndNotifyMessage");
		method.setAccessible(true);

		@SuppressWarnings("unchecked")
		val actualMap = (Map<NotifyAccount, String>) method.invoke(this.lineNotify);

		assertThat(actualMap.size(), is(0));
	}

	//SQL文を実行
	private void executeScript(String file) {
		val resource = new ClassPathResource(file, getClass());

		val rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		val conn = DataSourceUtils.getConnection(this.jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}
}