package jp.co.code_reminder.core.algorithm;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Timestamp;
import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import jp.co.code_reminder.domain.model.question.Review;
import lombok.val;

@SpringBootTest
public class SuperMemo2Test {

	private final Review review = new Review();
	private final long millisecondsInDay = 60 * 60 * 24 * 1000;
	private final long now = System.currentTimeMillis();

	@Autowired
	private CalculateReviewDateLogic algorithm;

	@BeforeEach
	public void 初期化処理() throws Exception {
		this.review.setId(0);
		this.review.setRepetitions(0);
		this.review.setIntervals(1);
		this.review.setCompleteDay(365);
		this.review.setEasiness(2.5F);
	}

	@Test
	public void スコアが1を10回取ると1日後を返す() throws Exception {
		val expectedDate = new Timestamp(this.now + (this.millisecondsInDay * 1)).toLocalDateTime().toLocalDate();
		LocalDate actualDate = null;

		for (int i = 1; i <= 10; i++) {
			this.algorithm.calculateReviewDate(this.review, 1);
			actualDate = this.review.getNextLearnDate().toLocalDateTime().toLocalDate();
			assertEquals(expectedDate, actualDate);
		}
	}

	@Test
	public void スコアが2を17回取ると373日後を返す() throws Exception {

		for (int i = 1; i <= 17; i++) {
			this.algorithm.calculateReviewDate(this.review, 2);
		}

		val expectedDate = new Timestamp(this.now + (this.millisecondsInDay * 373)).toLocalDateTime().toLocalDate();
		val actualDate = this.review.getNextLearnDate().toLocalDateTime().toLocalDate();

		assertEquals(expectedDate, actualDate);
	}

	@Test
	public void スコアが3を12回取ると406日後を返す() throws Exception {

		for (int i = 1; i <= 12; i++) {
			this.algorithm.calculateReviewDate(this.review, 3);
		}

		val expectedDate = new Timestamp(this.now + (this.millisecondsInDay * 406)).toLocalDateTime().toLocalDate();
		val actualDate = this.review.getNextLearnDate().toLocalDateTime().toLocalDate();

		assertEquals(expectedDate, actualDate);
	}

	@Test
	public void スコアが4を7回取ると595日後を返す() throws Exception {
		for (int i = 1; i <= 7; i++) {
			this.algorithm.calculateReviewDate(this.review, 4);
		}

		val expectedDate = new Timestamp(this.now + (this.millisecondsInDay * 595)).toLocalDateTime().toLocalDate();
		val actualDate = this.review.getNextLearnDate().toLocalDateTime().toLocalDate();

		assertEquals(expectedDate, actualDate);
	}

	@Test
	public void スコアが1_2_3_4を取ると9日後を返す() throws Exception {
		for (int i = 1; i <= 4; i++) {
			this.algorithm.calculateReviewDate(this.review, i);
		}

		val expectedDate = new Timestamp(this.now + (this.millisecondsInDay * 14)).toLocalDateTime().toLocalDate();
		val actualDate = this.review.getNextLearnDate().toLocalDateTime().toLocalDate();

		assertEquals(expectedDate, actualDate);
	}

	@Test
	public void スコアが4_3_2_1を取ると1日後を返す() throws Exception {
		for (int i = 4; 1 <= i; i--) {
			this.algorithm.calculateReviewDate(this.review, i);
		}

		val expectedDate = new Timestamp(this.now + (this.millisecondsInDay * 1)).toLocalDateTime().toLocalDate();
		val actualDate = this.review.getNextLearnDate().toLocalDateTime().toLocalDate();

		assertEquals(expectedDate, actualDate);
	}

}
