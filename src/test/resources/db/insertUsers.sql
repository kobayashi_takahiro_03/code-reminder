DELETE FROM accounts;
DELETE FROM mail_auth_accounts;

INSERT INTO
accounts(uuid,login_id,password,name,mail,role_name)
VALUES
('39479f56-c0c4-43e3-9b84-e67406b66fc2','kobayasi','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林隆弘','takachanman@yahoo.co.jp','ROLE_ADMIN'),
('695d36e1-4bbe-4f4a-b598-02be3ac312f7','maruyama','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','丸山翔太郎','maru@yahoo.co.jp','ROLE_USER'),
('523fdc7c-5956-46e6-9726-b9b58fc997f1','morioka','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','森岡菜海','mori@yahoo.co.jp','ROLE_USER'),
('e034c492-d2c2-449b-a40f-9dea83ebe4c9','hirata','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','平田敦士','hira@yahoo.co.jp','ROLE_USER'),
('75ab6bf1-0ccb-4b10-869e-71e307eef3ab','kobayasitakesi','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林たかし','alh@yahoo.co.jp','ROLE_USER'),
('75ab6bf1-0ccb-4b10-869e-71e307eef3sb','kobayasitake','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林たかし','alf@yahoo.co.jp','ROLE_USER');

INSERT INTO
mail_auth_accounts(uuid,login_id,password,name,mail,role_name)
VALUES
('39479f56-c0c4-43e3-9b84-e67406b66fc2','kobayasi','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林隆弘','takachanman@yahoo.co.jp','ROLE_ADMIN');

INSERT INTO
	line_notifications(account_id,token)
VALUES
	('39479f56-c0c4-43e3-9b84-e67406b66fc2','cwHEt4kZjvDF2jo3DSwxJsRt0rwspBecjgFbvqjKBcE'),
	('695d36e1-4bbe-4f4a-b598-02be3ac312f7',NULL),
	('523fdc7c-5956-46e6-9726-b9b58fc997f1',NULL),
	('e034c492-d2c2-449b-a40f-9dea83ebe4c9',NULL),
	('75ab6bf1-0ccb-4b10-869e-71e307eef3ab',NULL);