DELETE FROM questions;
DELETE FROM reviews;
DELETE FROM categories;
DELETE FROM question_category_categorizations;

SELECT setval ('questions_id_seq', 11, false);
SELECT setval ('categories_id_seq', 8, false);
SELECT setval ('question_category_categorizations_id_seq', 11, false);

INSERT INTO
	questions(id,question,answer,is_learned,is_shared,account_id)
VALUES
	(1,'パイソン1','パイソン1',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(2,'パイソン2','パイソン2',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(3,'パイソン3','パイソン3',0,0,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(4,'数学1','数学1',0,0,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(5,'数学2','数学2',0,0,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(6,'数学3','数学3',0,0,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(7,'ルビー1','ルビー1',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(8,'ルビー2','ルビー2',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(9,'ルビー3','ルビー3',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(10,'ルビー5','ルビー5',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(11,'ルビー6','ルビー6',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(12,'ルビー7','ルビー7',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2');

INSERT INTO
	reviews(id,repetitions,intervals,easiness,complete_day)
VALUES
	(1,0,1,2.5,365),
	(2,0,1,2.5,365),
	(3,0,1,2.5,365),
	(4,0,1,2.5,365),
	(5,0,1,2.5,365),
	(6,0,1,2.5,365),
	(7,0,1,2.5,365),
	(8,0,1,2.5,365),
	(9,0,1,2.5,365),
	(10,0,1,2.5,365),
	(11,0,1,2.5,365),
	(12,0,1,2.5,365);

INSERT INTO
	categories(id,name,account_id)
VALUES
	(1,'数学','39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(6,'ruby','39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(7,'python','39479f56-c0c4-43e3-9b84-e67406b66fc2');

INSERT INTO
	question_category_categorizations(question_id, category_id, account_id)
VALUES
	(1,7,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(2,7,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(3,7,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(4,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(5,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(6,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(7,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(8,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(9,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(10,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(11,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(12,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2');