DELETE FROM accounts;
DELETE FROM line_notifications;
DELETE FROM questions;
DELETE FROM reviews;
DELETE FROM categories;
DELETE FROM question_category_categorizations;

SELECT setval ('questions_id_seq', 11, false);
SELECT setval ('categories_id_seq', 8, false);
SELECT setval ('question_category_categorizations_id_seq', 11, false);

INSERT INTO
accounts(uuid,login_id,password,name,mail,role_name)
VALUES
('39479f56-c0c4-43e3-9b84-e67406b66fc2','kobayasi','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林隆弘','takachanman@yahoo.co.jp','ROLE_ADMIN'),
('695d36e1-4bbe-4f4a-b598-02be3ac312f7','maruyama','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','丸山翔太郎','maru@yahoo.co.jp','ROLE_USER'),
('523fdc7c-5956-46e6-9726-b9b58fc997f1','morioka','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','森岡菜海','mori@yahoo.co.jp','ROLE_USER'),
('e034c492-d2c2-449b-a40f-9dea83ebe4c9','hirata','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','平田敦士','hira@yahoo.co.jp','ROLE_USER'),
('75ab6bf1-0ccb-4b10-869e-71e307eef3ab','kobayasitakesi','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林たかし','alh@yahoo.co.jp','ROLE_USER'),
('75ab6bf1-0ccb-4b10-869e-71e307eef3sb','kobayasitake','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林たかし','alf@yahoo.co.jp','ROLE_USER');

INSERT INTO
	line_notifications(account_id,token)
VALUES
	('39479f56-c0c4-43e3-9b84-e67406b66fc2','cwHEt4kZjvDF2jo3DSwxJsRt0rwspBecjgFbvqjKBcE'),
	('695d36e1-4bbe-4f4a-b598-02be3ac312f7',NULL),
	('523fdc7c-5956-46e6-9726-b9b58fc997f1',NULL),
	('e034c492-d2c2-449b-a40f-9dea83ebe4c9',NULL),
	('75ab6bf1-0ccb-4b10-869e-71e307eef3ab',NULL);


INSERT INTO
	questions(id,question,answer,is_learned,is_shared,account_id)
VALUES
	(1,'パイソン1','パイソン1',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(2,'パイソン2','パイソン2',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(3,'パイソン3','パイソン3',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(4,'数学1','数学1',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(5,'数学2','数学2',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(6,'数学3','数学3',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(7,'ルビー1','ルビー1',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(8,'ルビー2','ルビー2',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(9,'ルビー3','ルビー3',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(10,'ルビー4','ルビー4',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2');

INSERT INTO
	reviews(id,repetitions,intervals,easiness,complete_day)
VALUES
	(1,0,1,2.5,365),
	(2,0,1,2.5,365),
	(3,0,1,2.5,365),
	(4,0,1,2.5,365),
	(5,0,1,2.5,365),
	(6,0,1,2.5,365),
	(7,0,1,2.5,365),
	(8,0,1,2.5,365),
	(9,0,1,2.5,365),
	(10,0,1,2.5,365);

INSERT INTO
	categories(id,name,account_id)
VALUES
	(1,'数学','39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(6,'ruby','39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(7,'python','39479f56-c0c4-43e3-9b84-e67406b66fc2');

INSERT INTO
	question_category_categorizations(question_id, category_id, account_id)
VALUES
	(1,7,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(2,7,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(3,7,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(4,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(5,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(6,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(7,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(8,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(9,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	(10,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2');