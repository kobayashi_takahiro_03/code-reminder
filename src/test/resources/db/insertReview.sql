DELETE FROM reviews;
DELETE FROM categories;
DELETE FROM questions;
DELETE FROM accounts;
DELETE FROM question_category_categorizations;
DELETE FROM mail_auth_accounts;
DELETE FROM forgot_pass;

SELECT setval ('question_category_categorizations_id_seq', 1, false);
SELECT setval ('categories_id_seq', 1, false);
SELECT setval ('questions_id_seq', 1, false);

INSERT INTO
accounts(uuid,login_id,password,name,mail,role_name)
VALUES
('39479f56-c0c4-43e3-9b84-e67406b66fc2','kobayasi','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林隆','takachanman@yahoo.co.jp','ROLE_ADMIN'),
('0b80bf07-6da7-4ec0-a5d2-4ffcc382259f','testaccount2','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林隆司','takachan@yahoo.co.jp','ROLE_USER'),
('0b80bf57-6da7-4ec0-a5d2-4ffcc382259f','testaccount3','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林隆雄','takamanchan@yahoo.co.jp','ROLE_USER'),
('0b80bf87-6da7-4ec0-a5d2-4ffcc382259f','testaccount4','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林清見','takamana@yahoo.co.jp','ROLE_USER'),
('0b80bf77-6da7-4ec0-a5d2-4ffcc382259f','testaccount5','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','小林晴彦','takachaos@yahoo.co.jp','ROLE_USER');

INSERT INTO
questions(question,answer,is_learned,is_shared,account_id)
VALUES
('パンはパンでも食べられないパンはなんだ','フライパン',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
('日本で最初に拳銃の携行が許可された職業はなんだ','郵便局員',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
('今まで投入した費用を無駄にしたくないという思いから切り上げられず損失を膨らまし続けてしまう心理効果はなんだ','サンクコストバイアス/埋没費用効果/コンコルド効果',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
('貨幣価値が下がり続けることによってさらに消費が落ち込む悪循環はなんだ','デフレスパイラル',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
('うっうー','うまうま〜',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
('iPhoneやiPadの広告用デモ画面の時間が9:41で統一されているのは何故でしょうか','iPhoneの発表時間に合わせている',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
('ビルゲイツの本名はなんだ','ウィルアム・ヘンリー・ゲイツ三世',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
('Javaでのメソッドオーバーライドの条件はなんだ','メソッド名、引数の数、型、順番が親と同一であること',0,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
('テスト質問','テスト回答',0,1,'0b80bf07-6da7-4ec0-a5d2-4ffcc382259f'),
('サバを漢字で書くと？','鯖',0,1,'0b80bf57-6da7-4ec0-a5d2-4ffcc382259f'),
('ふとんが？','ふっとんだ',0,1,'0b80bf87-6da7-4ec0-a5d2-4ffcc382259f'),
('あいうえ？','お',0,1,'0b80bf77-6da7-4ec0-a5d2-4ffcc382259f');

INSERT INTO
reviews(id,repetitions,intervals,easiness,complete_day)
VALUES
(1,0,1,2.5,365),
(2,0,1,2.5,365),
(3,0,1,2.5,365),
(4,0,1,2.5,365),
(5,0,1,2.5,365),
(6,0,1,2.5,365),
(7,0,1,2.5,365),
(8,0,1,2.5,365),
(9,0,1,2.5,365),
(10,0,1,2.5,365),
(11,0,1,2.5,365),
(12,0,1,2.5,365);

INSERT INTO
categories(name,account_id)
VALUES
('数学','39479f56-c0c4-43e3-9b84-e67406b66fc2'),
('国語','0b80bf07-6da7-4ec0-a5d2-4ffcc382259f'),
('社会','0b80bf57-6da7-4ec0-a5d2-4ffcc382259f'),
('学問','0b80bf87-6da7-4ec0-a5d2-4ffcc382259f'),
('java','0b80bf77-6da7-4ec0-a5d2-4ffcc382259f'),
('ruby','39479f56-c0c4-43e3-9b84-e67406b66fc2'),
('python','39479f56-c0c4-43e3-9b84-e67406b66fc2');

INSERT INTO
question_category_categorizations(question_id, category_id, account_id)
VALUES
(1,6,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
(2,1,'0b80bf07-6da7-4ec0-a5d2-4ffcc382259f'),
(3,7,'0b80bf57-6da7-4ec0-a5d2-4ffcc382259f'),
(4,6,'0b80bf87-6da7-4ec0-a5d2-4ffcc382259f'),
(5,1,'0b80bf77-6da7-4ec0-a5d2-4ffcc382259f'),
(6,7,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
(7,6,'0b80bf07-6da7-4ec0-a5d2-4ffcc382259f'),
(8,1,'0b80bf57-6da7-4ec0-a5d2-4ffcc382259f'),
(9,7,'0b80bf87-6da7-4ec0-a5d2-4ffcc382259f'),
(10,6,'0b80bf77-6da7-4ec0-a5d2-4ffcc382259f'),
(11,1,'39479f56-c0c4-43e3-9b84-e67406b66fc2'),
(12,7,'39479f56-c0c4-43e3-9b84-e67406b66fc2');

INSERT INTO
	line_notifications(account_id)
VALUES
	('39479f56-c0c4-43e3-9b84-e67406b66fc2'),
	('0b80bf07-6da7-4ec0-a5d2-4ffcc382259f'),
	('0b80bf57-6da7-4ec0-a5d2-4ffcc382259f'),
	('0b80bf87-6da7-4ec0-a5d2-4ffcc382259f'),
	('0b80bf77-6da7-4ec0-a5d2-4ffcc382259f');

UPDATE
reviews
SET
next_learn_date= now() - interval '1 DAY'
WHERE
id = 1;

UPDATE
reviews
SET
next_learn_date= now() + interval '1 DAY'
WHERE
id = 2;
