// ------------------------------------------------- header menu

$(document).ready(function() {
      if(location.pathname != "/") {
          $('.nav-link[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
      } else $('.nav-link:eq(0)').addClass('active');
  });

// ------------------------------------------------- 確認ダイアログ form
$('#delete').on('click',function(e){
    e.preventDefault();
    var form = $(this).parents('form');
    swal({
        title: "確認",
        text: "削除してよろしいですか？",
        icon: "warning",
        type: "warning",
        buttons: ["キャンセル", true],
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }).then(
    function(isConfirm){
        if (isConfirm) form.submit();
    });
});
// ------------------------------------------------- 二重サブミット防止 form

$(".submit-form").submit(function() {
  var self = this;
  $(":submit", self).prop("disabled", true);
  setTimeout(function() {
    $(":submit", self).prop("disabled", false);
  }, 10000);
});

$(".submit-form-sub").submit(function() {
  var self = this;
  $(":submit", self).prop("disabled", true);
  setTimeout(function() {
    $(":submit", self).prop("disabled", false);
  }, 10000);
});

$('#update').on('click', function () {
  var self = this;
  $(":submit", self).prop("disabled", true);
  $(self).css('background-color','#f0f8ff');
  setTimeout(function() {
    $(":submit", self).prop("disabled", false);
  }, 10000);
});

$('#create').on('click', function () {
  var self = this;
  $(":submit", self).prop("disabled", true);
  $(self).css('background-color','#f0f8ff');
  setTimeout(function() {
    $(":submit", self).prop("disabled", false);
  }, 10000);
});

// ------------------------------------------------- 検索ボタン #serch-btn

$(function () {
  $('#search-btn').on('click', () => {
      $('#category-search-box').toggle();
  });
});

// ------------------------------------------------- タイムライン welcome/timeline

$(function(){
  var winScrollTop;
  $('.js-modal-open').each(function(){
      $(this).on('click',function(){
        winScrollTop = $(window).scrollTop();
          var target = $(this).data('target');
          var modal = document.getElementById(target);
          $(modal).fadeIn();
          return false;
      });
  });
  $('.js-modal-close').on('click',function(){
      $('.js-modal').fadeOut();
      $('body,html').stop().animate({scrollTop:winScrollTop}, 100);
      return false;
  });
});