CREATE
TABLE IF NOT EXISTS
	accounts (
						uuid VARCHAR(36) NOT NULL PRIMARY KEY,
						login_id VARCHAR(20) NOT NULL UNIQUE,
						password VARCHAR(255) NOT NULL,
						name VARCHAR(10) NOT NULL,
						mail VARCHAR(255) NOT NULL UNIQUE,
						role_name VARCHAR(15) NOT NULL,
						is_non_locked smallint NOT NULL DEFAULT 0,
						created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
						updated_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
					);

CREATE
TABLE IF NOT EXISTS
	categories (
						id SERIAL NOT NULL PRIMARY KEY,
						name VARCHAR(50) NOT NULL,
					    account_id VARCHAR(36) NOT NULL REFERENCES accounts(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
						created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
						updated_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
					);

CREATE
TABLE IF NOT EXISTS
	forgot_pass (
						uuid VARCHAR(36) NOT NULL PRIMARY KEY,
						mail VARCHAR(255) NOT NULL,
						created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
						updated_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
					);

CREATE
TABLE IF NOT EXISTS
	mail_auth_accounts (
						uuid VARCHAR(36) NOT NULL PRIMARY KEY,
						login_id VARCHAR(20) NOT NULL,
						password VARCHAR(255) NOT NULL,
						name VARCHAR(10) NOT NULL,
						mail VARCHAR(255) NOT NULL,
						role_name VARCHAR(15) NOT NULL,
						created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
						updated_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
					);

CREATE
TABLE IF NOT EXISTS
	questions (
						id SERIAL NOT NULL PRIMARY KEY,
						question TEXT NOT NULL,
						answer TEXT NOT NULL,
						is_learned SMALLINT NOT NULL,
						is_shared SMALLINT NOT NULL ,
					    account_id VARCHAR(36) NOT NULL REFERENCES accounts(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
						created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
						updated_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
					);

CREATE
TABLE IF NOT EXISTS
	reviews (
					    id INT NOT NULL PRIMARY KEY REFERENCES questions(id) ON DELETE CASCADE ON UPDATE CASCADE,
						repetitions SMALLINT NOT NULL,
						intervals SMALLINT NOT NULL,
						easiness NUMERIC NOT NULL,
						complete_day SMALLINT NOT NULL,
						next_learn_date TIMESTAMP,
						created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
						updated_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
					);

CREATE
TABLE IF NOT EXISTS
	question_category_categorizations (
						id SERIAL NOT NULL PRIMARY KEY,
					    question_id INT NOT NULL REFERENCES questions(id) ON DELETE CASCADE ON UPDATE CASCADE,
   					    category_id INT NOT NULL REFERENCES categories(id) ON DELETE CASCADE ON UPDATE CASCADE,
					    account_id VARCHAR(36) NOT NULL REFERENCES accounts(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
						created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
						updated_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
					);

CREATE
TABLE IF NOT EXISTS
	line_notifications  (
					    account_id VARCHAR(36) PRIMARY KEY NOT NULL REFERENCES accounts(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
					    token VARCHAR(50),
					    notification_flag SMALLINT NOT NULL DEFAULT 0,
					    notification_time VARCHAR(2) NOT NULL DEFAULT '9' ,
						created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
						updated_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
					);

CREATE
TABLE IF NOT EXISTS
	persistent_logins (
						username VARCHAR(64) NOT NULL,
			            series VARCHAR(64) PRIMARY KEY,
			            token VARCHAR(64) NOT NULL,
			            last_used TIMESTAMP NOT NULL
			        );

CREATE
TABLE IF NOT EXISTS
failed_authentication (
					    login_id character varying(128) REFERENCES accounts(login_id),
					    authentication_timestamp timestamp without time zone,
					    CONSTRAINT pk_tbl_fa PRIMARY KEY (login_id, authentication_timestamp)
					);

COMMIT;