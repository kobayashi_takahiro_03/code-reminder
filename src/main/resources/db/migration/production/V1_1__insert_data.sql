DELETE FROM reviews;
DELETE FROM categories;
DELETE FROM questions;
DELETE FROM accounts;
DELETE FROM question_category_categorizations;
DELETE FROM mail_auth_accounts;
DELETE FROM forgot_pass;

SELECT setval ('question_category_categorizations_id_seq', 1, false);
SELECT setval ('categories_id_seq', 1, false);
SELECT setval ('questions_id_seq', 1, false);

INSERT INTO
accounts(uuid,login_id,password,name,mail,role_name)
VALUES
('0b80bf97-6da7-4ec0-a5d2-4ffcc382259f','testaccount','$2a$10$rgpfbOCBe4quOR6M1XdvB.BZf2sVAevs6nWPV8ePjlCuhQsW5.P/q','ALH太郎','alh.kakuu.address@alhinc.co.jp','ROLE_ADMIN');

INSERT INTO
questions(question,answer,is_learned,is_shared,account_id)
VALUES
('ALHの事業所数は？','4',0,1,'0b80bf97-6da7-4ec0-a5d2-4ffcc382259f');

INSERT INTO
reviews(id,repetitions,intervals,easiness,complete_day)
VALUES
(1,0,1,2.5,365);

INSERT INTO
categories(name,account_id)
VALUES
('java','0b80bf97-6da7-4ec0-a5d2-4ffcc382259f'),
('ruby','0b80bf97-6da7-4ec0-a5d2-4ffcc382259f'),
('python','0b80bf97-6da7-4ec0-a5d2-4ffcc382259f');

INSERT INTO
question_category_categorizations(question_id, category_id, account_id)
VALUES
(1,1,'0b80bf97-6da7-4ec0-a5d2-4ffcc382259f');

INSERT INTO
line_notifications(account_id)
VALUES
('0b80bf97-6da7-4ec0-a5d2-4ffcc382259f');