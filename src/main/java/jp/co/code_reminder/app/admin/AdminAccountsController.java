package jp.co.code_reminder.app.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.code_reminder.app.account.AccountForm;
import jp.co.code_reminder.app.account.NotifyForm;
import jp.co.code_reminder.app.mapper.EntityAndFormMapper;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.core.message.Message;
import jp.co.code_reminder.core.validator.account.AccountFormValidator;
import jp.co.code_reminder.core.validator.line_notify.NotificationValidator;
import jp.co.code_reminder.domain.model.account.RoleName;
import jp.co.code_reminder.domain.model.line_notification.NotificationFlag;
import jp.co.code_reminder.domain.model.line_notification.Notify;
import jp.co.code_reminder.domain.service.account.AccountReferService;
import jp.co.code_reminder.domain.service.account.AccountRegistService;
import jp.co.code_reminder.domain.service.security.AccountDetail;
import lombok.NonNull;
import lombok.val;

@Controller
@RequestMapping("/admin")
@lombok.RequiredArgsConstructor
public class AdminAccountsController {

	private final AccountFormValidator accountFormValidator;
	private final NotificationValidator notificationValidator;
	private final EntityAndFormMapper mapper;
	private final AccountReferService accountReferService;
	private final AccountRegistService accountRegistService;

	@InitBinder("accountForm")
	public void initBinder(final WebDataBinder binder) {
		binder.addValidators(this.accountFormValidator);
	}

	@InitBinder("notifyForm")
	public void initBinderForNotifyForm(final WebDataBinder binder) {
		binder.addValidators(this.notificationValidator);
	}

	@GetMapping("/accounts")
	public String getAccounts(final @PageableDefault(size = 5) Pageable pageable,
			final @RequestParam(name = "searchLoginId", required = false) String searchLoginId, final Model model) {

		val accountPage = getAccountPage(pageable, searchLoginId);
		model.addAttribute("page", accountPage);
		model.addAttribute("accountFormList", accountPage.getContent());
		model.addAttribute("searchLoginId", searchLoginId);
		model.addAttribute("notifyFormList", getNotifyFormList());

		return "admin/accounts/index";
	}

	@GetMapping("/accounts/{uuid}/edit")
	public String getAccountsEdit(final @PathVariable("uuid") String uuid, final RedirectAttributes redirectAttributes,
			final Model model) {

		try {
			val account = this.accountReferService.findOne(uuid, "uuid");
			val notify = this.accountReferService.findByUuidForNotify(uuid);

			model.addAttribute("accountForm", this.mapper.convertToAccountForm(account));
			model.addAttribute("notifyForm", this.mapper.convertToNotifyForm(notify));
			model.addAttribute("roleNameList", getRoleNameList(uuid));
			model.addAttribute("notificationRadio", NotificationFlag.getMap());
			model.addAttribute("selectTimeList", Notify.getSelectTimeList());
			return "admin/accounts/edit";
		} catch (final RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
		}

	}

	@PostMapping(value = "/accounts/{uuid}/edit", params = "update")
	public String accountsUpdate(
			final @Validated AccountForm accountForm, final BindingResult result,
			final RedirectAttributes redirectAttributes, final Model model) {

		if (result.hasErrors()) {

			try {
				val notify = this.accountReferService.findByUuidForNotify(accountForm.getUuid());
				model.addAttribute("notifyForm", this.mapper.convertToNotifyForm(notify));
				model.addAttribute("roleNameList", getRoleNameList(accountForm.getUuid()));
				model.addAttribute("notificationRadio", NotificationFlag.getMap());
				model.addAttribute("selectTimeList", Notify.getSelectTimeList());
				return "admin/accounts/edit";
			} catch (final RecordNotFoundException e) {
				return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
			}

		}

		try {
			this.accountRegistService.updateAccount(this.mapper.convertToAccount(accountForm));
			return redirectView(redirectAttributes, Message.SUCCESS_UPDATE);
		} catch (final RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_UPDATE);
		}
	}

	@PostMapping(value = "/accounts/{uuid}/edit", name = "delete")
	public String accountsDelete(final @PathVariable("uuid") String uuid, final RedirectAttributes redirectAttributes) {
		try {
			this.accountRegistService.deleteAccount(uuid);
			return redirectView(redirectAttributes, Message.SUCCESS_DELETE);
		} catch (final RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_DELETE);
		} catch (final IllegalArgumentException e) {
			return redirectView(redirectAttributes, Message.ERROR_DELETE_LOGIN_ACCOUNT);
		}
	}

	@PostMapping(value = "/accounts/{uuid}/updateNotify")
	public String upsertLineNotification(
			final @PathVariable("uuid") String uuid,
			final @Validated NotifyForm notifyForm,
			final BindingResult bindingResult, final RedirectAttributes redirectAttributes,
			final Model model) {

		if (bindingResult.hasErrors()) {
			val account = this.accountReferService.findOne(uuid, "uuid");
			model.addAttribute("accountForm", this.mapper.convertToAccountForm(account));
			model.addAttribute("roleNameList", getRoleNameList(uuid));
			model.addAttribute("notificationRadio", NotificationFlag.getMap());
			model.addAttribute("selectTimeList", Notify.getSelectTimeList());
			return "account/edit";
		}

		try {
			this.accountRegistService.upsertNotify(this.mapper.convertToNotify(notifyForm));
			return redirectView(redirectAttributes, Message.SUCCESS_UPDATE);
		} catch (final RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_UPDATE);
		}
	}

	private String redirectView(final RedirectAttributes redirectAttributes, final Message message) {

		String successErrorKey = null;

		if (message.name().startsWith("S")) {
			successErrorKey = Message.SUCCESS.getMessage();
		} else {
			successErrorKey = Message.ERROR.getMessage();
		}

		redirectAttributes.addFlashAttribute(successErrorKey, message.getMessage());
		return "redirect:/admin/accounts";
	}

	private List<String> getRoleNameList(@NonNull final String accountId)
			throws RecordNotFoundException {
		val editAccount = this.accountReferService.findOne(accountId, "uuid");

		if (getLoginUuid().equals(editAccount.getUuid())) {
			return List.copyOf(new ArrayList<String>(Arrays.asList(editAccount.getRoleName().getString())));
		} else {
			return List.copyOf(Arrays.stream(RoleName.values()).map(x -> x.getString()).collect(Collectors.toList()));
		}
	}

	private Page<AccountForm> getAccountPage(@NonNull final Pageable pageable, final String searchLoginId) {

		val count = this.accountReferService.count(searchLoginId);

		if (count == 0) {
			return Page.empty(pageable);
		}

		val accountList = this.accountReferService.findByPageAndLoginId(pageable, searchLoginId);
		val accountFormList = this.mapper.convertToAccountFormList(accountList);

		return new PageImpl<>(accountFormList, pageable, count);
	}

	private List<NotifyForm> getNotifyFormList() {
		return this.mapper.convertToNotifyFormList(this.accountReferService.findAllNotify());
	}

	private String getLoginUuid() throws NullPointerException {
		val loginAccount = ((AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
				.getAccount();

		Objects.requireNonNull(loginAccount, "ログインユーザーが存在しません");

		return loginAccount.getUuid();
	}

}