package jp.co.code_reminder.app.admin;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.code_reminder.app.category.CategoryForm;
import jp.co.code_reminder.app.mapper.EntityAndFormMapper;
import jp.co.code_reminder.app.question.QuestionReviewCategoryForm;
import jp.co.code_reminder.app.question.SearchForm;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.core.message.Message;
import jp.co.code_reminder.core.validator.order.GroupOrder;
import jp.co.code_reminder.domain.model.question.Learn;
import jp.co.code_reminder.domain.model.question.Share;
import jp.co.code_reminder.domain.service.category.CategoryReferService;
import jp.co.code_reminder.domain.service.question.QuestionReferService;
import jp.co.code_reminder.domain.service.question.QuestionRegistService;
import lombok.val;

@Controller
@RequestMapping("/admin")
@lombok.RequiredArgsConstructor
public class AdminQuestionsController {

	private final CategoryReferService categoryReferService;
	private final QuestionRegistService questionRegistService;
	private final EntityAndFormMapper mapper;
	private final QuestionReferService questionReferService;

	@GetMapping("/questions")
	public String getQuestions(final @PageableDefault(size = 5) Pageable pageable,
			final @ModelAttribute("searchForm") SearchForm searchForm,
			final @RequestParam(value = "selectedCategoryNums", required = false) String[] selectedNums,
			final Model model) {

		val qrcFormListPage = getAdminQuestionPage(searchForm, pageable);
		val categoryFormList = getCategoryFormList();

		model.addAttribute("qrcFormList", qrcFormListPage.getContent());
		model.addAttribute("page", qrcFormListPage);
		model.addAttribute("categories", categoryFormList);

		searchForm.setCategoryMap(this.mapper.convertToCategoryMap(categoryFormList));
		model.addAttribute("searchForm", searchForm);
		model.addAttribute("selectedCategoryNumsParam", createCategoryCheckboxQueryParam(selectedNums));

		return "admin/questions/index";
	}

	@GetMapping("/questions/{questionId}/edit")
	public String getQuestionsEdit(final @PathVariable("questionId") String pathQuestionId,
			final RedirectAttributes redirectAttributes,
			final Model model) {

		try {
			val categoryFormList = getCategoryFormList(pathQuestionId);
			model.addAttribute("qrcForm", getQrcForm(pathQuestionId));
			model.addAttribute("completeStatus", Learn.values());
			model.addAttribute("isSharedRadio", Share.getMap());
			model.addAttribute("categoryFormList", this.mapper.convertToCategoryMap(categoryFormList));
			return "admin/questions/edit";
		} catch (final IllegalArgumentException | RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
		}

	}

	@PostMapping(value = "/questions/{questionId}/edit", params = "update")
	public String questionsUpdate(final @PathVariable("questionId") String pathQuestionId,
			final @Validated(GroupOrder.class) @ModelAttribute("qrcForm") QuestionReviewCategoryForm qrcForm,
			final BindingResult result,
			final RedirectAttributes redirectAttributes,
			final Model model) {

		if (result.hasErrors()) {
			try {
				val categoryFormList = getCategoryFormList(pathQuestionId);
				model.addAttribute("completeStatus", Learn.values());
				model.addAttribute("categoryFormList", this.mapper.convertToCategoryMap(categoryFormList));
				return "admin/questions/edit";
			} catch (final IllegalArgumentException | RecordNotFoundException e) {
				return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
			}
		}

		try {
			this.questionRegistService.updateQuestion(this.mapper.convertToQrc(qrcForm),
					qrcForm.getQuestion().getAccountId());
			return redirectView(redirectAttributes, Message.SUCCESS_UPDATE);
		} catch (IllegalArgumentException | RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
		}
	}

	@PostMapping(value = "/questions/{questionId}/edit", name = "delete")
	public String questionsDelete(
			final @PathVariable("questionId") String pathQuestionId,
			final RedirectAttributes redirectAttributes,
			final Model model) {

		try {
			this.questionRegistService.deleteQuestion(pathQuestionId);
			return redirectView(redirectAttributes, Message.SUCCESS_DELETE);
		} catch (final IllegalArgumentException e) {
			return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
		}
	}

	private String redirectView(final RedirectAttributes redirectAttributes, final Message message) {

		String successErrorKey = null;

		if (message.name().startsWith("S")) {
			successErrorKey = Message.SUCCESS.getMessage();
		} else {
			successErrorKey = Message.ERROR.getMessage();
		}

		redirectAttributes.addFlashAttribute(successErrorKey, message.getMessage());
		return "redirect:/admin/questions";
	}

	private Page<QuestionReviewCategoryForm> getAdminQuestionPage(final SearchForm searchForm,
			final Pageable pageable) {

		String accountId = null;
		val count = this.questionReferService.countByAccountIdAndSearchForm(accountId, searchForm);

		if (count == 0) {
			return Page.empty(pageable);
		}
		val qrcList = this.questionReferService.findByPageAndSearchForm(pageable, accountId, searchForm);
		val qrcFormList = this.mapper.convertToQrcFormList(qrcList);

		return new PageImpl<QuestionReviewCategoryForm>(qrcFormList, pageable, count);
	}

	private List<CategoryForm> getCategoryFormList(final String pathQuestionId) {
		return this.mapper.convertCategoryFormList(this.categoryReferService.findByQuestionId(pathQuestionId));
	}

	private List<CategoryForm> getCategoryFormList() {
		return this.mapper.convertCategoryFormList(this.categoryReferService.findAll());
	}

	private QuestionReviewCategoryForm getQrcForm(final String pathQuestionId)
			throws RecordNotFoundException, IllegalArgumentException {
		return this.mapper.convertToQrcForm(this.questionReferService.findByQuestionId(pathQuestionId));
	}

	private String createCategoryCheckboxQueryParam(final String[] selectedNums) {
		if (ArrayUtils.isEmpty(selectedNums)) {
			return "";
		}
		return Arrays.asList(selectedNums).stream().map(x -> "&selectedCategoryNums=" + x)
				.reduce((accum, value) -> value + accum).orElse("");
	}

}
