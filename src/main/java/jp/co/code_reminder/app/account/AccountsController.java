package jp.co.code_reminder.app.account;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.code_reminder.app.mapper.EntityAndFormMapper;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.core.message.Message;
import jp.co.code_reminder.core.validator.account.AccountFormValidator;
import jp.co.code_reminder.core.validator.line_notify.NotificationValidator;
import jp.co.code_reminder.domain.model.line_notification.NotificationFlag;
import jp.co.code_reminder.domain.model.line_notification.Notify;
import jp.co.code_reminder.domain.service.account.AccountReferService;
import jp.co.code_reminder.domain.service.account.AccountRegistService;
import jp.co.code_reminder.domain.service.security.AccountDetail;
import lombok.val;

@Controller
@RequestMapping("/accounts")
@lombok.RequiredArgsConstructor
public class AccountsController {

	private final AccountFormValidator accountFormValidator;
	private final NotificationValidator notificationValidator;
	private final EntityAndFormMapper mapper;
	private final AccountRegistService accountRegistService;
	private final AccountReferService accountReferService;

	@InitBinder("accountForm")
	public void initBinderForAccountForm(final WebDataBinder binder) {
		binder.addValidators(this.accountFormValidator);
	}

	@InitBinder("notifyForm")
	public void initBinderForNotifyForm(final WebDataBinder binder) {
		binder.addValidators(this.notificationValidator);
	}

	@GetMapping(value = { "/mypage", "/mypage/edit" })
	public String getMypage(final Model model, final HttpServletRequest request) {

		val account = this.accountReferService.findOne(getLoginUuid(), "uuid");
		val notify = this.accountReferService.findByUuidForNotify(getLoginUuid());

		model.addAttribute("accountForm", this.mapper.convertToAccountForm(account));
		model.addAttribute("notifyForm", this.mapper.convertToNotifyForm(notify));
		model.addAttribute("notificationRadio", NotificationFlag.getMap());
		model.addAttribute("selectTimeList", Notify.getSelectTimeList());
		return request.getRequestURI().equals("/accounts/mypage") ? "account/show" : "account/edit";
	}

	@PostMapping(value = "/mypage/edit")
	public String updateAccount(
			final @Validated AccountForm accountForm, final BindingResult result,
			final RedirectAttributes redirectAttributes, final HttpServletRequest request,
			final Model model) {

		if (result.hasErrors()) {
			val notify = this.accountReferService.findByUuidForNotify(getLoginUuid());
			model.addAttribute("notifyForm", this.mapper.convertToNotifyForm(notify));
			model.addAttribute("notificationRadio", NotificationFlag.getMap());
			model.addAttribute("selectTimeList", Notify.getSelectTimeList());
			return "account/edit";
		}

		try {
			this.accountRegistService.updateAccount(this.mapper.convertToAccount(accountForm));
			return redirectView(redirectAttributes, Message.SUCCESS_UPDATE);
		} catch (final RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_UPDATE);
		}
	}

	@PostMapping(value = "/mypage/updateNotify")
	public String upsertLineNotification(
			final @Validated NotifyForm notifyForm,
			final BindingResult bindingResult, final RedirectAttributes redirectAttributes,
			final Model model) {

		if (bindingResult.hasErrors()) {
			val account = this.accountReferService.findOne(getLoginUuid(), "uuid");
			model.addAttribute("accountForm", this.mapper.convertToAccountForm(account));
			model.addAttribute("notificationRadio", NotificationFlag.getMap());
			model.addAttribute("selectTimeList", Notify.getSelectTimeList());
			return "account/edit";
		}

		try {
			this.accountRegistService.upsertNotify(this.mapper.convertToNotify(notifyForm));
			return redirectView(redirectAttributes, Message.SUCCESS_UPDATE);
		} catch (final RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_UPDATE);
		}
	}

	private String redirectView(final RedirectAttributes redirectAttributes, final Message message) {

		String successErrorKey = null;

		if (message.name().startsWith("S")) {
			successErrorKey = Message.SUCCESS.getMessage();
		} else {
			successErrorKey = Message.ERROR.getMessage();
		}

		redirectAttributes.addFlashAttribute(successErrorKey, message.getMessage());
		return "redirect:/accounts/mypage";
	}

	private String getLoginUuid() throws NullPointerException {
		val loginAccount = ((AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
				.getAccount();

		Objects.requireNonNull(loginAccount, "ログインユーザーが存在しません");

		return loginAccount.getUuid();
	}

}