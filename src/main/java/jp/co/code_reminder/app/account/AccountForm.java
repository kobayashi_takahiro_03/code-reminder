package jp.co.code_reminder.app.account;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import javax.validation.constraints.Email;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import jp.co.code_reminder.core.validator.order.MailFormat;
import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.model.account.RoleName;
import jp.co.code_reminder.domain.service.security.AccountDetail;

@lombok.Data
@lombok.ToString(exclude = { "password", "confirmPassword" })
public class AccountForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private String uuid;
	private String loginId;
	private String password;
	private String confirmPassword;
	private String name;

	@Email(groups = MailFormat.class, message = "メールアドレスはメールアドレス形式で入力してください")
	private String mail;

	@Email(groups = MailFormat.class, message = "確認用メールアドレスはメールアドレス形式で入力してください")
	private String confirmMail;

	private RoleName roleName;

	public String getUuid() {
		if (StringUtils.isBlank(this.uuid)) {
			this.uuid = UUID.randomUUID().toString();
			return this.uuid;
		}
		return this.uuid;
	}

	public RoleName getRoleName() {
		if (Objects.isNull(getLoginAccount())) {
			this.roleName = RoleName.ROLE_USER;
			return this.roleName;
		}

		if (this.uuid.equals(getLoginAccount().getUuid())) {
			this.roleName = getLoginAccount().getRoleName();
			return this.roleName;
		}

		if (Objects.isNull(this.roleName)) {
			this.roleName = RoleName.ROLE_USER;
			return this.roleName;
		}

		return this.roleName;
	}

	private Account getLoginAccount() {
		try {
			return ((AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getAccount();
		} catch (final ClassCastException | NullPointerException e) {
			return null;
		}

	}
}