package jp.co.code_reminder.app.account;

import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.code_reminder.app.mapper.EntityAndFormMapper;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.core.message.Message;
import jp.co.code_reminder.core.validator.account.AccountFormValidator;
import jp.co.code_reminder.domain.service.account.SignupService;
import jp.co.code_reminder.domain.service.mail.SendMailService;
import lombok.val;

@Controller
@lombok.RequiredArgsConstructor
public class SignupController {

	private final SignupService signupService;
	private final SendMailService sendMailService;
	private final AccountFormValidator accountFormValidator;
	private final EntityAndFormMapper mapper;

	@InitBinder("accountForm")
	public void initBinderForAccountForm(final WebDataBinder binder) {
		binder.addValidators(this.accountFormValidator);
	}

	@GetMapping("/signup")
	public String getSignup(final Model model) {
		model.addAttribute("accountForm", new AccountForm());
		return "account/new";
	}

	@PostMapping( "/signup")
	public String createAccount(
			final @Validated AccountForm accountForm,
			final BindingResult result, final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "account/new";
		}

		try {
			val mailAuthAccount = this.mapper.convertToMailAuthAccount(accountForm);
			this.sendMailService.sendAuthMail(mailAuthAccount);
			this.signupService.createMailAuthAccount(mailAuthAccount);
			return redirectView(redirectAttributes, Message.SUCCESS_MAIL_SEND);
		} catch (final MailSendException e) {
			return redirectView(redirectAttributes, Message.ERROR_MAIL_SEND);
		}

	}

	@GetMapping("/signup/validate/{uuid}")
	public String getAuthAccount(final @PathVariable(name = "uuid") String uuid,
			final RedirectAttributes redirectAttributes) {

		try {
			val mailAuthAccount = this.signupService.findMailAuthAccount(uuid);
			this.signupService.authAccount(this.mapper.convertToAccount(mailAuthAccount));
			return redirectView(redirectAttributes, Message.SUCCESS_AUTHENTICATION);
		} catch (final RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_AUTHENTICATION);
		}
	}

	private String redirectView(final RedirectAttributes redirectAttributes, final Message message) {

		String successErrorKey = null;

		if (message.name().startsWith("S")) {
			successErrorKey = Message.SUCCESS.getMessage();
		} else {
			successErrorKey = Message.ERROR.getMessage();
		}

		redirectAttributes.addFlashAttribute(successErrorKey, message.getMessage());
		return "redirect:/login";
	}

}
