package jp.co.code_reminder.app.account;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import jp.co.code_reminder.domain.model.line_notification.NotificationFlag;

@lombok.Data
public class NotifyForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private String accountId;
	private String token;
	private NotificationFlag notificationFlag;
	private String notificationTime;
	private Timestamp createdDate;
	private Timestamp updatedDate;
	private Map<Integer, NotificationFlag> radioItems;
	private List<String> selectTimeItems;
}
