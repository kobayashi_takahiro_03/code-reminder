package jp.co.code_reminder.app.forgot_pass;

import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.code_reminder.core.message.Message;
import jp.co.code_reminder.core.validator.account.UsedMailValidator;
import jp.co.code_reminder.core.validator.order.GroupOrder;
import jp.co.code_reminder.domain.model.forgot_pass.ForgotPass;
import jp.co.code_reminder.domain.model.forgot_pass.ResetPass;
import jp.co.code_reminder.domain.service.forgot_pass.ForgotPassService;
import jp.co.code_reminder.domain.service.mail.SendMailService;
import lombok.val;
import ma.glasnost.orika.MapperFactory;

@Controller
@RequestMapping("/forgotpass")
@lombok.RequiredArgsConstructor
public class ForgotPassController {

	private final ForgotPassService forgotPassService;
	private final SendMailService sendMailService;
	private final MapperFactory beanMapperFactory;
	private final UsedMailValidator usedMailValidator;

	@InitBinder("forgotPassForm")
	public void initBinder(final WebDataBinder binder) {
		binder.addValidators(this.usedMailValidator);
	}

	@GetMapping
	public String getForgetPass(final Model model) {
		model.addAttribute("forgotPassForm", new ForgotPassForm());
		return "forgotpass/index";
	}

	@PostMapping
	public String requestMail(final @Validated(GroupOrder.class) ForgotPassForm forgotPassForm,
			final BindingResult result, final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "forgotpass/index";
		}

		val forgotPass = convertToForgotPass(forgotPassForm);

		try {
			this.sendMailService.sendResetPassMail(forgotPass);
		} catch (final MailSendException e) {
			redirectAttributes.addFlashAttribute("error", Message.ERROR_MAIL_SEND.getMessage());
			return "redirect:/forgotpass";
		}

		redirectAttributes.addFlashAttribute("success", Message.SUCCESS_MAIL_SEND.getMessage());
		this.forgotPassService.createForgetPassUser(forgotPass);

		return "redirect:/login";
	}

	@GetMapping("/validate/{uuid}")
	public String getResetPassword(final @PathVariable(name = "uuid") String uuid,
			final RedirectAttributes redirectAttributes, final Model model) {

		if (!this.forgotPassService.existForgotPass(uuid, "uuid")) {
			redirectAttributes.addFlashAttribute("error", Message.ERROR_AUTHENTICATION.getMessage());
			return "redirect:/login";
		}

		model.addAttribute("resetPassForm", new ResetPassForm(uuid));
		return "forgotpass/edit";
	}

	@PostMapping(value = "/validate/{uuid}")
	public String updatePassword(
			final @Validated(GroupOrder.class) ResetPassForm resetPassForm,
			final BindingResult result, final @PathVariable(name = "uuid") String uuid,
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "forgotpass/edit";
		}

		if (!this.forgotPassService.existForgotPass(uuid, "uuid")) {
			redirectAttributes.addFlashAttribute("error", Message.ERROR_AUTHENTICATION.getMessage());
			return "redirect:/login";
		}

		val resetPass = convertToResetPass(resetPassForm);
		this.forgotPassService.resetPassword(resetPass);
		redirectAttributes.addFlashAttribute("success", Message.SUCCESS_UPDATE.getMessage());

		return "redirect:/login";
	}

	private ResetPass convertToResetPass(final ResetPassForm resetPassForm) {
		return this.beanMapperFactory.getMapperFacade(ResetPassForm.class, ResetPass.class).map(resetPassForm);
	}

	private ForgotPass convertToForgotPass(final ForgotPassForm forgotPassForm) {
		return this.beanMapperFactory.getMapperFacade(ForgotPassForm.class, ForgotPass.class).map(forgotPassForm);
	}

}
