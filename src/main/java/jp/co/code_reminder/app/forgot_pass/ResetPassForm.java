package jp.co.code_reminder.app.forgot_pass;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import jp.co.code_reminder.core.validator.account.ConfirmPassword;
import jp.co.code_reminder.core.validator.order.ValidGroup1;
import jp.co.code_reminder.core.validator.order.ValidGroup2;
import jp.co.code_reminder.core.validator.order.ValidGroup3;
import jp.co.code_reminder.core.validator.order.ValidGroup4;

@lombok.Data
@lombok.ToString(exclude = { "password", "confirmPassword" })
@ConfirmPassword(field = "password", groups = ValidGroup4.class)
public class ResetPassForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private String uuid;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min = 6, max = 20, groups = ValidGroup2.class)
	@Pattern(regexp = "^[a-zA-Z0-9!-/:-@¥[-`{-~]]+$", groups = ValidGroup3.class)
	private String password;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min = 6, max = 20, groups = ValidGroup2.class)
	@Pattern(regexp = "^[a-zA-Z0-9!-/:-@¥[-`{-~]]+$", groups = ValidGroup3.class)
	private String confirmPassword;

	public ResetPassForm(final String uuid){
		this.uuid = uuid;
	}

}
