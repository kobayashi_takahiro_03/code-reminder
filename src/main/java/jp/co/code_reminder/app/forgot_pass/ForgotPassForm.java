package jp.co.code_reminder.app.forgot_pass;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import jp.co.code_reminder.core.validator.order.ValidGroup1;
import jp.co.code_reminder.core.validator.order.ValidGroup2;

@lombok.Data
public class ForgotPassForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private String uuid;

	@NotBlank(groups = ValidGroup1.class)
	@Email(groups = ValidGroup2.class)
	private String mail;

}
