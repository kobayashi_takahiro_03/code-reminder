package jp.co.code_reminder.app.review;

import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.code_reminder.app.question.QuestionReviewCategoryForm;
import jp.co.code_reminder.domain.model.question.QuestionReviewCategory;
import jp.co.code_reminder.domain.service.question.QuestionReferService;
import jp.co.code_reminder.domain.service.question.QuestionRegistService;
import jp.co.code_reminder.domain.service.security.AccountDetail;
import jp.co.code_reminder.web.interceptor.SessionCreater;
import lombok.val;
import ma.glasnost.orika.MapperFactory;

/**
*
* @author takachanman
*
* qrc = QuestionReviewCategory 長いので省略
* qrcForm = QuestionReviewCategoryForm 長いので省略
*
* リクエストの度に、QuestionReviewCategoryの全件取得クエリが走るのが嫌だったので、セッションで使いまわしています。
* QuestionIntercepterでsession登録または削除しています。
*
*/

@Controller
@RequestMapping("/reviews")
@lombok.RequiredArgsConstructor
public class ReviewsController {

	private final QuestionRegistService questionRegistService;
	private final QuestionReferService questionReferService;
	private final MapperFactory beanMapperFactory;
	private final SessionCreater creater;
	private final HttpSession session;

	@GetMapping
	public String getReview(final Model model) {

		int newQuestionCount = 0;
		int reviewQuestionCount = 0;
		int nextQuestionId = 0;
		val qrcFormList = getQrcFormList();

		if (!CollectionUtils.isEmpty(qrcFormList)) {
			newQuestionCount = (int) qrcFormList.stream()
					.filter(x -> Objects.isNull(x.getReview().getNextLearnDate())).count();
			reviewQuestionCount = (int) qrcFormList.stream()
					.filter(x -> Objects.nonNull(x.getReview().getNextLearnDate())).count();
			nextQuestionId = qrcFormList.peek().getQuestion().getId();
		}

		model.addAttribute("newQuestionCount", newQuestionCount);
		model.addAttribute("reviewQuestionCount", reviewQuestionCount);
		model.addAttribute("nextQuestionId", nextQuestionId);

		return "review/index";
	}

	@GetMapping("/question")
	public String getEdit(final Model model) {

		val qrcFormList = getQrcFormList();

		if (CollectionUtils.isEmpty(qrcFormList)) {
			return "redirect:/reviews";
		}

		val qrcForm = qrcFormList.peek();
		model.addAttribute("question", qrcForm.getQuestion());

		return "review/edit";
	}

	@PostMapping("/question")
	public String update(final @RequestParam(name = "score", required = false) String scoreParam,
			final RedirectAttributes redirectAttributes) {

		val qrcFormList = getQrcFormList();

		if (CollectionUtils.isEmpty(qrcFormList)) {
			return "redirect:/reviews";
		}

		val qrcForm = qrcFormList.poll();
		this.questionRegistService.updateReview(getLoginUuid(), qrcForm.getQuestion().getId(), scoreParam);

		redirectAttributes.addFlashAttribute("question", qrcForm.getQuestion());
		return "redirect:/reviews/answer";
	}

	@GetMapping("/answer")
	public String getAnswer(final Model model) {

		if (Objects.isNull(model.getAttribute("question"))) {
			return "redirect:/reviews";
		}

		val qrcFormList = getQrcFormList();
		var nextQuestionId = 0;
		if (!CollectionUtils.isEmpty(qrcFormList)) {
			nextQuestionId = qrcFormList.peek().getQuestion().getId();
		}

		model.addAttribute("nextQuestionId", nextQuestionId);

		return "review/show";
	}

	private ConcurrentLinkedQueue<QuestionReviewCategoryForm> getQrcFormList() {

		if (Objects.isNull(this.session.getAttribute(this.creater.REVIEW_QUESTION))) {
			this.creater.createQrcFormReview(this.creater.REVIEW_QUESTION);
		}

		@SuppressWarnings("unchecked")
		var qrcFormListSession = (ConcurrentLinkedQueue<QuestionReviewCategoryForm>) this.session
				.getAttribute(this.creater.REVIEW_QUESTION);

		if (CollectionUtils.isEmpty(qrcFormListSession)) {
			var qrcList = this.questionReferService.findByAccountIdAndToday(getLoginUuid());

			if (CollectionUtils.isEmpty(qrcList)) {
				return new ConcurrentLinkedQueue<QuestionReviewCategoryForm>();
			}

			val beanMapper = this.beanMapperFactory.getMapperFacade(QuestionReviewCategory.class,
					QuestionReviewCategoryForm.class);

			val qrcFormList = qrcList.stream().map(x -> beanMapper.map(x)).collect(Collectors.toList());
			qrcFormListSession = new ConcurrentLinkedQueue<QuestionReviewCategoryForm>(qrcFormList);
			this.session.setAttribute("reviewQuestion", qrcFormListSession);
		}

		return qrcFormListSession;
	}

	private String getLoginUuid() throws NullPointerException {
		val loginAccount = ((AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
				.getAccount();

		Objects.requireNonNull(loginAccount, "ログインユーザーが存在しません");

		return loginAccount.getUuid();
	}
}
