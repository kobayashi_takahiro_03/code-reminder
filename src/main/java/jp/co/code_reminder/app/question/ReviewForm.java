package jp.co.code_reminder.app.question;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import jp.co.code_reminder.core.validator.order.ValidGroup1;

@lombok.Data
public class ReviewForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private Integer repetitions;

	private Integer intervals;

	private Float easiness;

	@NotNull(groups = ValidGroup1.class)
	private Integer completeDay;

	private String nextLearnDate;

	private Timestamp createdDate;

	private Timestamp updatedDate;

	public Integer getCompleteDay() {

		if (Objects.isNull(this.completeDay)) {
			this.completeDay = 365;
			return this.completeDay;
		}

		return this.completeDay;
	}

}