package jp.co.code_reminder.app.question;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import jp.co.code_reminder.core.validator.order.ValidGroup1;
import jp.co.code_reminder.core.validator.order.ValidGroup2;
import jp.co.code_reminder.domain.model.question.Learn;
import jp.co.code_reminder.domain.model.question.Share;

@lombok.Data
public class QuestionForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min = 1, max = 100000, groups = ValidGroup2.class)
	private String question;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min = 1, max = 100000, groups = ValidGroup2.class)
	private String answer;

	@NotNull(groups = ValidGroup1.class)
	private Learn isLearned;

	@NotNull(groups = ValidGroup1.class)
	private Share isShared;

	private String accountId;

	private Timestamp createdDate;

	private Timestamp updatedDate;

	public Learn getIsLearned() {

		if (Objects.isNull(this.isLearned)) {
			this.isLearned = Learn.NotLearned;
			return this.isLearned;
		}

		return this.isLearned;
	}

}