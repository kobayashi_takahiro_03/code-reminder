package jp.co.code_reminder.app.question;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@lombok.Data
public class SearchForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private String searchWord;
	private List<Integer> selectedCategoryNums;
	private Map<Integer, String> categoryMap;

	public List<Integer> getSelectedCategoryNums() {
		if (Objects.isNull(this.selectedCategoryNums)) {
			return Collections.emptyList();
		}
		return this.selectedCategoryNums;
	}

}
