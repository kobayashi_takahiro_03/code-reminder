package jp.co.code_reminder.app.question;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

@lombok.Data
public class QuestionReviewCategoryForm implements Serializable {

	private static final long serialVersionUID = 1L;

	@Valid
	private QuestionForm question;

	@Valid
	private ReviewForm review;
	private List<Integer> categoryNumList;

}