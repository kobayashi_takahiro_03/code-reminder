package jp.co.code_reminder.app.question;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.code_reminder.app.category.CategoryForm;
import jp.co.code_reminder.app.mapper.EntityAndFormMapper;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.core.message.Message;
import jp.co.code_reminder.core.validator.order.GroupOrder;
import jp.co.code_reminder.domain.model.question.Learn;
import jp.co.code_reminder.domain.model.question.Share;
import jp.co.code_reminder.domain.service.category.CategoryReferService;
import jp.co.code_reminder.domain.service.question.QuestionReferService;
import jp.co.code_reminder.domain.service.question.QuestionRegistService;
import jp.co.code_reminder.domain.service.security.AccountDetail;
import lombok.val;

/**
 *
 * @author takachanman
 *
 * qrc = QuestionReviewCategory 長いので省略
 * qrcForm = QuestionReviewCategoryForm 長いので省略
 *
 */

@Controller
@RequestMapping("/questions")
@lombok.RequiredArgsConstructor
public class QuestionsController {

	private final QuestionRegistService questionRegistService;
	private final QuestionReferService questionReferService;
	private final CategoryReferService categoryReferService;
	private final EntityAndFormMapper mapper;

	@GetMapping
	public String getIndex(
			final @PageableDefault(size = 5) Pageable pageable,
			final @ModelAttribute("searchForm") SearchForm searchForm,
			final @RequestParam(value = "selectedCategoryNums", required = false) String[] selectedNums,
			final Model model) {

		val qrcFormListPage = getQuestionPageable(searchForm, pageable);
		val categoryFormList = getCategoryFormList();

		model.addAttribute("qrcFormList", qrcFormListPage.getContent());
		model.addAttribute("page", qrcFormListPage);
		model.addAttribute("categories", categoryFormList);

		searchForm.setCategoryMap(this.mapper.convertToCategoryMap(categoryFormList));
		model.addAttribute("searchForm", searchForm);
		model.addAttribute("selectedCategoryNumsParam", createCategoryCheckboxQueryParam(selectedNums));
		return "question/index";
	}

	@GetMapping("/new")
	public String getNew(final @ModelAttribute("qrcForm") QuestionReviewCategoryForm qrcForm, final Model model) {
		val categoryFormList = getCategoryFormList();
		model.addAttribute("isSharedRadio", Share.getMap());
		model.addAttribute("categoryMap", this.mapper.convertToCategoryMap(categoryFormList));
		return "question/new";
	}

	@PostMapping("/new")
	public String postNew(
			final @Validated(GroupOrder.class) @ModelAttribute("qrcForm") QuestionReviewCategoryForm qrcForm,
			final BindingResult result,
			final RedirectAttributes redirectAttributes,
			final Model model) {

		if (result.hasErrors()) {
			return getNew(qrcForm, model);
		}

		try {
			this.questionRegistService.createQuestion(this.mapper.convertToQrc(qrcForm), getLoginUuid());
			return redirectView(redirectAttributes, Message.SUCCESS_CREATE);
		} catch (final RecordNotFoundException | IllegalArgumentException e) {
			return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
		} catch (final DuplicateKeyException e) {
			return redirectView(redirectAttributes, Message.ERROR_INSERT);
		}
	}

	@GetMapping("/{questionId}/edit")
	public String getEdit(final @PathVariable("questionId") String questionId,
			final RedirectAttributes redirectAttributes,
			final Model model) {

		try {
			model.addAttribute("categoryFormList", this.mapper.convertToCategoryMap(getCategoryFormList()));
			model.addAttribute("completeStatus", Learn.values());
			model.addAttribute("isSharedRadio", Share.getMap());
			model.addAttribute("qrcForm", getQrcForm(questionId, getLoginUuid()));
			return "question/edit";
		} catch (final IllegalArgumentException | RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
		}
	}

	@PostMapping(value = "/{questionId}/edit", params = "update")
	public String update(final @PathVariable("questionId") String pathQuestionId,
			final @Validated(GroupOrder.class) @ModelAttribute("qrcForm") QuestionReviewCategoryForm qrcForm,
			final BindingResult result,
			final RedirectAttributes redirectAttributes,
			final Model model) {

		if (result.hasErrors()) {
			model.addAttribute("completeStatus", Learn.values());
			model.addAttribute("categoryFormList", this.mapper.convertToCategoryMap(getCategoryFormList()));
			return "question/edit";
		}

		try {
			this.questionRegistService.updateQuestion(this.mapper.convertToQrc(qrcForm), getLoginUuid());
			return redirectView(redirectAttributes, Message.SUCCESS_UPDATE);
		} catch (final IllegalArgumentException | RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
		}
	}

	@PostMapping(value = "/{questionId}/edit", name = "delete")
	public String delete(final @PathVariable("questionId") String pathQuestionId,
			final RedirectAttributes redirectAttributes) {

		try {
			this.questionRegistService.deleteQuestion(pathQuestionId);
			return redirectView(redirectAttributes, Message.SUCCESS_DELETE);
		} catch (final IllegalArgumentException e) {
			return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
		}
	}

	private String redirectView(final RedirectAttributes redirectAttributes, final Message message) {

		String successErrorKey = null;

		if (message.name().startsWith("S")) {
			successErrorKey = Message.SUCCESS.getMessage();
		} else {
			successErrorKey = Message.ERROR.getMessage();
		}

		redirectAttributes.addFlashAttribute(successErrorKey, message.getMessage());
		return "redirect:/questions";
	}

	private String getLoginUuid() throws NullPointerException {
		val account = ((AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
				.getAccount();

		Objects.requireNonNull(account, "ログインユーザーが存在しません");

		return account.getUuid();
	}

	private List<CategoryForm> getCategoryFormList() {
		return this.mapper.convertCategoryFormList(this.categoryReferService.findByAccountId(getLoginUuid()));
	}

	private Page<QuestionReviewCategoryForm> getQuestionPageable(final SearchForm searchForm, final Pageable pageable) {

		val count = this.questionReferService.countByAccountIdAndSearchForm(getLoginUuid(), searchForm);

		if (count == 0) {
			return Page.empty(pageable);
		}
		val qrcList = this.questionReferService.findByPageAndSearchForm(pageable, getLoginUuid(), searchForm);
		val qrcFormList = this.mapper.convertToQrcFormList(qrcList);

		return new PageImpl<QuestionReviewCategoryForm>(qrcFormList, pageable, count);
	}

	private QuestionReviewCategoryForm getQrcForm(final String pathQuestionId, final String accountId)
			throws RecordNotFoundException, IllegalArgumentException {
		return this.mapper.convertToQrcForm(this.questionReferService.findByQuestionId(pathQuestionId, accountId));
	}

	private String createCategoryCheckboxQueryParam(final String[] selectedNums) {
		if (ArrayUtils.isEmpty(selectedNums)) {
			return "";
		}
		return Arrays.asList(selectedNums).stream().map(x -> "&selectedCategoryNums=" + x)
				.reduce((accum, value) -> value + accum).orElse("");
	}

}
