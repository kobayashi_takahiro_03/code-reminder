package jp.co.code_reminder.app.auth;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LogoutController {

	@GetMapping("/logout")
	public String getLogout() {
		return "redirect:/login";
	}

}
