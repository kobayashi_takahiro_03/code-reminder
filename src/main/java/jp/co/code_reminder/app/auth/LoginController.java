package jp.co.code_reminder.app.auth;

import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

	@GetMapping("/login")
	public String getLogin() {
		return "auth/login";
	}

	@PostMapping("/login-error")
	public String onAuthenticationError(final @RequestParam(name = "loginId", required = false) String loginId,
			final @RequestParam(name = "password", required = false) String password,
			final @RequestAttribute("SPRING_SECURITY_LAST_EXCEPTION") AuthenticationException ex,
			final Model model) {
		model.addAttribute("authenticationException", ex);
		model.addAttribute("loginId", loginId);

		return "auth/login";
	}

}