package jp.co.code_reminder.app.category;

import java.io.Serializable;
import java.sql.Timestamp;

@lombok.Data
public class CategoryForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private String accountId;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
