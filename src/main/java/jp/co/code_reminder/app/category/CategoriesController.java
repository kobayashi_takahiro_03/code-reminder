package jp.co.code_reminder.app.category;

import java.util.List;
import java.util.Objects;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.code_reminder.app.mapper.EntityAndFormMapper;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.core.message.Message;
import jp.co.code_reminder.core.validator.category.CategoryFormValidator;
import jp.co.code_reminder.domain.service.category.CategoryReferService;
import jp.co.code_reminder.domain.service.category.CategoryRegistService;
import jp.co.code_reminder.domain.service.security.AccountDetail;
import lombok.NonNull;
import lombok.val;

@Controller
@RequestMapping("/categories")
@lombok.RequiredArgsConstructor
public class CategoriesController {

	private final CategoryRegistService categoryRegistService;
	private final EntityAndFormMapper mapper;
	private final CategoryFormValidator categoryFormValidator;
	private final CategoryReferService categoryReferService;

	@InitBinder(value = { "categoryForms", "categoryForm" })
	public void initBinder(final WebDataBinder binder) {
		binder.addValidators(this.categoryFormValidator);
	}

	@GetMapping
	public String getIndex(final Model model, final @PageableDefault(size = 5) Pageable pageable,
			final @RequestParam(name = "searchCategoryName", required = false) String searchCategoryName) {

		val categoryListPage = getCategoryPage(pageable, searchCategoryName);
		model.addAttribute("categoryFormList", categoryListPage.getContent());
		model.addAttribute("searchCategoryName", searchCategoryName);
		model.addAttribute("page", categoryListPage);
		return "category/index";
	}

	@GetMapping("/edit")
	public String getEdit(final Model model, final @PageableDefault(size = 5) Pageable pageable,
			final @RequestParam(name = "searchCategoryName", required = false) String searchCategoryName) {

		val categoryListPage = getCategoryPage(pageable, searchCategoryName);
		model.addAttribute("categoryForms", new CategoryForms(categoryListPage.getContent()));
		model.addAttribute("categoryForm", new CategoryForm());
		model.addAttribute("page", categoryListPage);
		return "category/edit";
	}

	@PostMapping(value = "/edit", params = "update")
	public String update(final @Validated CategoryForms categoryForms,
			final BindingResult bindingResult,
			final @PageableDefault(size = 5) Pageable pageable,
			final RedirectAttributes redirectAttributes,
			final Model model,
			final @RequestParam(name = "searchCategoryName", required = false) String searchCategoryName) {

		if (bindingResult.hasErrors()) {
			val categoryListPage = getCategoryPage(pageable, searchCategoryName);
			model.addAttribute("categoryForms", categoryForms);
			model.addAttribute("categoryForm", new CategoryForm());
			model.addAttribute("page", categoryListPage);
			return "category/edit";
		}

		try {
			this.categoryRegistService.update(getLoginUuid(), this.mapper.convertCategoryList(categoryForms));
			return redirectView(redirectAttributes, Message.SUCCESS_UPDATE);
		} catch (final RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_DELETE_CATEGORY);
		} catch (final IllegalArgumentException e) {
			return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
		}
	}

	@PostMapping(value = "/edit", params = "create")
	public String create(final @Validated CategoryForm categoryForm,
			final BindingResult bindingResult,
			final @PageableDefault(size = 5) Pageable pageable,
			final RedirectAttributes redirectAttributes,
			final Model model,
			final @RequestParam(name = "searchCategoryName", required = false) String searchCategoryName) {

		if (bindingResult.hasErrors()) {
			val categoryListPage = getCategoryPage(pageable, searchCategoryName);
			model.addAttribute("page", categoryListPage);
			model.addAttribute("categoryForms", new CategoryForms(categoryListPage.getContent()));
			return "category/edit";
		}

		this.categoryRegistService.create(this.mapper.convertToCategory(categoryForm));
		return redirectView(redirectAttributes, Message.SUCCESS_CREATE);
	}

	@PostMapping(value = "/edit", name = "delete")
	public String delete(@RequestParam(name = "deleteNum", required = false) final List<Integer> deleteNumList,
			final RedirectAttributes redirectAttributes) {

		try {
			this.categoryRegistService.deleteMultiple(getLoginUuid(), deleteNumList);
			return redirectView(redirectAttributes, Message.SUCCESS_DELETE);
		} catch (final RecordNotFoundException e) {
			return redirectView(redirectAttributes, Message.ERROR_DELETE_CATEGORY);
		} catch (final IllegalArgumentException e) {
			return redirectView(redirectAttributes, Message.ERROR_ILLEGAL_PARAMETER);
		}
	}

	private String redirectView(final RedirectAttributes redirectAttributes, final Message message) {

		String successErrorKey = null;

		if (message.name().startsWith("S")) {
			successErrorKey = Message.SUCCESS.getMessage();
		} else {
			successErrorKey = Message.ERROR.getMessage();
		}

		redirectAttributes.addFlashAttribute(successErrorKey, message.getMessage());
		return "redirect:/categories/edit";
	}

	private String getLoginUuid() throws NullPointerException {
		val loginAccount = ((AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
				.getAccount();

		Objects.requireNonNull(loginAccount, "ログインユーザーが存在しません");

		return loginAccount.getUuid();
	}

	private Page<CategoryForm> getCategoryPage(@NonNull final Pageable pageable, final String searchCategoryName) {

		val count = this.categoryReferService.count(getLoginUuid(), searchCategoryName);

		if (0 == count) {
			return Page.empty(pageable);
		}

		val categoryList = this.categoryReferService.findByPageAndAccountIdAndName(pageable, getLoginUuid(),
				searchCategoryName);
		val categoryFormList = this.mapper.convertCategoryFormList(categoryList);

		return new PageImpl<>(categoryFormList, pageable, count);
	}

}