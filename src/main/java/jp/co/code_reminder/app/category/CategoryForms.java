package jp.co.code_reminder.app.category;

import java.io.Serializable;
import java.util.List;

@lombok.Data
public class CategoryForms implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<CategoryForm> categoryForm;

	public CategoryForms(final List<CategoryForm> categoryForm) {
		this.categoryForm = categoryForm;
	}

}
