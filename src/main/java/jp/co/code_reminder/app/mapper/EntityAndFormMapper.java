package jp.co.code_reminder.app.mapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import jp.co.code_reminder.app.account.AccountForm;
import jp.co.code_reminder.app.account.NotifyForm;
import jp.co.code_reminder.app.category.CategoryForm;
import jp.co.code_reminder.app.category.CategoryForms;
import jp.co.code_reminder.app.question.QuestionReviewCategoryForm;
import jp.co.code_reminder.app.welcome.QuestionAccountCategoryForm;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.model.account.MailAuthAccount;
import jp.co.code_reminder.domain.model.category.Category;
import jp.co.code_reminder.domain.model.line_notification.Notify;
import jp.co.code_reminder.domain.model.question.QuestionAccountCategory;
import jp.co.code_reminder.domain.model.question.QuestionReviewCategory;
import lombok.NonNull;
import lombok.val;
import ma.glasnost.orika.MapperFactory;

@Component
@lombok.RequiredArgsConstructor
public class EntityAndFormMapper{

	private final MapperFactory beanMapperFactory;

	/***************************************** ここからAccount関連 *****************************************/

	public Account convertToAccount(@NonNull final AccountForm accountForm) {
		return this.beanMapperFactory.getMapperFacade(AccountForm.class, Account.class).map(accountForm);
	}

	public AccountForm convertToAccountForm(@NonNull final Account account) {
		return this.beanMapperFactory.getMapperFacade(Account.class, AccountForm.class).map(account);
	}

	public NotifyForm convertToNotifyForm(@NonNull final Notify notify) {
		return this.beanMapperFactory.getMapperFacade(Notify.class, NotifyForm.class).map(notify);
	}

	public Notify convertToNotify(@NonNull final NotifyForm notifyForm) {
		return this.beanMapperFactory.getMapperFacade(NotifyForm.class, Notify.class).map(notifyForm);
	}

	public Account convertToAccount(@NonNull final MailAuthAccount mailAuthAccount) throws RecordNotFoundException {
		return this.beanMapperFactory.getMapperFacade(MailAuthAccount.class, Account.class).map(mailAuthAccount);
	}

	public MailAuthAccount convertToMailAuthAccount(@NonNull final AccountForm accountForm) {
		return this.beanMapperFactory.getMapperFacade(AccountForm.class, MailAuthAccount.class).map(accountForm);
	}


	public List<AccountForm> convertToAccountFormList(final List<Account> accountList) {

		if (CollectionUtils.isEmpty(accountList)) {
			return Collections.emptyList();
		}

		val beanMapper = this.beanMapperFactory.getMapperFacade(Account.class, AccountForm.class);
		return List.copyOf(accountList.stream().map(x -> beanMapper.map(x)).collect(Collectors.toList()));
	}

	public List<NotifyForm> convertToNotifyFormList(final List<Notify> notifyList) {

		if (CollectionUtils.isEmpty(notifyList)) {
			return Collections.emptyList();
		}

		val beanMapper = this.beanMapperFactory.getMapperFacade(Notify.class, NotifyForm.class);
		return List.copyOf(notifyList.stream().map(x -> beanMapper.map(x)).collect(Collectors.toList()));
	}

	/***************************************** ここまでAccount関連 *****************************************/

	/***************************************** ここからQuestion関連 *****************************************/

	public QuestionReviewCategory convertToQrc(@NonNull final QuestionReviewCategoryForm qrcForm) {
		return this.beanMapperFactory.getMapperFacade(QuestionReviewCategoryForm.class, QuestionReviewCategory.class)
				.map(qrcForm);
	}

	public QuestionReviewCategoryForm convertToQrcForm(@NonNull final QuestionReviewCategory qrc) {
		return this.beanMapperFactory.getMapperFacade(QuestionReviewCategory.class, QuestionReviewCategoryForm.class)
				.map(qrc);
	}

	public List<QuestionReviewCategoryForm> convertToQrcFormList(final List<QuestionReviewCategory> qrcList) {
		if (CollectionUtils.isEmpty(qrcList)) {
			return Collections.emptyList();
		}

		val beanMapper = this.beanMapperFactory.getMapperFacade(QuestionReviewCategory.class,
				QuestionReviewCategoryForm.class);

		return List.copyOf(qrcList.stream().map(x -> beanMapper.map(x)).collect(Collectors.toList()));
	}

	public Map<Integer, String> convertToCategoryMap(final List<CategoryForm> categoryFormList) {

		if (CollectionUtils.isEmpty(categoryFormList)) {
			return Collections.emptyMap();
		}

		return Map.copyOf(
				categoryFormList.stream().collect(Collectors.toMap(CategoryForm::getId, CategoryForm::getName)));
	}

	public List<QuestionAccountCategoryForm> convertQacFormList(final List<QuestionAccountCategory> qacList) {

		if (CollectionUtils.isEmpty(qacList)) {
			return Collections.emptyList();
		}

		val beanMapper = this.beanMapperFactory.getMapperFacade(QuestionAccountCategory.class,
				QuestionAccountCategoryForm.class);

		return List.copyOf(qacList.stream().map(x -> beanMapper.map(x)).collect(Collectors.toList()));
	}

	/***************************************** ここまでQuestion関連 *****************************************/

	/***************************************** ここからCategory関連 *****************************************/

	public Category convertToCategory(@NonNull final CategoryForm categoryForm) {
		return this.beanMapperFactory.getMapperFacade(CategoryForm.class, Category.class).map(categoryForm);
	}

	public List<CategoryForm> convertCategoryFormList(final List<Category> categoryList) {

		if (CollectionUtils.isEmpty(categoryList)) {
			Collections.emptyList();
		}

		val beanMapper = this.beanMapperFactory.getMapperFacade(Category.class, CategoryForm.class);
		return List.copyOf(categoryList.stream().map(x -> beanMapper.map(x)).collect(Collectors.toList()));
	}

	public List<Category> convertCategoryList(@NonNull final CategoryForms categoryForms) {

		val categoryFormList = categoryForms.getCategoryForm();

		if (CollectionUtils.isEmpty(categoryFormList)) {
			return Collections.emptyList();
		}

		val beanMapper = this.beanMapperFactory.getMapperFacade(CategoryForm.class, Category.class);
		return List.copyOf(categoryFormList.stream().map(x -> beanMapper.map(x)).collect(Collectors.toList()));
	}

	/***************************************** ここまでCategory関連 *****************************************/

}
