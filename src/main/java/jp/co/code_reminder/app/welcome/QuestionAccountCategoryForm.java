package jp.co.code_reminder.app.welcome;

import java.util.List;

import javax.validation.Valid;

import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.model.question.Question;

@lombok.Data
public class QuestionAccountCategoryForm {

	@Valid
	private Question question;
	@Valid
	private Account account;
	private List<String> categoryList;
}
