package jp.co.code_reminder.app.welcome;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import jp.co.code_reminder.app.mapper.EntityAndFormMapper;
import jp.co.code_reminder.domain.model.question.SearchType;
import jp.co.code_reminder.domain.service.question.QuestionReferService;
import lombok.val;

@Controller
@lombok.RequiredArgsConstructor
public class TimelineController {

	private final QuestionReferService questionReferService;
	private final EntityAndFormMapper mapper;

	@GetMapping("/timeline")
	public String getTimeline(final @PageableDefault(size = 5) Pageable pageable,
			final @ModelAttribute("genericSearchForm") GenericSearchForm genericSearchForm,
			final @RequestParam(value = "searchWord", required = false) String searchWord,
			final Model model) {

		val qacFormListPage = getQuestionPageable(genericSearchForm, pageable);

		model.addAttribute("page", qacFormListPage);
		model.addAttribute("categories", createSearchParam(searchWord, genericSearchForm));
		model.addAttribute("qacFormList", qacFormListPage.getContent());

		model.addAttribute("searchTypeRadio", SearchType.createSearchTypeMap());
		model.addAttribute("genericSearchForm", genericSearchForm);

		return "welcome/timeline";
	}

	private Page<QuestionAccountCategoryForm> getQuestionPageable(final GenericSearchForm genericSearchForm,
			final Pageable pageable) {

		val count = this.questionReferService.countByGenericSearchForm(genericSearchForm);

		if (count == 0) {
			return Page.empty(pageable);
		}

		val qrcList = this.questionReferService.findByPageAndGenericSearchForm(pageable, genericSearchForm);
		val qrcFormList = this.mapper.convertQacFormList(qrcList);

		return new PageImpl<QuestionAccountCategoryForm>(qrcFormList, pageable, count);
	}

	private String createSearchParam(final String searchWord, GenericSearchForm searchForm) {
		if (StringUtils.isEmpty(searchWord)) {
			return "";
		}
		var searchParam = new StringBuilder();
		searchParam.append("&searchWord=" + searchWord);
		searchParam.append("&searchType=" + searchForm.getSearchType());

		return searchParam.toString();
	}

}
