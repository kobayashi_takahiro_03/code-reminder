package jp.co.code_reminder.app.welcome;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
@Controller
public class HomeController {
	@GetMapping("/")
	public String index() {
		return "welcome/index";
	}
	@GetMapping("/terms")
	public String terms() {
		return "welcome/terms";
	}
	@GetMapping("/privacy")
	public String privacy() {
		return "welcome/privacy";
	}

	@GetMapping("/line-description")
	public String lineDescription() {
		return "welcome/line_description";
	}

	@GetMapping("/about")
	public String about() {
		return "welcome/about";

	}
	@GetMapping("/contact")
	public String contact() {
		return "welcome/contact";
	}

	@PostMapping("/")
	public String getIndex() {
		return "redirect:/";
	}
}