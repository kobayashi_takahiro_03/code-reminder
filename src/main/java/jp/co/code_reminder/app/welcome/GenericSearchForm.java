package jp.co.code_reminder.app.welcome;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

import jp.co.code_reminder.domain.model.question.SearchType;

@lombok.Data
public class GenericSearchForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private String searchWord;
	private SearchType searchType;
	private Map<Integer, String> searchTypeMap;

	public SearchType getSearchType() {
		if (Objects.isNull(this.searchType)) {
			this.searchType = SearchType.Question;
		}
		return this.searchType;
	}

}
