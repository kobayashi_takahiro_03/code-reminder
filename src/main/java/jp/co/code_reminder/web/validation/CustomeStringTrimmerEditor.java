package jp.co.code_reminder.web.validation;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import lombok.val;

final class CustomeStringTrimmerEditor extends PropertyEditorSupport {

	private final boolean emptyAsNull;

	CustomeStringTrimmerEditor(final boolean emptyAsNull) {
		this.emptyAsNull = emptyAsNull;
	}

	@Override
	public String getAsText() {
		val value = this.getValue();

		if (Objects.nonNull(value)) {
			return value.toString();
		} else {
			return "";
		}

	}

	@Override
	public void setAsText(final String text) {
		if (Objects.isNull(text)) {
			this.setValue((Object) null);
		} else {
			val value = StringUtils.strip(text);

			if (this.emptyAsNull && "".equals(value)) {
				this.setValue((Object) null);
			} else {
				this.setValue(value);
			}
		}
	}
}