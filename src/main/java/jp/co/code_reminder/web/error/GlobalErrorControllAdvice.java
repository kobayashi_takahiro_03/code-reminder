package jp.co.code_reminder.web.error;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Component
@ControllerAdvice
public final class GlobalErrorControllAdvice {

	@ExceptionHandler(Exception.class)
	public String exceptionHandler(final Exception e, final Model model) {
		model.addAttribute("error", "内部サーバーエラー");
		model.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR);
		System.out.println(e.getMessage());
		e.printStackTrace();

		return "error/business_logic_error";
	}
}

//package jp.co.code_reminder.web.error;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.dao.DuplicateKeyException;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Component;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//
//import jp.co.code_reminder.core.exception.RecordNotFoundException;
//
//@Component
//@ControllerAdvice
//public final class GlobalErrorControllAdvice {
//	private static final Logger logger = LoggerFactory.getLogger(GlobalErrorControllAdvice.class);// ログ出力の際に新規で追加
//
//	@ExceptionHandler(DuplicateKeyException.class)
//	public String duplicateKeyExceptionExceptionHandler(final DuplicateKeyException e, final Model model) {
//		model.addAttribute("error", "内部サーバーエラー");
//		model.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR);
//		System.out.println(e.getMessage());
//		logger.error("内部サーバーエラー",e);
//		e.printStackTrace();
//
//		return "error/datebase_error";
//	}
//
//	@ExceptionHandler(RecordNotFoundException.class)
//	public String recordNotFoundHandler(final RecordNotFoundException e, final Model model) {
//		model.addAttribute("error", "内部サーバーエラー");
//		model.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR);
//		System.out.println(e.getMessage());
//		logger.error("内部サーバーエラー",e);
//		e.printStackTrace();
//
//		return "error/record_not_found_error";
//	}
//
//	@ExceptionHandler(Exception.class)
//	public String exceptionHandler(final Exception e, final Model model) {
//		model.addAttribute("error", "内部サーバーエラー");
//		model.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR);
//		System.out.println(e.getMessage());
//		logger.error("内部サーバーエラー",e);// ログ出力の際に新規で追加
//		e.printStackTrace();
//
//		return "error/business_logic_error";
//	}
//}