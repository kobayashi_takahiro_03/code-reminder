package jp.co.code_reminder.web.interceptor;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import lombok.val;

/**
 *
 * @author takachanman
 *
 * /question → 属性名"question" List<QuestionReviewCategoryForm> アクセスするだけなのでArrayList
 * /reviews, /reviews/{id} → 属性名"questionReview" ConcurrentLinkedQueue<QuestionReviewCategoryForm>
 * →削除等をするので、ConcurrentLinkedQueueで定義
 *
 */

@Component
@lombok.RequiredArgsConstructor
public final class QuestionIntercepter implements HandlerInterceptor {

	private final SessionCreater sessionCreater;
	private final HttpSession session;

	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
			throws Exception {

		if (handler instanceof ResourceHttpRequestHandler) {
			return true;
		}

		val url = request.getRequestURI();

		if (url.equals("/reviews") || url.matches("/reviews/answer") || url.matches("/reviews/question")) {
			this.sessionCreater.createQrcFormReview(this.sessionCreater.REVIEW_QUESTION);
			return true;
		}

		if (Objects.nonNull(this.session.getAttribute(this.sessionCreater.REVIEW_QUESTION)))  {
			this.session.removeAttribute(this.sessionCreater.REVIEW_QUESTION);
		}


		return true;
	}

	@Override
	public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler,
			@Nullable ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(final HttpServletRequest request, final HttpServletResponse response,
			final Object handler, @Nullable Exception ex) throws Exception {
	}
}
