package jp.co.code_reminder.web.interceptor;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import jp.co.code_reminder.app.question.QuestionReviewCategoryForm;
import jp.co.code_reminder.domain.model.question.QuestionReviewCategory;
import jp.co.code_reminder.domain.service.question.QuestionReferService;
import jp.co.code_reminder.domain.service.security.AccountDetail;
import lombok.val;
import ma.glasnost.orika.MapperFactory;

/**
 *
 * REVIEW_QUESTION → ReviewControllerで使います
 * 問題回答時の情報をセッションに保存します。
 *
 * 上記コントローラーで使用するセッションを作成します
 *
 * @author takachanman
 *
 */

@Component
@lombok.RequiredArgsConstructor
public final class SessionCreater {

	private final QuestionReferService questionReferService;
	private final MapperFactory beanMapperFactory;
	private final HttpSession session;

	public final String REVIEW_QUESTION = "reviewQuestion";

	public void createQrcFormReview(final String sessionName) {

		@SuppressWarnings("unchecked")
		var qrcFormSessionList = (Queue<QuestionReviewCategoryForm>) this.session.getAttribute(sessionName);

		if (CollectionUtils.isEmpty(qrcFormSessionList)) {

			val qrcList = this.questionReferService.findByAccountIdAndToday(getLoginUuid());

			if (CollectionUtils.isEmpty(qrcList)) {
				return;
			}

			val beanMapper = this.beanMapperFactory.getMapperFacade(QuestionReviewCategory.class,
					QuestionReviewCategoryForm.class);
			val qrcFormList = qrcList.stream().map(x -> beanMapper.map(x)).collect(Collectors.toList());
			this.session.setAttribute(sessionName, new ConcurrentLinkedQueue<QuestionReviewCategoryForm>(qrcFormList));
		}

	}


	private String getLoginUuid() throws NullPointerException {
		val loginAccount = ((AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
				.getAccount();

		Objects.requireNonNull(loginAccount, "ログインユーザーが存在しません");

		return loginAccount.getUuid();
	}

}
