package jp.co.code_reminder.domain.repository.question;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import jp.co.code_reminder.domain.model.question.Learn;
import jp.co.code_reminder.domain.model.question.Question;

@Mapper
public interface QuestionRepository {

	boolean existIdAndAccountId(@Param("questionId") Integer questionId,
			@Param("accountId") String accountId);

	void insert(Question quesiton);

	void update(Question quesiton);

	void updateIsLearned(@Param("questionId") Integer questionId, @Param("isLearned") Learn learn);

	void delete(Integer quesitonId);

	int count(String accountId);

	boolean existId(@Param("questionId") Integer questionId);
}
