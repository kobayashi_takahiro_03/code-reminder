package jp.co.code_reminder.domain.repository.question;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import jp.co.code_reminder.app.question.SearchForm;
import jp.co.code_reminder.domain.model.question.QuestionReviewCategory;

@Mapper
public interface QuestionReviewCategoryRepository {

	List<QuestionReviewCategory> findByPageAndSearchForm(
			@Param("pageable") Pageable pageable,
			@Param("accountId") String accountId,
			@Param("searchForm") SearchForm searchForm);

	List<QuestionReviewCategory> findByAccountId(String accountId);

	List<QuestionReviewCategory> findByAccountIdAndToday(
			@Param("accountId") String accountId,
			@Param("today") Timestamp today);

	Optional<QuestionReviewCategory> findByQuestionId(Integer id);

	int count(@Param("accountId") String accountId, @Param("searchForm") SearchForm searchForm);
}
