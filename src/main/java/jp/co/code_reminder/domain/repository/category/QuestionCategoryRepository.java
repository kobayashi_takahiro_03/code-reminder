package jp.co.code_reminder.domain.repository.category;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import jp.co.code_reminder.domain.model.category.QuestionCategory;

@Mapper
public interface QuestionCategoryRepository {

	List<QuestionCategory> findByQuestionId(Integer questionId);

	List<QuestionCategory> findByAccountId(String accountId);

	int update(QuestionCategory questionCategory);

	int deleteAll(@Param("deleteIdList") List<Integer> deleteIdList);

	void insertList(@Param("questionCategoryList") List<QuestionCategory> questionCategoryList);

	int count(String accountId);

}
