package jp.co.code_reminder.domain.repository.question;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import jp.co.code_reminder.app.welcome.GenericSearchForm;
import jp.co.code_reminder.domain.model.question.QuestionAccountCategory;

@Mapper
public interface QuestionAccountCategoryRepository {

	List<QuestionAccountCategory> findByPageAndGenericSearchForm(
			@Param("pageable") Pageable pageable,
			@Param("genericSearchForm") GenericSearchForm genericSearchForm);

	int count(@Param("genericSearchForm") GenericSearchForm genericSearchForm);

}
