package jp.co.code_reminder.domain.repository.authenticationevent;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import jp.co.code_reminder.domain.model.authentication.FailedAuthentication;

@Mapper
public interface FailedAuthenticationRepository {

    int create(FailedAuthentication event);

    List<FailedAuthentication> findLatest(@Param("loginId")String loginId, @Param("count")long count);

    int deleteByUsername(String loginId);
}