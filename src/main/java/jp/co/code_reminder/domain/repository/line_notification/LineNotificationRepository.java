package jp.co.code_reminder.domain.repository.line_notification;

import java.util.List;
import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;

import jp.co.code_reminder.domain.model.line_notification.Notify;
import jp.co.code_reminder.domain.model.line_notification.NotifyAccount;

@Mapper
public interface LineNotificationRepository {

	Optional<Notify> findByUuIdForNotify(String uuid);

	List<NotifyAccount> findNotifyAndAccount();

	List<Notify> findAllNotify();

	void upsertNotify(Notify notify);

	boolean existAccountId(String accountId);
}
