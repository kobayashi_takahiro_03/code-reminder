package jp.co.code_reminder.domain.repository.account;

import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;

import jp.co.code_reminder.domain.model.account.MailAuthAccount;

@Mapper
public interface MailAuthAccountRepository {

	Optional<MailAuthAccount> findByUuid(String uuid);

	boolean existUuid(String uuid);

	void insert(MailAuthAccount mailAuthAccount);

	void delete(String uuid);

}
