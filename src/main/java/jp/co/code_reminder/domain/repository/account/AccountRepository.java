package jp.co.code_reminder.domain.repository.account;

import java.util.List;
import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import jp.co.code_reminder.domain.model.account.Account;

@Mapper
public interface AccountRepository {

	Optional<Account> findOne(@Param("target") String target, @Param("column") String column);

	List<Account> findAll(@Param("pageable") Pageable pageable, @Param("searchLoginId") String searchLoginId);

	int count(String searchLoginId);

	boolean existAccount(@Param("target") String target, @Param("column") String column);

	void insert(Account account);

	void delete(String id);

	void update(Account account);

	void updatePass(Account account);

}