package jp.co.code_reminder.domain.repository.question;

import java.util.List;
import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;

import jp.co.code_reminder.domain.model.question.Review;

@Mapper
public interface ReviewRepository {

	Optional<Review> findById(Integer id);

	List<Review> findAll();

	void insert(Review review);

	void delete(Integer id);

	void update(Review review);

	void updateCompleteDay(Integer completeDay);

}
