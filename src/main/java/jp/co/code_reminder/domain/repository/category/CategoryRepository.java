package jp.co.code_reminder.domain.repository.category;

import java.util.List;
import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import jp.co.code_reminder.domain.model.category.Category;

@Mapper
public interface CategoryRepository {

	List<Category> findByAccountIdAndPage(@Param("pageable") Pageable pageable,
			@Param("accountId") String accountIdString, @Param("searchCategoryName") String searchCategoryName);

	List<Category> findByAccountId(String accountId);

	List<Category> findByQuestionId(Integer questionId);

	Optional<Category> findByIdAndId(@Param("accountId") String accountId, @Param("id") Integer id);

	List<Category> findAll();

	boolean existCategory(@Param("accountId") String accountId, @Param("target") String target,
			@Param("column") String column);

	int count(@Param("accountId") String accountId, @Param("searchCategoryName") String searchCategoryName);

	void insert(Category category);

	void delete(@Param("deleteNumList") List<Integer> deleteNumList);

	void update(@Param("categoryList") List<Category> categoryList);
}
