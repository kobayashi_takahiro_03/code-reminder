package jp.co.code_reminder.domain.repository.forgot_pass;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.repository.query.Param;

import jp.co.code_reminder.domain.model.forgot_pass.ForgotPass;
import jp.co.code_reminder.domain.model.forgot_pass.ResetPass;

@Mapper
public interface ForgotPassRepository {

	void insert(String uuid);

	void insertForgotPassUser(ForgotPass forgotPass);

	void deleteByUuid(String uuid);

	String getAccountUuid(String uuid);

	boolean existForgotPass(@Param("target") String target, @Param("column") String column);

	void resetPass(ResetPass resetPass);

}
