package jp.co.code_reminder.domain.service.question;

import java.util.List;

import org.springframework.data.domain.Pageable;

import jp.co.code_reminder.app.question.SearchForm;
import jp.co.code_reminder.app.welcome.GenericSearchForm;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.question.QuestionAccountCategory;
import jp.co.code_reminder.domain.model.question.QuestionReviewCategory;

public interface QuestionReferService {

	List<QuestionReviewCategory> findByPageAndSearchForm(Pageable pageable, String accountId,
			SearchForm searchForm);

	List<QuestionReviewCategory> findByAccountId(String accountId);

	List<QuestionReviewCategory> findByAccountIdAndToday(String accountId);

	List<QuestionAccountCategory> findByPageAndGenericSearchForm(Pageable pageable,
			GenericSearchForm genericSearchForm);

	QuestionReviewCategory findByQuestionId(String pathQuestionId, String accountId)
			throws RecordNotFoundException, IllegalArgumentException;

	QuestionReviewCategory findByQuestionId(String pathQuestionId)
			throws RecordNotFoundException, IllegalArgumentException;

	int countByAccountIdAndSearchForm(String accountId, SearchForm searchForm);

	int countByGenericSearchForm(GenericSearchForm genericSearchForm);

}
