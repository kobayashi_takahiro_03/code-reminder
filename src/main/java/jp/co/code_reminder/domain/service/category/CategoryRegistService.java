package jp.co.code_reminder.domain.service.category;

import java.util.List;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.category.Category;

public interface CategoryRegistService {

	public void create(Category category);

	public void deleteMultiple(final String accountId, List<Integer> deleteNumList)
			throws RecordNotFoundException, IllegalArgumentException;

	void update(String accountId, List<Category> categoryList) throws IllegalArgumentException;

}
