package jp.co.code_reminder.domain.service.account;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.model.account.MailAuthAccount;

public interface SignupService {

	void authAccount(Account account);

	void createMailAuthAccount(MailAuthAccount mailAuthAccount);

	MailAuthAccount findMailAuthAccount(String uuid) throws RecordNotFoundException;

	boolean existMailAuthAccount(String uuid);

}
