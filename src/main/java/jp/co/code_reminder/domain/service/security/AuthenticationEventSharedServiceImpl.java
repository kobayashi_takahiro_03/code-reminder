package jp.co.code_reminder.domain.service.security;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.terasoluna.gfw.common.date.ClassicDateFactory;

import jp.co.code_reminder.domain.model.authentication.FailedAuthentication;
import jp.co.code_reminder.domain.repository.authenticationevent.FailedAuthenticationRepository;
import jp.co.code_reminder.domain.service.account.AccountReferService;
import lombok.val;

@Service
@lombok.RequiredArgsConstructor
public class AuthenticationEventSharedServiceImpl implements AuthenticationEventSharedService {

	private final ClassicDateFactory dateFactory;
	private final FailedAuthenticationRepository failedAuthenticationRepository;
	private final AccountReferService accountReferService;

	@Override
	@Transactional(readOnly = true)
	public List<FailedAuthentication> findLatestFailureEvents(String loginId,
			int count) {
		return failedAuthenticationRepository.findLatest(loginId, count);
	}

	/**
	 * 成功したときの処理
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void authenticationSuccess(final String loginId) {
		deleteFailureEventByUsername(loginId);
	}

	/**
	 * 存在するアカウントのログインに失敗した場合ユーザー情報を登録します
	 */
	@Override
	@Transactional
	public void authenticationFailure(final String loginId) {
		if (this.accountReferService.existAccount(loginId, "login_id")) {
			val failureEvents = new FailedAuthentication();
			failureEvents.setLoginId(loginId);
			failureEvents.setAuthenticationTimestamp(this.dateFactory.newTimestamp().toLocalDateTime());
			this.failedAuthenticationRepository.create(failureEvents);
		}
	}

	/**
	 * ログイン成功時に失敗記録をDBから消します。
	 */
	@Override
	@Transactional
	public int deleteFailureEventByUsername(final String loginId) {
		return this.failedAuthenticationRepository.deleteByUsername(loginId);
	}

}