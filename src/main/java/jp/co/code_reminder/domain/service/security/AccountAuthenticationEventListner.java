package jp.co.code_reminder.domain.service.security;

import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import lombok.val;

/**
 * ログイン時に呼ばれるクラスです。
 * @author shotaromaruyama
 *
 */
@Component
@lombok.RequiredArgsConstructor
public class AccountAuthenticationEventListner {

	private final AuthenticationEventSharedService authenticationEventSharedService;

	/**
	 * ログイン成功時に呼ばれるイベントです
	 * @param event
	 */
	@EventListener
	public void onApplicationSuccessEvent(final AuthenticationSuccessEvent event) {
		val details = (AccountDetail) event.getAuthentication().getPrincipal();
		this.authenticationEventSharedService.authenticationSuccess(details.getUsername());
	}

	/**
	 * ログイン失敗時に呼ばれるイベントです。
	 * @param event
	 */
	@EventListener
	public void onApplicationFailureEvent(final AuthenticationFailureBadCredentialsEvent event) {
		val loginId = (String) event.getAuthentication().getPrincipal();
		this.authenticationEventSharedService.authenticationFailure(loginId);
	}

}
