package jp.co.code_reminder.domain.service.category;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.category.Category;
import jp.co.code_reminder.domain.repository.category.CategoryRepository;
import lombok.NonNull;

@Service
@Transactional(rollbackFor = Exception.class, readOnly = true)
@lombok.RequiredArgsConstructor
class CategoryReferServiceImpl implements CategoryReferService {

	private final CategoryRepository categoryRepository;

	@Override
	public List<Category> findByAccountId(@NonNull final String accountId) {
		return List.copyOf(this.categoryRepository.findByAccountId(accountId));
	}

	@Override
	public List<Category> findByQuestionId(final String pathQuestionId) throws IllegalArgumentException {
		return this.categoryRepository.findByQuestionId(parseQuestionIdParam(pathQuestionId));
	}

	@Override
	public List<Category> findByPageAndAccountIdAndName(@NonNull final Pageable pageable,
			@NonNull final String accountId, final String searchCategoryName) {
		return List.copyOf(this.categoryRepository.findByAccountIdAndPage(pageable, accountId, searchCategoryName));
	}

	@Override
	public Category findByAccountIdAndId(@NonNull final String accountId, @NonNull final Integer id){
		return this.categoryRepository.findByIdAndId(accountId, id).orElseThrow(RecordNotFoundException::new);
	}

	@Override
	public List<Category> findAll() {
		return this.categoryRepository.findAll();
	}

	@Override
	public boolean existIdAndAccountId(final List<Integer> categoryNumList, @NonNull final String accountId) {

		final List<Integer> registeredCategoryNumList = this.categoryRepository.findByAccountId(accountId)
				.parallelStream().map(Category::getId).collect(Collectors.toList());

		if (CollectionUtils.isEmpty(registeredCategoryNumList)) {
			return false;
		}

		return categoryNumList.parallelStream().allMatch(x -> registeredCategoryNumList.contains(x));
	}

	@Override
	public int count(@NonNull final String accountId, final String searchCategoryName) {
		return this.categoryRepository.count(accountId, searchCategoryName);
	}

	private Integer parseQuestionIdParam(final String questionId) throws IllegalArgumentException {
		if (!questionId.matches("^[0-9]*$") || StringUtils.isBlank(questionId)) {
			throw new IllegalArgumentException();
		}
		return Integer.valueOf(questionId);
	}
}