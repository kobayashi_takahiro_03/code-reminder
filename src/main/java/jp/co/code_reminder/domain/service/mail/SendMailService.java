package jp.co.code_reminder.domain.service.mail;

import java.util.List;

import org.springframework.mail.MailSendException;

import jp.co.code_reminder.domain.model.account.MailAuthAccount;
import jp.co.code_reminder.domain.model.forgot_pass.ForgotPass;
import jp.co.code_reminder.domain.model.line_notification.NotifyAccount;

public interface SendMailService {

	void sendAuthMail(MailAuthAccount mailAuthAccount) throws MailSendException;

	void sendResetPassMail(ForgotPass forgotPass) throws MailSendException;

	void sendNotify(List<NotifyAccount> notifyAccountList) throws MailSendException;

}
