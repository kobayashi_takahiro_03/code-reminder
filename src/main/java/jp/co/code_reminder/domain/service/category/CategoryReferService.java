package jp.co.code_reminder.domain.service.category;

import java.util.List;

import org.springframework.data.domain.Pageable;

import jp.co.code_reminder.domain.model.category.Category;

public interface CategoryReferService {

	List<Category> findByPageAndAccountIdAndName(Pageable pageable, String accountId, String searchCategoryName);

	List<Category> findByAccountId(String accountId);

	List<Category> findByQuestionId(String pathQuestionId);

	List<Category> findAll();

	Category findByAccountIdAndId(String accountId, Integer id);

	boolean existIdAndAccountId(List<Integer> categoryNumList, String accountId);

	int count(String accountId, String searchCategoryName);

}
