package jp.co.code_reminder.domain.service.account;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.model.account.MailAuthAccount;
import jp.co.code_reminder.domain.model.line_notification.Notify;
import jp.co.code_reminder.domain.repository.account.AccountRepository;
import jp.co.code_reminder.domain.repository.account.MailAuthAccountRepository;
import jp.co.code_reminder.domain.repository.line_notification.LineNotificationRepository;
import lombok.NonNull;

@Service
@lombok.RequiredArgsConstructor
public class SignupServiceImpl implements SignupService {

	private final AccountRepository accountRepository;
	private final MailAuthAccountRepository mailAuthAccountRepository;
	private final LineNotificationRepository lineNotificationRepository;
	private final PasswordEncoder passwordEncoder;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void authAccount(@NonNull final Account account) {
		this.accountRepository.insert(account);
		this.lineNotificationRepository.upsertNotify(new Notify(account.getUuid()));
		this.mailAuthAccountRepository.delete(account.getUuid());
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public MailAuthAccount findMailAuthAccount(@NonNull final String uuid) throws RecordNotFoundException {
		return this.mailAuthAccountRepository.findByUuid(uuid).orElseThrow(RecordNotFoundException::new);
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public boolean existMailAuthAccount(@NonNull final String uuid) {
		return this.mailAuthAccountRepository.existUuid(uuid);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createMailAuthAccount(@NonNull final MailAuthAccount mailAuthAccount) {
		mailAuthAccount.setPassword(this.passwordEncoder.encode(mailAuthAccount.getPassword()));
		this.mailAuthAccountRepository.insert(mailAuthAccount);
	}

}
