package jp.co.code_reminder.domain.service.mail;

import java.util.List;

import org.springframework.mail.MailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.core.mail.MailContent;
import jp.co.code_reminder.core.mail.UrlProperty;
import jp.co.code_reminder.core.message.Message;
import jp.co.code_reminder.domain.model.account.MailAuthAccount;
import jp.co.code_reminder.domain.model.forgot_pass.ForgotPass;
import jp.co.code_reminder.domain.model.line_notification.NotifyAccount;
import jp.co.code_reminder.domain.repository.account.AccountRepository;
import lombok.NonNull;
import lombok.val;

@Service
@lombok.RequiredArgsConstructor
class SendMailServiceImpl implements SendMailService {


	private final AccountRepository accountRepository;
	private final UrlProperty urlProperty;
	private final MailSender mailSender;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void sendAuthMail(@NonNull final MailAuthAccount mailAuthAccount) {
		val mailContent = MailContent.builder()
				.path("signup/validate/" + mailAuthAccount.getUuid())
				.title("code_reminder アカウント確認のお願い")
				.message("以下のリンクにアクセスしてアカウントを認証してください")
				.urlProperty(this.urlProperty)
				.name(mailAuthAccount.getName())
				.mail(mailAuthAccount.getMail())
				.build();

		this.mailSender.send(mailContent.createMail());
	}

	@Override
	public void sendResetPassMail(@NonNull final ForgotPass forgotPass) throws RecordNotFoundException {
		val mailContent = MailContent.builder()
				.path("forgotpass/validate/" + forgotPass.getUuid())
				.title("code_reminder パスワード再設定の手続き")
				.message("以下のリンクにアクセスしてアカウントを認証してください")
				.urlProperty(this.urlProperty)
				.name(this.accountRepository.findOne(forgotPass.getMail(), "mail")
						.orElseThrow(() -> {
							val message = "mail:" + forgotPass.getMail() + Message.ERROR_RECORD_NOTFOUND.getMessage();
							return new RecordNotFoundException(message);
						}).getName())
				.mail(forgotPass.getMail())
				.build();

		this.mailSender.send(mailContent.createMail());
	}

	@Override
	public void sendNotify(final List<NotifyAccount> notifyAccountList) {
		val mailContentBuilder = MailContent.builder()
				.path("account/mypage/edit")
				.title("code_reminder LINEアクセストークンが無効です")
				.message("登録されたアクセストークンが無効です。以下のurlから修正をお願いします。")
				.urlProperty(this.urlProperty);

		for (val notifyAccount : notifyAccountList) {
			val mailContent = mailContentBuilder
					.name(notifyAccount.getName())
					.mail(notifyAccount.getMail())
					.build();

			this.mailSender.send(mailContent.createMail());
		}
	}

}
