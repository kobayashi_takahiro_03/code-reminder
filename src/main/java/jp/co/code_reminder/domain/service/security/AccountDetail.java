package jp.co.code_reminder.domain.service.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import jp.co.code_reminder.domain.model.account.Account;

public class AccountDetail implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Account account;
	private Collection<GrantedAuthority> authorities;

	public AccountDetail(final Account account, final Collection<GrantedAuthority> authorities, final boolean isNonLocked) {
		this.account = account;
		this.authorities = authorities;
		this.account.setNonLocked(isNonLocked);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.account.getPassword();
	}

	@Override
	public String getUsername() {
		return this.account.getLoginId();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {

		return account.isNonLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}


	public Account getAccount() {
		return this.account;
	}
}
