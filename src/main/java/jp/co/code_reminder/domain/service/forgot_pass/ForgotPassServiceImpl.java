package jp.co.code_reminder.domain.service.forgot_pass;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.code_reminder.domain.model.forgot_pass.ForgotPass;
import jp.co.code_reminder.domain.model.forgot_pass.ResetPass;
import jp.co.code_reminder.domain.repository.forgot_pass.ForgotPassRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;

@Service
@RequiredArgsConstructor
@Transactional
class ForgotPassServiceImpl implements ForgotPassService {

	private final ForgotPassRepository forgotPassRepository;
	private final PasswordEncoder passwordEncoder;

	@Override
	public void resetPassword(@NonNull final ResetPass resetPass) {
		val accountsUuid = this.forgotPassRepository.getAccountUuid(resetPass.getUuid());
		resetPass.setPassword(this.passwordEncoder.encode(resetPass.getPassword()));
		if (Objects.nonNull(accountsUuid)) {
			deleteForgotPassUser(resetPass.getUuid());
		}
		resetPass.setUuid(accountsUuid);
		this.forgotPassRepository.resetPass(resetPass);
	}

	@Override
	public boolean existForgotPass(@NonNull final String target, @NonNull final String column) {

		if (StringUtils.isEmpty(target)) {
			return false;
		}

		return this.forgotPassRepository.existForgotPass(target, column);
	}

	@Override
	public void createForgetPassUser(@NonNull final ForgotPass forgotPass) {
		this.forgotPassRepository.insertForgotPassUser(forgotPass);
	}

	@Override
	public void deleteForgotPassUser(@NonNull final String uuid) {
		this.forgotPassRepository.deleteByUuid(uuid);
	}
}