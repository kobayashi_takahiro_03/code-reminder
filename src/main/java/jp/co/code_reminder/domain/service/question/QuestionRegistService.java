package jp.co.code_reminder.domain.service.question;

import org.springframework.dao.DuplicateKeyException;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.question.QuestionReviewCategory;

public interface QuestionRegistService {

	void updateQuestion(QuestionReviewCategory questionReviewCategory, String accountId)
			throws IllegalArgumentException;

	void updateReview(String accountId, Integer questionId, String scoreParam)
			throws RecordNotFoundException, IllegalArgumentException;

	void deleteQuestion(String pathQuestionId) throws IllegalArgumentException;

	void createQuestion(QuestionReviewCategory questionReviewCategory, String accountId)
			throws DuplicateKeyException, RecordNotFoundException, IllegalArgumentException;

}
