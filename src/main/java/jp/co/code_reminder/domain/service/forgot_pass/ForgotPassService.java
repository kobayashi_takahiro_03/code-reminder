package jp.co.code_reminder.domain.service.forgot_pass;

import jp.co.code_reminder.domain.model.forgot_pass.ForgotPass;
import jp.co.code_reminder.domain.model.forgot_pass.ResetPass;

public interface ForgotPassService {

	void resetPassword(ResetPass resetPass);

	boolean existForgotPass(String target, String column);

	void createForgetPassUser(ForgotPass forgotPass);

	void deleteForgotPassUser(String uuid);

}
