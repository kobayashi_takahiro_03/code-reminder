package jp.co.code_reminder.domain.service.security;

import java.util.List;

import jp.co.code_reminder.domain.model.authentication.FailedAuthentication;

public interface AuthenticationEventSharedService {

	List<FailedAuthentication> findLatestFailureEvents(String loginId, int count);

	void authenticationSuccess(String loginId);

	void authenticationFailure(String loginId);

	int deleteFailureEventByUsername(String loginId);
}
