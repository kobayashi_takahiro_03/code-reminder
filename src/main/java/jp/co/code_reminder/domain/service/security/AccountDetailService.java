package jp.co.code_reminder.domain.service.security;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.terasoluna.gfw.common.date.DefaultClassicDateFactory;

import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.repository.account.AccountRepository;
import lombok.val;

@Service
@PropertySource("classpath:/security.properties")
@lombok.RequiredArgsConstructor
public class AccountDetailService implements UserDetailsService {

	private final AccountRepository accountRepository;
	private final AuthenticationEventSharedService authenticationEventSharedService;
	private final DefaultClassicDateFactory dateFactory;

	@Value("${security.lockingDurationSeconds}")
	private int lockingDurationSeconds;

	@Value("${security.lockingThreshold}")
	private int lockingThreshold;

	@Override
	public UserDetails loadUserByUsername(final String loginId) throws UsernameNotFoundException {

		val account = this.accountRepository.findOne(loginId, "login_id")
				.orElseThrow(() -> new UsernameNotFoundException("User not found: " + loginId));

		return new AccountDetail(account, getAuthorities(account), isNonLocked(loginId));
	}

	private Collection<GrantedAuthority> getAuthorities(final Account account) {

		if (account.getRoleName().getString().equals("ROLE_ADMIN")) {
			return AuthorityUtils.createAuthorityList("ROLE_ADMIN", "ROLE_USER");
		}

		return AuthorityUtils.createAuthorityList("ROLE_USER");
	}

	@Transactional(readOnly = true)
	public boolean isNonLocked(final String loginId) {
		val failureEvents = this.authenticationEventSharedService.findLatestFailureEvents(loginId,
				this.lockingThreshold);

		if (failureEvents.size() < this.lockingThreshold) {
			return true;
		}

		//過去１０分以内失敗した数が３回未満であればtrueを返す
		if (failureEvents
				.get(this.lockingThreshold - 1)
				.getAuthenticationTimestamp()
				.isBefore(
						this.dateFactory.newTimestamp().toLocalDateTime()
								.minusSeconds(this.lockingDurationSeconds))) {
			return true;
		}

		return false;
	}

}