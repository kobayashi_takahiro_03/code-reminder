package jp.co.code_reminder.domain.service.account;

import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.model.line_notification.Notify;
import jp.co.code_reminder.domain.model.line_notification.NotifyAccount;
import jp.co.code_reminder.domain.repository.account.AccountRepository;
import jp.co.code_reminder.domain.repository.line_notification.LineNotificationRepository;
import lombok.NonNull;
import lombok.val;

@Service
@Transactional(rollbackFor = Exception.class, readOnly = true)
@lombok.RequiredArgsConstructor
class AccountReferServiceImpl implements AccountReferService {

	private final AccountRepository accountRepository;
	private final LineNotificationRepository lineNotificationRepository;

	@Override
	public Account findOne(@NonNull final String target, @NonNull final String column) throws RecordNotFoundException {
		return this.accountRepository.findOne(target, column).orElseThrow(RecordNotFoundException::new);
	}

	@Override
	public List<Account> findByPageAndLoginId(@NonNull final Pageable pageable, final String searchLoginId) {

		val accountList = this.accountRepository.findAll(pageable, searchLoginId);

		if (CollectionUtils.isEmpty(accountList)) {
			return Collections.emptyList();
		}

		return List.copyOf(accountList);
	}

	@Override
	public boolean existAccount(@NonNull final String target, @NonNull final String column) {
		return this.accountRepository.existAccount(target, column);
	}

	@Override
	public Notify findByUuidForNotify(@NonNull final String uuid) throws RecordNotFoundException {
		return this.lineNotificationRepository.findByUuIdForNotify(uuid).orElseThrow(RecordNotFoundException::new);
	}

	@Override
	public List<Notify> findAllNotify() {

		val notifyList = this.lineNotificationRepository.findAllNotify();

		if (CollectionUtils.isEmpty(notifyList)) {
			return Collections.emptyList();
		}

		return notifyList;
	}

	@Override
	public List<NotifyAccount> findNotifyAndAccount() {

		val notifyListAccount = this.lineNotificationRepository.findNotifyAndAccount();

		if (CollectionUtils.isEmpty(notifyListAccount)) {
			return Collections.emptyList();
		}

		return notifyListAccount;
	}

	@Override
	public boolean existLineNotify(@NonNull final String accountId) {
		return this.lineNotificationRepository.existAccountId(accountId);
	}

	@Override
	public int count(final String searchLoginId) {
		return this.accountRepository.count(searchLoginId);
	}

}