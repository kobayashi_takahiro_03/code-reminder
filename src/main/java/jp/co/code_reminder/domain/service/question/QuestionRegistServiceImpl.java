package jp.co.code_reminder.domain.service.question;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import jp.co.code_reminder.core.algorithm.CalculateReviewDateLogic;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.category.QuestionCategory;
import jp.co.code_reminder.domain.model.question.Learn;
import jp.co.code_reminder.domain.model.question.QuestionReviewCategory;
import jp.co.code_reminder.domain.repository.category.QuestionCategoryRepository;
import jp.co.code_reminder.domain.repository.question.QuestionRepository;
import jp.co.code_reminder.domain.repository.question.ReviewRepository;
import jp.co.code_reminder.domain.service.category.CategoryReferService;
import lombok.val;

/**
 * QuestionReferServiceで定義されている基本サービスを組み合わせた複合的サービスを提供します。
 * こちらは実装側です。
 * @author shotaromaruyama
 *
 */
@Service
@lombok.RequiredArgsConstructor
class QuestionRegistServiceImpl implements QuestionRegistService {

	private final QuestionRepository questionRepository;
	private final QuestionCategoryRepository questionCategoryRepository;
	private final CategoryReferService categoryReferService;
	private final ReviewRepository reviewRepository;
	private final CalculateReviewDateLogic calculateReviewDateLogic;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateQuestion(final QuestionReviewCategory qrc, final String accountId)
			throws IllegalArgumentException {

		if (!questionRepository.existIdAndAccountId(qrc.getQuestion().getId(), accountId)) {
			throw new IllegalArgumentException();
		}

		this.questionRepository.update(qrc.getQuestion());

		this.reviewRepository.updateCompleteDay(qrc.getReview().getCompleteDay());

		updateCategory(qrc.getQuestion().getId(), qrc.getQuestion().getAccountId(), qrc.getCategoryNumList());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateReview(final String accountId, final Integer questionId, final String scoreParam)
			throws RecordNotFoundException, IllegalArgumentException {

		if (!questionRepository.existIdAndAccountId(questionId, accountId)) {
			throw new IllegalArgumentException();
		}

		val review = this.reviewRepository.findById(questionId).orElseThrow(RecordNotFoundException::new);

		this.calculateReviewDateLogic.calculateReviewDate(review, convertScore(scoreParam));

		if (review.getCompleteDay() < review.getIntervals()) {
			this.questionRepository.updateIsLearned(questionId, Learn.Learned);
		}
		this.reviewRepository.update(review);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteQuestion(final String pathQuestionId) throws IllegalArgumentException {
		this.questionRepository.delete(parseQuestionIdParam(pathQuestionId));
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createQuestion(final QuestionReviewCategory qrc, final String accountId)
			throws DuplicateKeyException, RecordNotFoundException, IllegalArgumentException {

		val questionId = qrc.getQuestion().getId();
		val categoryNumList = qrc.getCategoryNumList();

		if (Objects.nonNull(questionId)) {
			if (!this.questionRepository.existIdAndAccountId(questionId, accountId)) {
				throw new RecordNotFoundException();
			}
		}
		if (!CollectionUtils.isEmpty(qrc.getCategoryNumList())
				&& !this.categoryReferService.existIdAndAccountId(categoryNumList, accountId)) {
			throw new IllegalArgumentException();
		}

		this.questionRepository.insert(qrc.getQuestion());

		qrc.getReview().setId(qrc.getQuestion().getId());
		this.reviewRepository.insert(qrc.getReview());

		insertCategory(qrc.getQuestion().getId(), qrc.getQuestion().getAccountId(), qrc.getCategoryNumList());
	}

	private void insertCategory(final Integer questionId, final String accountId, final List<Integer> categoryNumList) {

		if (CollectionUtils.isEmpty(categoryNumList)) {
			return;
		}

		final List<QuestionCategory> qcList = categoryNumList.stream()
				.map(x -> {
					val qc = new QuestionCategory();
					qc.setQuestionId(questionId);
					qc.setCategoryId(x);
					qc.setAccountId(accountId);
					return qc;
				}).collect(Collectors.toList());

		this.questionCategoryRepository.insertList(qcList);
	}

	private void updateCategory(final Integer questionId, final String accountId, final List<Integer> categoryNumList) {

		if (this.questionCategoryRepository.count(accountId) == 0) {
			return;
		}

		val registeredList = this.questionCategoryRepository.findByQuestionId(questionId);
		val deleteIdList = new ArrayList<Integer>();
		val qcIdList = new ArrayList<Integer>();

		registeredList.stream().forEach(x -> {
			if (categoryNumList.contains(x.getCategoryId())) {
				qcIdList.add(x.getCategoryId());
			} else {
				deleteIdList.add(x.getId());
			}
		});

		if (!CollectionUtils.isEmpty(deleteIdList)) {
			this.questionCategoryRepository.deleteAll(deleteIdList);
		}

		final List<QuestionCategory> qcList = categoryNumList.stream()
				.filter(x -> !qcIdList.contains(x))
				.map(x -> {
					val qc = new QuestionCategory();
					qc.setQuestionId(questionId);
					qc.setCategoryId(x);
					qc.setAccountId(accountId);
					return qc;
				}).collect(Collectors.toList());

		if (CollectionUtils.isEmpty(qcList)) {
			return;
		}

		this.questionCategoryRepository.insertList(qcList);
	}

	private Integer parseQuestionIdParam(final String questionId) throws IllegalArgumentException {
		if (!questionId.matches("^[0-9]*$") || StringUtils.isBlank(questionId)) {
			throw new IllegalArgumentException();
		}

		return Integer.valueOf(questionId);
	}

	private int convertScore(final String scoreParam) {

		int result = 0;
		switch (scoreParam) {
		case "分からなかった":
			result = 1;
			break;
		case "難しい":
			result = 2;
			break;
		case "普通":
			result = 3;
			break;
		case "余裕":
			result = 4;
			break;
		default:
			result = 1;
		}
		return result;
	}
}
