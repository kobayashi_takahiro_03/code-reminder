package jp.co.code_reminder.domain.service.question;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.code_reminder.app.question.SearchForm;
import jp.co.code_reminder.app.welcome.GenericSearchForm;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.question.QuestionAccountCategory;
import jp.co.code_reminder.domain.model.question.QuestionReviewCategory;
import jp.co.code_reminder.domain.repository.question.QuestionAccountCategoryRepository;
import jp.co.code_reminder.domain.repository.question.QuestionRepository;
import jp.co.code_reminder.domain.repository.question.QuestionReviewCategoryRepository;
import lombok.NonNull;
import lombok.val;

/**
 *
 * @author Takahiro Kobayashi
 *
 */
@Service
@lombok.RequiredArgsConstructor
class QuestionReferServiceImpl implements QuestionReferService {

	private final QuestionReviewCategoryRepository questionReviewCategoryRepository;
	private final QuestionRepository questionRepository;
	private final QuestionAccountCategoryRepository questionAccountCategoryRepository;

	@Override
	@Transactional(readOnly = true)
	public List<QuestionReviewCategory> findByPageAndSearchForm(
			@NonNull final Pageable pageable, final String accountId, final SearchForm searchForm) {
		return List.copyOf(this.questionReviewCategoryRepository.findByPageAndSearchForm(pageable, accountId, searchForm));
	}

	@Override
	@Transactional(readOnly = true)
	public List<QuestionReviewCategory> findByAccountId(@NonNull final String accountId) {
		return List.copyOf(this.questionReviewCategoryRepository.findByAccountId(accountId));
	}

	@Override
	@Transactional(readOnly = true)
	public List<QuestionReviewCategory> findByAccountIdAndToday(@NonNull final String accountId) {

		val ld = LocalDate.now();
		val ldt = LocalDateTime.of(ld.getYear(), ld.getMonth(), ld.getDayOfMonth(), 23, 59, 59);
		val zdt = ldt.atZone(ZoneId.of("Asia/Tokyo"));
		val millsecond = zdt.toInstant().toEpochMilli();
		val today = new Timestamp(millsecond);

		return List.copyOf(this.questionReviewCategoryRepository.findByAccountIdAndToday(accountId, today));
	}

	@Override
	@Transactional(readOnly = true)
	public QuestionReviewCategory findByQuestionId(final String pathQuestionId, final String accountId)
			throws RecordNotFoundException, IllegalArgumentException {

		val parsedQuestionId = parseQuestionIdParam(pathQuestionId);

		if (!this.questionRepository.existIdAndAccountId(parsedQuestionId, accountId)) {
			throw new IllegalArgumentException();
		}

		return this.questionReviewCategoryRepository.findByQuestionId(parsedQuestionId)
				.orElseThrow(RecordNotFoundException::new);
	}

	@Override
	@Transactional(readOnly = true)
	public QuestionReviewCategory findByQuestionId(final String pathQuestionId)
			throws RecordNotFoundException, IllegalArgumentException {

		return this.questionReviewCategoryRepository.findByQuestionId(parseQuestionIdParam(pathQuestionId))
				.orElseThrow(RecordNotFoundException::new);
	}

	@Override
	@Transactional(readOnly = true)
	public List<QuestionAccountCategory> findByPageAndGenericSearchForm(final Pageable pageable,
			final GenericSearchForm genericSearchForm) {
		return List.copyOf(this.questionAccountCategoryRepository.findByPageAndGenericSearchForm(pageable, genericSearchForm));
	}

	@Override
	@Transactional(readOnly = true)
	public int countByAccountIdAndSearchForm(final String accountId, final SearchForm searchForm) {
		return this.questionReviewCategoryRepository.count(accountId, searchForm);
	}

	@Override
	@Transactional(readOnly = true)
	public int countByGenericSearchForm(final GenericSearchForm genericSearchForm) {
		return this.questionAccountCategoryRepository.count(genericSearchForm);
	}

	private Integer parseQuestionIdParam(final String questionId) throws IllegalArgumentException {
		if (!questionId.matches("^[0-9]*$") || StringUtils.isBlank(questionId)) {
			throw new IllegalArgumentException();
		}
		return Integer.valueOf(questionId);
	}

}
