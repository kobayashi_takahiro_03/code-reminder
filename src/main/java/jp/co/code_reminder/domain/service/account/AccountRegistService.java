package jp.co.code_reminder.domain.service.account;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.model.line_notification.Notify;

public interface AccountRegistService {

	void deleteAccount(String uuid) throws RecordNotFoundException, IllegalArgumentException;

	void updateAccount(Account account);

	void upsertNotify(Notify notify);

}
