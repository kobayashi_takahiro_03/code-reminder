package jp.co.code_reminder.domain.service.account;

import java.util.List;

import org.springframework.data.domain.Pageable;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.model.line_notification.Notify;
import jp.co.code_reminder.domain.model.line_notification.NotifyAccount;

public interface AccountReferService {

	Account findOne(String target, String column) throws RecordNotFoundException;

	List<Account> findByPageAndLoginId(Pageable pageable, String searchLoginId);

	boolean existAccount(String target, String column);

	Notify findByUuidForNotify(String uuiId) throws RecordNotFoundException;

	List<Notify> findAllNotify();

	List<NotifyAccount> findNotifyAndAccount();

	boolean existLineNotify(String accountId);

	int count(final String searchLoginId);
}
