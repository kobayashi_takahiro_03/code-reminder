package jp.co.code_reminder.domain.service.category;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.category.Category;
import jp.co.code_reminder.domain.repository.category.CategoryRepository;
import lombok.NonNull;

@Service
@Transactional(rollbackFor = Exception.class)
@lombok.RequiredArgsConstructor
class CategoryRegistServiceImpl implements CategoryRegistService {

	private final CategoryRepository categoryRepository;
	private final CategoryReferService categoryReferService;

	@Override
	public void create(@NonNull final Category category) {
		this.categoryRepository.insert(category);
	}

	@Override
	public void update(@NonNull final String accountId, final List<Category> categoryList)
			throws IllegalArgumentException {

		if (CollectionUtils.isEmpty(categoryList)) {
			return;
		}

		List<Integer> categoryNumList = categoryList.stream().map(x -> x.getId()).collect(Collectors.toList());

		if (!this.categoryReferService.existIdAndAccountId(categoryNumList, accountId)) {
			throw new IllegalArgumentException();
		}

		this.categoryRepository.update(categoryList);
	}

	@Override
	public void deleteMultiple(@NonNull final String accountId,  final List<Integer> deleteNumList)
			throws RecordNotFoundException, IllegalArgumentException {

		if (CollectionUtils.isEmpty(deleteNumList)) {
			throw new RecordNotFoundException();
		}

		if (!this.categoryReferService.existIdAndAccountId(deleteNumList, accountId)) {
			throw new IllegalArgumentException();
		}

		this.categoryRepository.delete(deleteNumList);
	}
}