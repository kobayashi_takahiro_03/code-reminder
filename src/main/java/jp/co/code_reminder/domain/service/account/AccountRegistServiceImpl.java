package jp.co.code_reminder.domain.service.account;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.model.line_notification.Notify;
import jp.co.code_reminder.domain.repository.account.AccountRepository;
import jp.co.code_reminder.domain.repository.line_notification.LineNotificationRepository;
import jp.co.code_reminder.domain.service.security.AccountDetail;
import lombok.NonNull;
import lombok.val;

@Service
@lombok.RequiredArgsConstructor
class AccountRegistServiceImpl implements AccountRegistService {

	private final AccountReferService accountReferService;
	private final AccountRepository accountRepository;
	private final LineNotificationRepository lineNotificationRepository;
	private final PasswordEncoder passwordEncoder;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteAccount(@NonNull final String uuid)
			throws RecordNotFoundException, IllegalArgumentException {

		if (!this.accountReferService.existAccount(uuid, "uuid")) {
			throw new RecordNotFoundException();
		}

		if (getLoginUuid().equals(uuid)) {
			throw new IllegalArgumentException();
		}

		this.accountRepository.delete(uuid);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateAccount(@NonNull final Account account) {
		if (StringUtils.isNotBlank(account.getPassword())) {
			account.setPassword(this.passwordEncoder.encode(account.getPassword()));
		}
		this.accountRepository.update(account);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void upsertNotify(@NonNull final Notify notify) {
		this.lineNotificationRepository.upsertNotify(notify);
	}

	private String getLoginUuid() throws NullPointerException {
		val loginAccount = ((AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
				.getAccount();

		Objects.requireNonNull(loginAccount, "ログインユーザーが存在しません");

		return loginAccount.getUuid();
	}

}
