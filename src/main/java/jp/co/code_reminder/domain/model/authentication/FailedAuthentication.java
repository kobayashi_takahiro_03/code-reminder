package jp.co.code_reminder.domain.model.authentication;

import java.io.Serializable;
import java.time.LocalDateTime;

@lombok.Data
public class FailedAuthentication implements Serializable {
    private static final long serialVersionUID = 1L;

    private String loginId;

    private LocalDateTime authenticationTimestamp;
}