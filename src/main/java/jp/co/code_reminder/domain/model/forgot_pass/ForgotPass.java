package jp.co.code_reminder.domain.model.forgot_pass;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

@lombok.Data
public class ForgotPass implements Serializable {

	private static final long serialVersionUID = 1L;

	private String uuid;
	private String mail;
	private Timestamp createdDate;
	private Timestamp updatedDate;

	public void setUuid(final String uuid) {

		if (StringUtils.isBlank(uuid)) {
			this.uuid = UUID.randomUUID().toString();
			return;
		}

		this.uuid = uuid;
	}

}
