package jp.co.code_reminder.domain.model.question;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@lombok.Data
public class Review implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer repetitions;
	private Integer intervals;
	private Float easiness;
	private Integer completeDay;
	private Timestamp nextLearnDate;
	private Timestamp createdDate;
	private Timestamp updatedDate;

	public void setRepetitions(final Integer repetitions) {

		if (Objects.isNull(repetitions)) {
			this.repetitions = 0;
			return;
		}

		this.repetitions = repetitions;
	}

	public void setIntervals(final Integer intervals) {

		if (Objects.isNull(intervals)) {
			this.intervals = 1;
			return;
		}

		this.intervals = intervals;
	}

	public void setEasiness(final Float easiness) {

		if (Objects.isNull(easiness)) {
			this.easiness = 2.5F;
			return;
		}

		this.easiness = easiness;
	}
}
