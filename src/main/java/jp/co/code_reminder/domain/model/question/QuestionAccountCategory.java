package jp.co.code_reminder.domain.model.question;

import java.io.Serializable;
import java.util.List;

import jp.co.code_reminder.domain.model.account.Account;

/**
 *
 * タイムラインの表示に使います
 * @author shotaromaruyama
 */
@lombok.Data
public class QuestionAccountCategory implements Serializable {
	private static final long serialVersionUID = 1L;
	private Question question;
	private Account account;
	private List<String> categoryList;
}
