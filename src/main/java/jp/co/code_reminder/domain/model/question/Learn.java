package jp.co.code_reminder.domain.model.question;

public enum Learn{

	Learned(1, "完了"), NotLearned(0, "未完了");

	private final int num;
	private final String status;

	private Learn(final int num, final String status) {
		this.num = num;
		this.status = status;
	}

	public int getInt() {
		return this.num;
	}

	public String getString() {
		return status;
	}

}
