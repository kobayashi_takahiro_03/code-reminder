package jp.co.code_reminder.domain.model.account;

import java.io.Serializable;
import java.sql.Timestamp;

@lombok.Data
@lombok.ToString(exclude = { "password" })
public class MailAuthAccount implements Serializable {

	private static final long serialVersionUID = 1L;

	private String uuid;
	private String loginId;
	private String password;
	private String name;
	private String mail;
	private RoleName roleName;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
