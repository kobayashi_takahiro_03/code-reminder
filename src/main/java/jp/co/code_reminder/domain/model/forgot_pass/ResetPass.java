package jp.co.code_reminder.domain.model.forgot_pass;

import java.io.Serializable;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

@lombok.Data
public class ResetPass implements Serializable {

	private static final long serialVersionUID = 1L;

	private String uuid;
	private String password;
	private String confirmPassword;

	public void setUuid(final String uuid) {

		if (StringUtils.isBlank(uuid)) {
			this.uuid = UUID.randomUUID().toString();
			return;
		}

		this.uuid = uuid;
	}
}
