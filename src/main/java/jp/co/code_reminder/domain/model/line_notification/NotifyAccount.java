package jp.co.code_reminder.domain.model.line_notification;

import java.io.Serializable;

@lombok.Data
public class NotifyAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	private String accountId;
	private String name;
	private String mail;
	private String token;
	private NotificationFlag notificationFlag;
	private String notificationTime;
}
