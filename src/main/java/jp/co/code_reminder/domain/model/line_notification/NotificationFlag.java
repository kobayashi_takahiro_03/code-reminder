package jp.co.code_reminder.domain.model.line_notification;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public enum NotificationFlag {

	Notification(1, "オン"), NotNotification(0, "オフ");

	private final int num;
	private final String status;

	private NotificationFlag(final int num, final String status) {
		this.num = num;
		this.status = status;
	}

	public int getInt() {
		return this.num;
	}

	public String getString() {
		return status;
	}

	public static Map<Integer, NotificationFlag> getMap() {
		return Collections.unmodifiableMap(new LinkedHashMap<Integer, NotificationFlag>() {
			{
				put(NotificationFlag.Notification.getInt(), NotificationFlag.Notification);
				put(NotificationFlag.NotNotification.getInt(), NotificationFlag.NotNotification);
			}
		});
	}
}