package jp.co.code_reminder.domain.model.question;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public enum Share {
	Shared(1, "公開"), NotShared(0, "非公開");

	private final int num;
	private final String status;

	private Share(final int num, final String status) {
		this.num = num;
		this.status = status;
	}

	public int getInt() {
		return this.num;
	}

	public String getString() {
		return status;
	}

	public static Map<Integer, Share> getMap() {
		return Collections.unmodifiableMap(new LinkedHashMap<Integer, Share>() {
			{
				put(1, Share.Shared);
				put(0, Share.NotShared);
			}
		});
	}
}
