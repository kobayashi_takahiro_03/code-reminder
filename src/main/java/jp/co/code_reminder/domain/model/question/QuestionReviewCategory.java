package jp.co.code_reminder.domain.model.question;

import java.io.Serializable;
import java.util.List;

@lombok.Data
public class QuestionReviewCategory implements Serializable {

	private static final long serialVersionUID = 1L;
	private Question question;
	private Review review;
	private List<Integer> categoryNumList;

}
