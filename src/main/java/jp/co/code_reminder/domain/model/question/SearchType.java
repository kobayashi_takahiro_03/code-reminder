package jp.co.code_reminder.domain.model.question;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public enum SearchType {

	Question(0, "問題"), Category(1, "カテゴリ");

	private final int num;
	private final String searchType;

	private SearchType(final int num, final String searchType) {
		this.num = num;
		this.searchType = searchType;
	}

	public int getInt() {
		return this.num;
	}

	public String getString() {
		return searchType;
	}


	public static Map<Integer, SearchType> createSearchTypeMap() {
		return Collections.unmodifiableMap(new LinkedHashMap<Integer, SearchType>() {
			{
				put(0, SearchType.Question);
				put(1, SearchType.Category);
			}
		});
	}

}
