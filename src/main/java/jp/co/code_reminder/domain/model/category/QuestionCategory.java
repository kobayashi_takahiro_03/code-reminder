package jp.co.code_reminder.domain.model.category;

import java.io.Serializable;
import java.sql.Timestamp;

@lombok.Data
public class QuestionCategory implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer questionId;
	private Integer categoryId;
	private String accountId;
	private String name;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
