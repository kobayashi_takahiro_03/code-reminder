package jp.co.code_reminder.domain.model.line_notification;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

@lombok.Data
public class Notify implements Serializable {
	private static final long serialVersionUID = 1L;

	public Notify(String accountId) {
		this.accountId = accountId;
		this.notificationFlag = NotificationFlag.NotNotification;
	}

	public Notify() {
	}

	private String accountId;
	private String token;
	private NotificationFlag notificationFlag;
	private String notificationTime;
	private Timestamp createdDate;
	private Timestamp updatedDate;

	public NotificationFlag getNotificationFlag() {

		if (Objects.isNull(this.notificationFlag)) {
			this.notificationFlag = NotificationFlag.NotNotification;
			return this.notificationFlag;
		}

		return this.notificationFlag;
	}

	public String getNotificationTime() {

		if (StringUtils.isBlank(this.notificationTime)) {
			this.notificationTime = "9";
			return this.notificationTime;
		}

		return this.notificationTime;
	}

	public static List<String> getSelectTimeList() {
		return Stream.iterate(0, n -> n + 1).map(String::valueOf).limit(24).collect(Collectors.toList());
	}

}