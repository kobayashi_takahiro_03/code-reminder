package jp.co.code_reminder.domain.model.question;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import jp.co.code_reminder.domain.service.security.AccountDetail;
import lombok.val;

@lombok.Data
public class Question implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String question;
	private String answer;
	private Learn isLearned;
	private Share isShared;
	private String accountId;
	private Timestamp createdDate;
	private Timestamp updatedDate;

	public void setAccountId(final String accountId) {

		if (StringUtils.isBlank(accountId)) {
			this.accountId = getLoginUuid();
			return;
		}

		this.accountId = accountId;
	}

	private String getLoginUuid() throws NullPointerException {
		val loginAccount = ((AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
				.getAccount();

		Objects.requireNonNull(loginAccount, "ログインユーザーが存在しません");

		return loginAccount.getUuid();
	}
}
