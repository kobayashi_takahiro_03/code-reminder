package jp.co.code_reminder.core.mail;

import org.springframework.mail.SimpleMailMessage;

import lombok.AccessLevel;
import lombok.val;

@lombok.Builder
@lombok.RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class MailContent {

	@lombok.NonNull
	private final String path;
	@lombok.NonNull
	private final String title;
	@lombok.NonNull
	private final String message;
	@lombok.NonNull
	private final String mail;
	@lombok.NonNull
	private final String name;
	@lombok.NonNull
	private final UrlProperty urlProperty;

	public SimpleMailMessage createMail() {

		val url = this.urlProperty.getProtocol() + this.urlProperty.getHostname() + this.urlProperty.getDomain() + this.path;
		val title = this.title;
		val content = this.name + "さん"
				+ "\n" + "\n"
				+ this.message
				+ "\n" + "\n"
				+ url
				+ "\n" + "\n"
				+ "心当たりの無い方は、メールを削除して下さい。";

		val msg = new SimpleMailMessage();
		msg.setFrom(this.urlProperty.getMail());
		msg.setTo(this.mail);
		msg.setSubject(title);
		msg.setText(content);

		return msg;
	}

}
