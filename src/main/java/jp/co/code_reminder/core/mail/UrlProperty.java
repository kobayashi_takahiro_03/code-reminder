package jp.co.code_reminder.core.mail;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.url")
@lombok.Data
public class UrlProperty {
	private String protocol;
	private String hostname;
	private String domain;
	private String mail;
}