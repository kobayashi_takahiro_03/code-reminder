package jp.co.code_reminder.core.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

@Component
public class MailSenderImpl implements MailSender {

	@Autowired
	private JavaMailSenderImpl sender;

	@Override
	public void send(final SimpleMailMessage simpleMessage) {
		this.sender.send(simpleMessage);
	}

	@Override
	public void send(final SimpleMailMessage... simpleMessages) {
	}
}