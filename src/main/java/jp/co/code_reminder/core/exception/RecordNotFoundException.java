package jp.co.code_reminder.core.exception;

public final class RecordNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RecordNotFoundException(final String message) {
		super(message);
	}

	public RecordNotFoundException() {

	}

}
