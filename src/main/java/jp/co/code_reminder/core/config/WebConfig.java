package jp.co.code_reminder.core.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.MappedInterceptor;
import org.springframework.web.servlet.resource.EncodedResourceResolver;
import org.terasoluna.gfw.common.date.ClassicDateFactory;
import org.terasoluna.gfw.common.date.DefaultClassicDateFactory;

import jp.co.code_reminder.core.algorithm.CalculateReviewDateLogic;
import jp.co.code_reminder.core.algorithm.SuperMemo2;
import jp.co.code_reminder.infra.orika.converter.StringToTimestampConverter;
import jp.co.code_reminder.web.interceptor.QuestionIntercepter;
import lombok.val;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Configuration
@PropertySource("classpath:/application.properties")
public class WebConfig implements WebMvcConfigurer {

	@Autowired
	private Environment env;

	@Autowired
	private QuestionIntercepter questionIntercepter;

	@Autowired
	private DataSource dataSource;

	@Bean
	public DataSourceTransactionManager transactionManager() {
	  return new DataSourceTransactionManager(this.dataSource);
	}

	@Bean
	public LocalValidatorFactoryBean localValidatorFactoryBean() {
		val localValidatorFactoryBean = new LocalValidatorFactoryBean();
		localValidatorFactoryBean.setValidationMessageSource(messageSource());
		return localValidatorFactoryBean;
	}

	@Bean
	public MessageSource messageSource() {
		val messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.addBasenames("classpath:messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Bean
	public MapperFactory getMapperFactory() {
		val mapperFactory = new DefaultMapperFactory.Builder().build();
	   val converterFactory = mapperFactory.getConverterFactory();
	    converterFactory.registerConverter(new StringToTimestampConverter());
		return mapperFactory;
	}

	@Bean
	public JavaMailSenderImpl mailSender() {
		val mailSender = new JavaMailSenderImpl();
		mailSender.setHost(this.env.getProperty("spring.mail.host"));
		mailSender.setPort(Integer.valueOf(this.env.getProperty("spring.mail.port")));
		mailSender.setUsername(this.env.getProperty("spring.mail.username"));
		mailSender.setPassword(this.env.getProperty("spring.mail.password"));

		val property = new Properties();
		property.setProperty("mail.smtp.auth", this.env.getProperty("spring.mail.properties.mail.smtp.auth"));
		property.setProperty("mail.smtp.starttls.enable",
				this.env.getProperty("spring.mail.properties.mail.smtp.starttls.enable"));
		mailSender.setJavaMailProperties(property);
		return mailSender;
	}

	@Bean
	public CalculateReviewDateLogic getCalculateReviewDateLogic() {
		return new SuperMemo2();
	}

	@Bean
	public ClassicDateFactory getClassicDateFactory() {
		return new DefaultClassicDateFactory();
	}

	@Bean
	public MappedInterceptor accessInterceptor() {
		String[] includePatterns = { "/**" };
		String[] excludePatterns = { "/favicon.ico", "robots.txt" };
		return new MappedInterceptor(includePatterns, excludePatterns, this.questionIntercepter);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/webjars/**")
				.addResourceLocations("classpath:/META-INF/resources/webjars/")
				.resourceChain(false).addResolver(new EncodedResourceResolver());
	}

}