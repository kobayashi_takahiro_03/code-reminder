package jp.co.code_reminder.core.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.ForwardAuthenticationFailureHandler;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import jp.co.code_reminder.domain.repository.security.JdbcTokenRepositoryImpl;
import jp.co.code_reminder.domain.service.security.AccountDetailService;
import lombok.val;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private final String REMEMEBER_ME_KEY = "codeRemindTeam";

	@Autowired
	private DataSource dataSource;

	@Autowired
	private AccountDetailService accountDetailService;

	@Autowired
	private AuthenticationEventPublisher authenticationEventPublisher;

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationEventPublisher(this.authenticationEventPublisher)
				.userDetailsService(this.accountDetailService)
				.passwordEncoder(passwordEncoder());
	}

	@Override
	public void configure(final WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/css/**", "/js/**", "/img/**", "/webjars/**");
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/").permitAll()
				.antMatchers("/login").permitAll()
				.antMatchers("/signup").permitAll()
				.antMatchers("/signup/validate/**").permitAll()
				.antMatchers("/forgotpass").permitAll()
				.antMatchers("/forgotpass/validate/**").permitAll()
				.antMatchers("/about").permitAll()
				.antMatchers("/contact").permitAll()
				.antMatchers("/terms").permitAll()
				.antMatchers("/privacy").permitAll()
				.antMatchers("/admin/**").hasRole("ADMIN")
				.anyRequest().authenticated()
				.and()
				.rememberMe()
				.tokenRepository(createTokenRepository())
				.rememberMeParameter("isOmisshionLogin") //自動ログインチェックボックスのPram名
				.rememberMeCookieName("remember_me") //発行されるCookie名
				.key(this.REMEMEBER_ME_KEY) //Cookieのハッシュ化に使う平文
				.tokenValiditySeconds(7 * 24 * 60 * 60) //自動ログイン有効期限 秒単位
				.useSecureCookie(true) //あとで消す HTTPSプロトコル出ないとCookieを返さなくなるためローカル環境でtrueにするとremembermeが動作しなくなるようです。デプロイ後にtrueにします。
				.and()
				.exceptionHandling().accessDeniedPage("/")
				.and()
				.formLogin()
				.loginPage("/login")
				.usernameParameter("loginId")
				.passwordParameter("password")
				.successForwardUrl("/")
				.failureHandler(new ForwardAuthenticationFailureHandler("/login-error"))
				.and()
				.logout()
				.logoutUrl("/logout")
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET"))
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID")
				.logoutSuccessUrl("/login")
		;
	}

	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}

	@Bean
	public RememberMeAuthenticationFilter rememberMeFilter() throws Exception {
		return new RememberMeAuthenticationFilter(authenticationManager(), rememberMeServices());
	}

	@Bean
	public TokenBasedRememberMeServices rememberMeServices() {
		return new TokenBasedRememberMeServices(this.REMEMEBER_ME_KEY, this.accountDetailService);
	}

	@Bean
	public RememberMeAuthenticationProvider rememberMeAuthenticationProvider() {
		return new RememberMeAuthenticationProvider(this.REMEMEBER_ME_KEY);
	}

	@Bean
	public AuthenticationEventPublisher authenticationEventPublisher(
			final ApplicationEventPublisher applicationEventPublisher) {
		return new DefaultAuthenticationEventPublisher(applicationEventPublisher);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public PersistentTokenRepository createTokenRepository() {
		val jdbcTokenRepositoryImpl = new JdbcTokenRepositoryImpl();
		jdbcTokenRepositoryImpl.setDataSource(this.dataSource);
		return jdbcTokenRepositoryImpl;
	}
}