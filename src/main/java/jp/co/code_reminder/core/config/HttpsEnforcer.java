package jp.co.code_reminder.core.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import lombok.val;


@Component
public class HttpsEnforcer implements Filter {

	/**
     * 初期化中にフィルタに情報を渡すためにサーブレットコンテナによって使用されるフィルタ設定オブジェクトです。
     */
    @SuppressWarnings("unused")
	private FilterConfig filterConfig;

    /**
     * X_FORWARDED_PROTO (XFP) ヘッダーは、プロキシまたはロードバランサーへ接続するのに使っていた
     * クライアントのプロトコル (HTTP または HTTPS) を特定するために事実上の標準となっているヘッダーです。
     */
    public static final String X_FORWARDED_PROTO = "x-forwarded-proto";

    /**
     * ウェブコンテナからコールされ、フィルタをサービスに組み込むためのメソッドです。
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    /**
     * リクエストヘッダにhttps以外のパラメーターが入力された際
     * ホーム画面に遷移させるメソッドです。
     */
    @Override
    public void doFilter(final ServletRequest servletRequest,final ServletResponse servletResponse,final FilterChain filterChain) throws IOException, ServletException {

        val request = (HttpServletRequest) servletRequest;
        val response = (HttpServletResponse) servletResponse;

        if (request.getHeader(X_FORWARDED_PROTO) != null) {
            if (request.getHeader(X_FORWARDED_PROTO).indexOf("https") != 0) {
                response.sendRedirect("https://" + request.getServerName());
                return;
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // nothing
    }
}