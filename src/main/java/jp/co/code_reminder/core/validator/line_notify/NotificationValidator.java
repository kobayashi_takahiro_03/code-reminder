package jp.co.code_reminder.core.validator.line_notify;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import jp.co.code_reminder.app.account.NotifyForm;
import jp.co.code_reminder.domain.model.line_notification.NotificationFlag;
import lombok.val;

@Component
public final class NotificationValidator implements Validator {

	@Override
	public boolean supports(final Class<?> clazz) {
		return NotifyForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors) {
		val notifyForm = (NotifyForm) target;

		if (Objects.isNull(notifyForm.getNotificationFlag())) {
			errors.rejectValue("notificationFlag",
					"NotificationValidator.NotifyForm.notificationFlag",
					"通知設定の選択は必須です");
		}

		if (!notifyForm.getNotificationTime().matches("[0-9]|[1][0-9]|[2][0-4]")) {
			errors.rejectValue("notificationTime",
					"NotificationValidator.NotifyForm.notificationTime",
					"0時から24時の間で入力してください");
		}

		if (StringUtils.isBlank(notifyForm.getToken())) {
			if (notifyForm.getNotificationFlag().equals(NotificationFlag.Notification)) {
				errors.rejectValue("token",
						"NotificationValidator.NotifyForm.token",
						"トークンを入力してください");
			}
			return;
		}

		if (!StringUtils.isBlank(notifyForm.getToken()) &&
				(40 > notifyForm.getToken().length() || 50 < notifyForm.getToken().length())) {
			errors.rejectValue("token",
					"NotificationValidator.NotifyForm.token",
					"トークンは40文字以上50文字以下で入力してください");
			return;
		}

		if (!StringUtils.isBlank(notifyForm.getToken()) &&
				(!notifyForm.getToken().matches("^[a-zA-Z0-9!-/:-@¥[-`{-~]]+$"))) {
			errors.rejectValue("token",
					"NotificationValidator.NotifyForm.token",
					"トークンは記号を含む半角英数字で入力してください");
			return;
		}
		return;
	}
}