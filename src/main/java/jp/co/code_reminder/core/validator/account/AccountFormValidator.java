package jp.co.code_reminder.core.validator.account;

import java.util.Objects;

import org.codehaus.plexus.util.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.Validator;

import jp.co.code_reminder.app.account.AccountForm;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.core.validator.order.MailFormat;
import jp.co.code_reminder.domain.model.account.Account;
import jp.co.code_reminder.domain.service.account.AccountReferService;
import lombok.val;

@Component
@lombok.RequiredArgsConstructor
public final class AccountFormValidator implements Validator {

	private final AccountReferService accountReferService;
	private final SmartValidator smartValidator;

	@Override
	public boolean supports(final Class<?> clazz) {
		return AccountForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors) {

		val accountForm = (AccountForm) target;
		Account registedAccount = null;

		try {
			registedAccount = this.accountReferService.findOne(accountForm.getUuid(), "uuid");
		} catch (final RecordNotFoundException e) {
		}

		validateLoginId(accountForm, errors);
		validateName(accountForm, errors);
		validatePassword(registedAccount, accountForm, errors);
		validateConfirmPassword(registedAccount, accountForm, errors);
		validateMail(registedAccount, accountForm, errors);
		validateConfirmMail(registedAccount, accountForm, errors);
	}

	private void validateLoginId(final AccountForm accountForm, final Errors errors) {

		if (StringUtils.isBlank(accountForm.getLoginId())) {
			errors.rejectValue("loginId",
					"AccountFormValidator.AccountForm.loginId",
					"ログインIDを入力してください");
			return;
		}

		if (accountForm.getLoginId().length() < 6 || 20 < accountForm.getLoginId().length()) {
			errors.rejectValue("loginId",
					"AccountFormValidator.AccountForm.loginId",
					"ログインIDは6文字以上、20文字以下で入力してください");
			return;
		}

		if (!accountForm.getLoginId().matches("^[a-zA-Z0-9]+$")) {
			errors.rejectValue("loginId",
					"AccountFormValidator.AccountForm.loginId",
					"ログインIDは半角英数字で入力してください");
			return;
		}

		String registedLoginId = null;
		try {
			registedLoginId = this.accountReferService.findOne(accountForm.getUuid(), "uuid").getLoginId();
		} catch (final RecordNotFoundException e) {
			if (this.accountReferService.existAccount(accountForm.getLoginId(), "login_id")) {
				errors.rejectValue("loginId",
						"AccountFormValidator.AccountForm.loginId",
						"入力されたログインIDは既に登録されています");
			}
			return;
		}

		if (registedLoginId.equals(accountForm.getLoginId())) {
			return;
		}

		if (this.accountReferService.existAccount(accountForm.getLoginId(), "login_id")) {
			errors.rejectValue("loginId",
					"AccountFormValidator.AccountForm.loginId",
					"入力されたログインIDは既に登録されています");
			return;
		}

	}

	private void validatePassword(final Account registedAccount, final AccountForm accountForm, final Errors errors) {

		if (Objects.nonNull(registedAccount) &&
				StringUtils.isBlank(accountForm.getPassword()) &&
				StringUtils.isBlank(accountForm.getConfirmPassword())) {

			return;
		}

		if (StringUtils.isBlank(accountForm.getPassword())) {
			errors.rejectValue("password",
					"AccountFormValidator.AccountForm.password",
					"パスワードを入力してください");
			return;
		}

		if (accountForm.getPassword().length() < 6 || 20 < accountForm.getPassword().length()) {
			errors.rejectValue("password",
					"AccountFormValidator.AccountForm.password",
					"パスワードは6文字以上、20文字以下で入力してください");
			return;
		}

		if (!accountForm.getPassword().matches("^[a-zA-Z0-9!-/:-@¥[-`{-~]]+$")) {
			errors.rejectValue("password",
					"AccountFormValidator.AccountForm.password",
					"パスワードは記号を含む半角文字で入力してください");
			return;
		}

		if (!accountForm.getPassword().equals(accountForm.getConfirmPassword())
				&& StringUtils.isNotBlank(accountForm.getConfirmPassword())) {
			errors.rejectValue("password",
					"AccountFormValidator.AccountForm.password",
					"入力されたパスワードと確認用パスワードが異なっています");
			return;
		}

	}

	private void validateConfirmPassword(final Account registedAccount, final AccountForm accountForm,
			final Errors errors) {

		if (Objects.nonNull(registedAccount) &&
				StringUtils.isBlank(accountForm.getPassword()) &&
				StringUtils.isBlank(accountForm.getConfirmPassword())) {

			return;
		}

		if (StringUtils.isBlank(accountForm.getConfirmPassword())) {
			errors.rejectValue("confirmPassword",
					"AccountFormValidator.AccountForm.confirmPassword",
					"確認用パスワードを入力してください");
			return;
		}

		if (accountForm.getConfirmPassword().length() < 6 || 20 < accountForm.getConfirmPassword().length()) {
			errors.rejectValue("confirmPassword",
					"AccountFormValidator.AccountForm.confirmPassword",
					"確認用パスワードは6文字以上、20文字以下で入力してください");
			return;
		}

		if (!accountForm.getConfirmPassword().matches("^[a-zA-Z0-9!-/:-@¥[-`{-~]]+$")) {
			errors.rejectValue("confirmPassword",
					"AccountFormValidator.AccountForm.confirmPassword",
					"確認用パスワードは記号を含む半角文字で入力してください");
			return;
		}

	}

	private void validateName(final AccountForm accountForm, final Errors errors) {

		if (StringUtils.isBlank(accountForm.getName())) {
			errors.rejectValue("name",
					"AccountFormValidator.AccountForm.name",
					"ハンドルネームを入力してください");
			return;
		}
		if (accountForm.getName().length() < 2 || 10 < accountForm.getName().length()) {
			errors.rejectValue("name",
					"AccountFormValidator.AccountForm.name",
					"ハンドルネームは2文字以上、10文字以下で入力してください");
			return;
		}
	}

	private void validateMail(final Account registedAccount, final AccountForm accountForm, final Errors errors) {

		if (Objects.nonNull(registedAccount)) {
			if (registedAccount.getMail().equals(accountForm.getMail()) &&
					StringUtils.isBlank(accountForm.getConfirmMail())) {
				return;
			}
		}

		if (StringUtils.isBlank(accountForm.getMail())) {
			errors.rejectValue("mail",
					"AccountFormValidator.AccountForm.mail",
					"メールアドレスを入力してください");
			return;
		}

		if (Objects.isNull(errors.getFieldError("mail")) && Objects.isNull(errors.getFieldError("confirmMail"))) {
			this.smartValidator.validate(accountForm, errors, MailFormat.class);
		}

		if (!accountForm.getMail().equals(accountForm.getConfirmMail())
				&& StringUtils.isNotBlank(accountForm.getConfirmMail())) {
			errors.rejectValue("mail",
					"AccountFormValidator.AccountForm.mail",
					"入力されたメールアドレスと確認用メールアドレスが異なっています");
			return;
		}

		String registedMail = null;
		try {
			registedMail = this.accountReferService.findOne(accountForm.getUuid(), "uuid").getMail();
		} catch (final RecordNotFoundException e) {
			if (this.accountReferService.existAccount(accountForm.getMail(), "mail")) {
				errors.rejectValue("mail",
						"AccountFormValidator.AccountForm.mail",
						"入力されたメールアドレスは既に登録されています");
			}
			return;
		}

		if (registedMail.equals(accountForm.getMail())) {
			return;
		}

		if (this.accountReferService.existAccount(accountForm.getMail(), "mail")) {
			errors.rejectValue("mail",
					"AccountFormValidator.AccountForm.mail",
					"入力されたメールアドレスは既に登録されています");
			return;
		}

	}

	private void validateConfirmMail(final Account registedAccount, final AccountForm accountForm,
			final Errors errors) {

		if (Objects.nonNull(registedAccount)) {
			if (registedAccount.getMail().equals(accountForm.getMail()) &&
					StringUtils.isBlank(accountForm.getConfirmMail())) {
				return;
			}
		}

		if (StringUtils.isBlank(accountForm.getConfirmMail())) {
			errors.rejectValue("confirmMail",
					"AccountFormValidator.AccountForm.confirmMail",
					"確認用メールアドレスを入力してください");
			return;
		}

		if (Objects.isNull(errors.getFieldError("mail")) && Objects.isNull(errors.getFieldError("confirmMail"))) {
			this.smartValidator.validate(accountForm, errors, MailFormat.class);
		}

	}

}
