package jp.co.code_reminder.core.validator.category;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import jp.co.code_reminder.app.category.CategoryForm;
import jp.co.code_reminder.app.category.CategoryForms;
import lombok.val;

@Component
public final class CategoryFormValidator implements Validator {

	@Override
	public boolean supports(final Class<?> clazz) {
		return CategoryForms.class.isAssignableFrom(clazz) || CategoryForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors) {

		if (target instanceof CategoryForms) {
			val categoryForms = (CategoryForms) target;
			val categoryFormList = categoryForms.getCategoryForm();

			for (int i = 0; i < categoryFormList.size(); i++) {
				if (StringUtils.isBlank(categoryFormList.get(i).getName())) {
					errors.rejectValue("categoryForm[" + i + "].name",
							"CategoryValidator.categoryForms.categoryForm[" + i + "].name",
							"カテゴリを入力してください");
				} else if (50 < categoryFormList.get(i).getName().length()) {
					errors.rejectValue("categoryForm[" + i + "].name",
							"CategoryValidator.categoryForms.categoryForm[" + i + "].name",
							"カテゴリは50文字以下で入力してください");
				}
			}
			return;
		}

		if (target instanceof CategoryForm) {
			val categoryForm = (CategoryForm) target;

			if (StringUtils.isBlank(categoryForm.getName())) {
				errors.rejectValue("name",
						"CategoryValidator.categoryForm.name",
						"カテゴリを入力してください");
				return;
			}
			if (50 < categoryForm.getName().length()) {
				errors.rejectValue("name",
						"CategoryValidator.categoryForm.name",
						"カテゴリは50文字以下で入力してください");
				return;
			}
		}
	}
}