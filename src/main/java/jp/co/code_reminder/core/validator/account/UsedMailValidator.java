package jp.co.code_reminder.core.validator.account;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import jp.co.code_reminder.app.account.AccountForm;
import jp.co.code_reminder.app.forgot_pass.ForgotPassForm;
import jp.co.code_reminder.core.exception.RecordNotFoundException;
import jp.co.code_reminder.domain.service.account.AccountReferService;
import lombok.val;

@Component
public final class UsedMailValidator implements Validator {

	@Autowired
	private AccountReferService accountReferService;

	@Override
	public boolean supports(final Class<?> clazz) {
		return AccountForm.class.isAssignableFrom(clazz) || ForgotPassForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors) {

		if (target instanceof AccountForm) {
			val accountForm = (AccountForm) target;

			if (isUpdate(accountForm)) {
				return;
			}

			if (this.accountReferService.existAccount(accountForm.getMail(), "mail")) {
				errors.rejectValue("mail",
						"UsedMailValidator.AccountForm.mail",
						"入力されたメールアドレスは既に登録されています");
			}
		}

		if (target instanceof ForgotPassForm) {
			val forgotPassForm = (ForgotPassForm) target;

			if (errors.getErrorCount() != 0) {
				return;
			}

			if (!this.accountReferService.existAccount(forgotPassForm.getMail(), "mail")) {
				errors.rejectValue("mail",
						"UsedMailValidator.ForgotPassForm.mail",
						"入力されたメールアドレスは既に登録されていません");
			}
		}

	}

	private boolean isUpdate(final AccountForm accountForm) {

		if (StringUtils.isBlank(accountForm.getMail())) {
			return true;
		}

		try {
			if (StringUtils.isBlank(accountForm.getUuid())) {
				return false;
			}

			val mail = this.accountReferService.findOne(accountForm.getUuid(), "uuid").getMail();
			return mail.equals(accountForm.getMail());

		} catch (RecordNotFoundException e) {
			return false;
		}

	}

}
