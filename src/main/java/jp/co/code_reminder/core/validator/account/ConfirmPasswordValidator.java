package jp.co.code_reminder.core.validator.account;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapperImpl;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import lombok.val;

public final class ConfirmPasswordValidator implements ConstraintValidator<ConfirmPassword, Object> {

	private String field;
	private String confirmField;
	private String message;

	public void initialize(final ConfirmPassword constraintAnnotation) {
		this.field = constraintAnnotation.field();
		this.confirmField = "confirm" + StringUtils.capitalize(this.field);
		this.message = constraintAnnotation.message();
	}

	public boolean isValid(final Object value, final ConstraintValidatorContext context) {
		val beanWrapper = new BeanWrapperImpl(value);
		val fieldValue = beanWrapper.getPropertyValue(this.field);
		val confirmFieldValue = beanWrapper.getPropertyValue(this.confirmField);

		if (ObjectUtils.nullSafeEquals(fieldValue, confirmFieldValue)) {
			return true;
		}

		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate(this.message)
				.addPropertyNode(this.field).addConstraintViolation();
		return false;
	}

}