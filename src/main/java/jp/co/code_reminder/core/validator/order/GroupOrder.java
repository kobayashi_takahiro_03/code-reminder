package jp.co.code_reminder.core.validator.order;

import javax.validation.GroupSequence;

@GroupSequence({ ValidGroup1.class, ValidGroup2.class, ValidGroup3.class, ValidGroup4.class })
public interface GroupOrder {
}