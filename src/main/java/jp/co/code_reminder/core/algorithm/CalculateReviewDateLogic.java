package jp.co.code_reminder.core.algorithm;

import jp.co.code_reminder.domain.model.question.Review;

public interface CalculateReviewDateLogic {

	void calculateReviewDate(Review review, int quality);

}
