package jp.co.code_reminder.core.algorithm;

import java.sql.Timestamp;

import jp.co.code_reminder.domain.model.question.Review;
import lombok.val;

public class SuperMemo2 implements CalculateReviewDateLogic {

	public void calculateReviewDate(final Review review, int quality) {
		if (quality < 1) {
			quality = 1;
		} else if (quality > 5) {
			quality = 4;
		}

		var repetitions = review.getRepetitions();
		var easiness = review.getEasiness();
		var interval = review.getIntervals();

		easiness = (float) Math.max(1.3, easiness + 0.1 - (5.0 - quality) * (0.08 + (5.0 - quality) * 0.02));

		/**
		 * defaultはqualityの値は3以下だが、1だった時、インターバルが初日にリセットされるので、甘く設定
		 */
		if (quality < 2) {
			repetitions = 0;
		} else {
			repetitions += 1;
		}

		if (repetitions <= 1) {
			interval = 1;
		} else if (repetitions == 2) {
			interval = 6;
		} else {
			interval = Math.round(interval * easiness);
		}

		val millisecondsInDay = (long) 60 * 60 * 24 * 1000;
		val now = System.currentTimeMillis();
		val nextLearnDate = new Timestamp(now + (millisecondsInDay * interval));

		if (repetitions != 0) {
			review.setRepetitions(repetitions);
		}

		if (repetitions == 0 && review.getRepetitions() == 0) {
			review.setRepetitions(1);
		}

		review.setIntervals(interval);
		review.setEasiness(easiness);
		review.setNextLearnDate(nextLearnDate);
		review.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
	}

}
