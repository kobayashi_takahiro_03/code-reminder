package jp.co.code_reminder.core.message;

public enum Message {

	SUCCESS("success"),
	SUCCESS_MAIL_SEND("メールが送信されました"),
	SUCCESS_AUTHENTICATION("認証に成功しました"),
	SUCCESS_UPDATE("更新に成功しました"),
	SUCCESS_CREATE("登録に成功しました"),
	SUCCESS_LOGIN("ログインに成功しました"),
	SUCCESS_DELETE("削除に成功しました"),

	ERROR("error"),
	ERROR_DELETE_CATEGORY("削除するカテゴリーを選択してください"),
	ERROR_DELETE_LOGIN_ACCOUNT("ログイン中のユーザーは削除することが出来ません"),
	ERROR_RECORD_NOTFOUND("レコードが見つかりません。最初からやり直してください"),
	ERROR_MAIL_SEND("メール送信に失敗しました"),
	ERROR_ILLEGAL_PARAMETER("不正なパラメーターです"),
	ERROR_AUTHENTICATION("認証に失敗しました"),
	ERROR_LOGIN("ログインに失敗しました"),
	ERROR_UPDATE("更新に失敗しました"),
	ERROR_INSERT("登録に失敗しました。最初からやり直して下さい"),
	ERROR_DELETE("削除に失敗しました");

    private final String message;

    private Message(final String message) {
        this.message = message;
    }

    public String getMessage() {
    	return message;
    }

}