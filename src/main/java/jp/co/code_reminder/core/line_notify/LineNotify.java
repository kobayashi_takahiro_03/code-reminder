package jp.co.code_reminder.core.line_notify;

import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import jp.co.code_reminder.core.mail.UrlProperty;
import jp.co.code_reminder.domain.model.line_notification.NotificationFlag;
import jp.co.code_reminder.domain.model.line_notification.NotifyAccount;
import jp.co.code_reminder.domain.service.account.AccountReferService;
import jp.co.code_reminder.domain.service.mail.SendMailService;
import jp.co.code_reminder.domain.service.question.QuestionReferService;
import lombok.val;

@Component
@lombok.RequiredArgsConstructor
public class LineNotify {

	private final AccountReferService accountReferService;
	private final QuestionReferService questionReferService;
	private final SendMailService sendMailService;
	private final UrlProperty urlProperty;

	private List<NotifyAccount> notifyAccountList = new ArrayList<NotifyAccount>();

	@Scheduled(cron = "0 0 * * * *", zone = "Asia/Tokyo")
	public void sendNotify() {
		val notifyMap = getTokenAndNotifyMessage();
		for (val notifyContent : notifyMap.entrySet()) {
			createNotify(notifyContent.getKey(), notifyContent.getValue(), this.notifyAccountList);
		}
		try {
			if (this.notifyAccountList.size() != 0) {
				this.sendMailService.sendNotify(this.notifyAccountList);
			}
		} catch (Exception e) {
		}
		this.notifyAccountList = null;
	}

	private void createNotify(final NotifyAccount notify, final String message,
			final List<NotifyAccount> notifyAccountList) {
		HttpURLConnection connection = null;
		val token = notify.getToken();
		try {
			val url = new URL("https://notify-api.line.me/api/notify");
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.addRequestProperty("Authorization", "Bearer " + token);
			try (val outputStream = connection.getOutputStream();
					val writer = new PrintWriter(outputStream)) {
				writer.append("message=").append(URLEncoder.encode(message, "UTF-8")).flush();
				int statusCode = connection.getResponseCode();
				if (statusCode != 200) {
					this.notifyAccountList.add(notify);
				}
			}
		} catch (Exception e) {
		} finally {
			if (Objects.nonNull(connection)) {
				connection.disconnect();
			}
		}
	}

	private Map<NotifyAccount, String> getTokenAndNotifyMessage() {

		int newQuestionCount = 0;
		int reviewQuestionCount = 0;
		val localDateTime = LocalDateTime.now();
		val presentTime = String.valueOf(localDateTime.getHour());

		val notifyContent = new HashMap<NotifyAccount, String>();
		val notifyList = this.accountReferService.findNotifyAndAccount();

		for (val notify : notifyList) {

			if (notify.getNotificationFlag().equals(NotificationFlag.NotNotification)) {
				continue;
			}

			val settingTime = notify.getNotificationTime();
			if (!settingTime.equals(presentTime)) {
				continue;
			}

			if (StringUtils.isBlank(notify.getToken())) {
				continue;
			}

			val qrcFormList = this.questionReferService.findByAccountIdAndToday(notify.getAccountId());
			if (!CollectionUtils.isEmpty(qrcFormList)) {
				newQuestionCount = (int) qrcFormList.stream()
						.filter(x -> Objects.isNull(x.getReview().getNextLearnDate())).count();
				reviewQuestionCount = (int) qrcFormList.stream()
						.filter(x -> Objects.nonNull(x.getReview().getNextLearnDate())).count();

				val str = new StringBuilder();
				val url = this.urlProperty.getProtocol() + this.urlProperty.getHostname() + this.urlProperty.getDomain()
						+ "review";

				str.append(System.getProperty("line.separator"));
				str.append("新たに学習：" + newQuestionCount);
				str.append(System.getProperty("line.separator"));
				str.append("復習：" + reviewQuestionCount);
				str.append(System.getProperty("line.separator"));
				str.append(url);
				notifyContent.put(notify, str.toString());
			}
		}
		return notifyContent;
	}
}