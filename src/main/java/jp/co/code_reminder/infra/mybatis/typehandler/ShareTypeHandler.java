package jp.co.code_reminder.infra.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import jp.co.code_reminder.domain.model.question.Share;

public final class ShareTypeHandler extends BaseTypeHandler<Share> {

	@Override
	public void setNonNullParameter(final PreparedStatement ps, final int i, final Share parameter,
			final JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getInt());
	}

	@Override
	public Share getNullableResult(final ResultSet rs, final String columnName) throws SQLException {
		if (rs.getInt(columnName) == Share.Shared.getInt()) {
			return Share.Shared;
		} else {
			return Share.NotShared;
		}
	}

	@Override
	public Share getNullableResult(final ResultSet rs, final int columnIndex) throws SQLException {
		if (rs.getInt(columnIndex) == Share.Shared.getInt()) {
			return Share.Shared;
		} else {
			return Share.NotShared;
		}
	}

	@Override
	public Share getNullableResult(final CallableStatement cs, final int columnIndex) throws SQLException {
		if (cs.getInt(columnIndex) == Share.Shared.getInt()) {
			return Share.Shared;
		} else {
			return Share.NotShared;
		}
	}

}
