package jp.co.code_reminder.infra.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import jp.co.code_reminder.domain.model.account.RoleName;

public final class RoleNameTypeHandler extends BaseTypeHandler<RoleName> {

	@Override
	public void setNonNullParameter(final PreparedStatement ps, final int i, final RoleName parameter,
			final JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.getString());
	}

	@Override
	public RoleName getNullableResult(final ResultSet rs, final String columnName) throws SQLException {

		for (RoleName roleName : RoleName.values()) {
			if (rs.getString(columnName).equals(roleName.getString())) {
				return roleName;
			}
		}

		return RoleName.ROLE_USER;
	}

	@Override
	public RoleName getNullableResult(final ResultSet rs, final int columnIndex) throws SQLException {

		for (RoleName roleName : RoleName.values()) {
			if (rs.getString(columnIndex).equals(roleName.getString())) {
				return roleName;
			}
		}

		return RoleName.ROLE_USER;
	}

	@Override
	public RoleName getNullableResult(final CallableStatement cs, final int columnIndex) throws SQLException {

		for (RoleName roleName : RoleName.values()) {
			if (cs.getString(columnIndex).equals(roleName.getString())) {
				return roleName;
			}
		}

		return RoleName.ROLE_USER;
	}

}
