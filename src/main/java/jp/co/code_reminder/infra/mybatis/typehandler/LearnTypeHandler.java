package jp.co.code_reminder.infra.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import jp.co.code_reminder.domain.model.question.Learn;

public final class LearnTypeHandler extends BaseTypeHandler<Learn> {

	@Override
	public void setNonNullParameter(final PreparedStatement ps, final int i, final Learn parameter,
			final JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getInt());
	}

	@Override
	public Learn getNullableResult(final ResultSet rs, final String columnName) throws SQLException {
		if (rs.getInt(columnName) == Learn.Learned.getInt()) {
			return Learn.Learned;
		} else {
			return Learn.NotLearned;
		}
	}

	@Override
	public Learn getNullableResult(final ResultSet rs, final int columnIndex) throws SQLException {
		if (rs.getInt(columnIndex) == Learn.Learned.getInt()) {
			return Learn.Learned;
		} else {
			return Learn.NotLearned;
		}
	}

	@Override
	public Learn getNullableResult(final CallableStatement cs, final int columnIndex) throws SQLException {
		if (cs.getInt(columnIndex) == Learn.Learned.getInt()) {
			return Learn.Learned;
		} else {
			return Learn.NotLearned;
		}
	}
}
