package jp.co.code_reminder.infra.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import jp.co.code_reminder.domain.model.line_notification.NotificationFlag;

public final class NotificationFlagTypeHandler extends BaseTypeHandler<NotificationFlag> {

	@Override
	public void setNonNullParameter(final PreparedStatement ps, final int i, final NotificationFlag parameter,
			final JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getInt());
	}

	@Override
	public NotificationFlag getNullableResult(final ResultSet rs, final String columnName) throws SQLException {
		if (rs.getInt(columnName) == NotificationFlag.Notification.getInt()) {
			return NotificationFlag.Notification;
		} else {
			return NotificationFlag.NotNotification;
		}
	}

	@Override
	public NotificationFlag getNullableResult(final ResultSet rs, final int columnIndex) throws SQLException {
		if (rs.getInt(columnIndex) == NotificationFlag.Notification.getInt()) {
			return NotificationFlag.Notification;
		} else {
			return NotificationFlag.NotNotification;
		}
	}

	@Override
	public NotificationFlag getNullableResult(final CallableStatement cs, final int columnIndex) throws SQLException {
		if (cs.getInt(columnIndex) == NotificationFlag.Notification.getInt()) {
			return NotificationFlag.Notification;
		} else {
			return NotificationFlag.NotNotification;
		}
	}
}