package jp.co.code_reminder.infra.orika.converter;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;

public final class StringToTimestampConverter extends CustomConverter<Timestamp,String> {

	@Override
	public String convert(Timestamp source, Type<? extends String> destinationType, MappingContext mappingContext) {
		return new SimpleDateFormat("yyyy/MM/dd").format(source);
	}
}