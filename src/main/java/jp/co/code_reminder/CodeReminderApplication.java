package jp.co.code_reminder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CodeReminderApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeReminderApplication.class, args);
	}
}
