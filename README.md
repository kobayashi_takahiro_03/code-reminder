## Name

CodeReminder

## Overview

SuperMemo2のアルゴリズムで算出された復習日程を基に学習するシステムです。

[SuperMemoとは？](https://www.supermemo.com/en/archives1990-2015/english/ol/sm2)

## Description

＜パッケージ構成＞

```

//メイン層
src.main.java
  |
  +--- jp.co.code_reminder
			+--- app //コントローラーやフォーム等のアプリケーション層で使用するクラス等
			+--- core //どこの層にも依存していないクラス等
			+--- domain //modelやService、Repository等の格納場所
			+--- infra //データベースアクセス層で使用する設定TypeHandlerなど等
			+--- web //アプリケーション層で使用するSpringSecurityやAOP等設定

src.main.resources
  |
  +--- db
  |	|
  |	+--- migration //flywayのmigrationファイル格納場所
  |
  +--- mybatis //mybatis設定ファイル格納場所
  |
  |
  +--- static //cssやjsのファイル
  |
  |
  +--- templates //Viewファイル格納場所
  |	+--- account
  |	+--- error
  |	+--- layout
  |	+--- question
  |	+--- forgotpass
  |	+--- review
  |	+--- category
  |	+--- welcome
  |	※show.html → 編集前のページ。編集対象のページを表示, new.html → 新規でアイテムを追加する時
  |	※edit.html → 既存のアイテムの更新や削除を行うページ, index.html → 他の3つに該当しない。ホーム画面等
  |
  +--- application.properties
  +--- message_jp.properties


//テスト層
src.test.java
  |
  +--- jp.co.code_reminder
			+--- app //フォームとコントローラーのテスト
			+--- core //アルゴリズムのテスト等
			+--- CodeReminderApplicationTests.java

src.main.resources
  |
  +--- db //テスト用のSQLファイル
  +--- application.properties
  +--- message_jp.properties

```

## Requirement

Java : 11

Maven CLI : 3.6.0

PostgreSQL : 11.5

※IDEにはLombokが導入されている環境が必要です

## Version

Spring Boot : 2.4.0

Thymeleaf : 3.0.11

Bootstrap : 4.5.0

jQuery : 3.5.1

Vue : 2.6.11

flyway : 6.5.0

## Contribution

①データベース作成

```
$ psql -d postgres //ポスグレ接続
$ create database code_reminder; //データベース作成
```

②プロジェクトインポート

```
$ git clone https://kobayashi_takahiro_03@bitbucket.org/kobayashi_takahiro_03/code-reminder.git
```

③ファイルの作成

・src/main/resources/application-local.properties

・src/test/resources/application-local.properties

・/flyway.properties


④③のファイルを編集

・setting.mdを参照下さい


⑤CL上でインポートしたディレクトリに移動後、以下を実行

```
$ mvn validate //コンパイル前の確認
$ mvn compile //コンパイル
$ mvn install //jarのインポート
```


## [heroku開発手順]


**①SpringBootアプリケーションに以下の2つのファイルを用意**

<br>
・Procfile ※必ず頭文字は大文字で作成
<br>

・system.properties
<br>

内容は下記参照
<br>

##### Procfile

```
web: java $JAVA_OPTS -jar target/*.jar --server.port=$PORT
```


##### system.properties

```
java.runtime,version=1.8 //javaのversionは自身の環境に合わせる
```


**②heroku CLIをインストール**

<br>
https://devcenter.heroku.com/articles/heroku-cli#download-and-install
<br>
<br>

**③環境変数の設定**

<br>
コントロールパネルを開き、「システムとセキュリティ」→
「システム」→「システムの詳細設定」→「環境変数」と進む。

システム環境変数欄の「Path」を選択した状態で「編集」を押下。

「新規」を選択後、`[インストール先のパス]/heroku/bin`を入力し設定完了。

<br>

**④herokuにsignup**

<br>
https://signup.heroku.com/login
<br>
<br>

**⑤herokuにログイン**

cmdで以下のコマンドを入力

```
$ heroku login
```


**⑥herokuアプリケーションの作成**

```
$ heroku create <任意のアプリ名>
```


**⑦herokuにアプリケーションをデプロイする**

```
$ git push heroku master
```


**⑧デプロイ後の確認**

```
$ heroku open
```

上記コマンドでアプリを開けます。



## Author
Takahiro Kobayashi